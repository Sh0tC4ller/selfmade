# Multi Theft Auto Selfmade
# Fraktionen
| Staatsfraktionen | Böse Fraktionen  | Neutrale Fraktionen |
|:----------------:|:----------------:|:-------------------:|
| LSPD             | Mafia            | LSR                 |
| FBI              |                  |                     |
| Army             |                  |                     |


#Ideen
###Allgemein
- Aktiensystem
- Interaktives Tutorial
- Neues GPS

###Fraktionen
- Böse Fraktionen bekommen Waffen durch Materialien Trucks, der der Fraktion bestimmte Waffenteile bringt
- Diese können sie dann zu Waffen zusammenbauen. (Evtl. Level-Sys zum Craften von Waffen)
- Waffen stärker machen durch andere Komponeten, Munition etc.
- Viele (hunderte) Gebiete einnehmen oder was anderes (vllt. Verbinden mit Unternehmen(Schutzgeld))

###Unternehmen
- Viele verschiedene Unternehmen
- Einstellen der Preise durch Besitzer 
  * Markt Aufbauen mit den unterschiedlichen Preisen ( evtl. Minimum, das man nicht unterschreiten darf?)
- (Evtl. Einstellen von Mitarbeitern)

###Autos
- Nochmal überarbeiten!

###Clicksystem
- Ausbauen

###GUI's
- (Evtl. eigene Klasse)
