﻿AdminSys = {}

AdminRanks = {
	[1] = "Supporter",
	[2] = "Moderator",
	[3] = "Administrator",
	[4] = "Serverleitung"
}

AdminsOnline = {}

AdminsInSupport = {}

AdminPermissions = {
	["checkip"] = 2,
	["marker"] = 1,
	["gotomarker"] = 1,
	["cleartext"] = 1,
	["adminchat"] = 1,
	["support"] = 1,
	["teleport"] = 2,
	["spectate"] = 3,
	["banperm"] = 3,
	["bantemp"] = 2,
	["banunban"] = 3,
	["bankick"] = 1,
}

AdminCars = {}

function AdminSys.init()
	addEvent("AdminshowKick")
	addEventHandler("reallife:onLogin", getRootElement(), function()
		if AdminSys.isAdmin(source) then
			addEventHandler("AdminshowKick", getRootElement(), AdminSys.showKickAndBans)
			addCommandHandler("o", AdminSys.oChat)
			addCommandHandler("a", AdminSys.aChat)
			addCommandHandler("ip", AdminSys.checkIP)
			addCommandHandler("sup", AdminSys.toggleSup)
			addCommandHandler("ct", AdminSys.clearText)
			addCommandHandler("spawncar", AdminSys.spawnAdminCar)
			addCommandHandler("warpto", AdminSys.gotoPlayer)
			addCommandHandler("warphere", AdminSys.getPlayerHere)
			addCommandHandler("warp", AdminSys.warp)
			addCommandHandler("spec", AdminSys.spectate)

			AdminsOnline[source] = getElementData(source, "Adminlevel")
			setElementData(source, "SupState", true)
			AdminsInSupport[source] = true
		end
			addCommandHandler("admins", AdminSys.showAdmins)
	end)

	addEventHandler("onPlayerQuit", getRootElement(), function()
		if getElementData(source, "isLoggedIn") then
			if AdminSys.isAdmin(source) then
				AdminsOnline[source] = nil

				removeEventHandler("AdminshowKick", getRootElement(), AdminSys.showKickAndBans)

				removeCommandHandler("o", AdminSys.aChat)
				removeCommandHandler("a", AdminSys.aChat)
				removeCommandHandler("ip", AdminSys.checkIP)
				removeCommandHandler("sup", AdminSys.toggleSup)
				removeCommandHandler("ct", AdminSys.clearText)
				removeCommandHandler("spawncar", AdminSys.spawnAdminCar)
				removeCommandHandler("warpto", AdminSys.gotoPlayer)
				removeCommandHandler("warphere", AdminSys.getPlayerHere)
				removeCommandHandler("warp", AdminSys.warp)
				removeCommandHandler("spec", AdminSys.spectate)


				setElementData(source, "SupState", false)
				AdminsInSupport[source] = nil

			end
		removeCommandHandler("admins", AdminSys.showAdmins)
		end
	end)

	outputDebugString("AdminSystem initialisiert!")
end


-- Generelle Funktionen --

function AdminSys.isAdmin(player)
	if isElement(player) and getElementData(player, "isLoggedIn") then
		if getElementData(player, "Adminlevel") > 0 then
			return true
		else
			return false
		end
	else
		assert("Falsches Arugment bei 'isAdmin' ( Erwarte player als Arugment 1, habe "..type(player)..")")
	end
end

function AdminSys.isInSupMode(player) 
	if isElement(player) then
		if AdminSys.isAdmin(player) then
			if getElementData(player, "SupState") then
				return true
			else
				return false
			end
		end
	else
		assert("Falsches Arugment bei 'isInSupMode' ( Erwarte player als Arugment 1, habe "..type(player)..")")
	end
end

function AdminSys.getRankName(player)
	if isElement(player) then
		if AdminSys.isAdmin(player) then
			return AdminRanks[getElementData(player, "Adminlevel")]
		else
			return false	
		end
	else
		assert("Falsches Arugment bei 'getRankName' ( Erwarte player als Arugment 1, habe "..type(player)..")")	
	end
end

function AdminSys.hasPermission(player, permission)
	if isElement(player) and type(permission) == "string" then
		if (AdminPermissions[permission:lower()] ~= nil) then
			if getElementData(player, "Adminlevel") >= AdminPermissions[permission:lower()] then
				return true
			else
				return false
			end
		else
			assert("Falsches Argument bei 'hasPermission' ( Erwarte AdminPermissionString bei Argument 1, habe nil)")	
		end
	else
		assert(isElement(player), "Falsches Argument bei 'hasPermission' (Erwarte player als Argument 1, habe "..type(player)..")")	
		assert(type(permission) == "string", "Falsches Argument bei 'hasPermission' (Erwarte string als Argument 2, habe "..type(permission)..")")	
	
	end
end

-- Command Funktionen --

function AdminSys.oChat(admin, _, ...)
	if AdminSys.hasPermission(admin, "adminchat") then
		local rangName = AdminSys.getRankName(admin)
		local adminName =  getPlayerName(admin)
		local message = table.concat({...}, " ")
		if message ~= " " then
			outputChatBox ( "[ #FF8000 "..rangName.." "..adminName ..": #FFFFFF "..message.." ]" , getRootElement(), 239, 251, 242, true)
		else
			infoBox(admin, "Bitte gib einen Text ein! /o [Text]", 125, 0, 0, 5000)
		end
	end
end

function AdminSys.aChat(admin, _, ...)
	if AdminSys.hasPermission(admin, "adminchat") then
		local rangName = AdminSys.getRankName(admin)
		local adminName = getPlayerName(admin)
		local message = table.concat({...}, " ")

		for player, _ in pairs(AdminsOnline) do
			outputChatBox ( "[ "..rangName.." "..adminName ..": "..message.." ]" , player, 183, 255, 0, true)
		end
	end
end

function AdminSys.checkIP(admin, _, player)
	if AdminSys.hasPermission(admin, "checkIP") then
		if player then
			if getPlayerFromName(player) then
				outputChatBox("Ip von "..player..": "..getPlayerIP(getPlayerFromName(player)), admin, 0, 125, 0)
			else
				infoBox(admin, "Der Spieler ist nicht online", 125, 0, 0, 5000)
			end
		end
	end
end

function AdminSys.toggleSup(admin, _)
	if AdminSys.hasPermission(admin, "support") then
		local supMode = getElementData(admin, "SupState")
		if not supMode then
			setElementData(admin, "SupState", true)
			outputChatBox("Du bist nun im Support-Modus und empfängst Support-Tickets!", admin, 0, 125, 0)
			AdminsInSupport[admin] = true
		else
			setElementData(admin, "SupState", false)
			outputChatBox("Du hast den Support-Modus verlassen!", admin, 125, 0, 0)
			AdminsInSupport[admin] = false
		end
	end
end

function AdminSys.clearText(admin, _)
	if AdminSys.hasPermission(admin, "clearText") then
		for i = 0, 85 do
			outputChatBox(" ", getRootElement(), 0, 0, 0)
		end
		outputChatBox("Der Chat wurde von #FFFFFF"..getPlayerName(admin).." #FFA500geleert!", getRootElement(), 255, 165, 0, true)
	end
end

function AdminSys.spawnAdminCar(player, _)
	if AdminSys.hasPermission(player, "support") then
		if AdminSys.isInSupMode(player) then
			if AdminCars[player] == nil then
				if not getPedOccupiedVehicle(player) then 
					local x, y, z = getElementPosition(player)
					local veh = createVehicle(411, x, y, z, getElementRotation(player), "Admin")
					setElementData(veh, "Besitzer", "Admins")
					setVehicleColor(veh, 0, 0, 0)
					setVehiclePlateText(veh, "ADMIN")
					setVehicleDamageProof(veh, true)
					setElementAlpha(veh, 100)
					setElementAlpha(player, 100)
					warpPedIntoVehicle(player, veh)
					AdminCars[player] = veh
					addEventHandler("onVehicleExit", veh, AdminSys.destroyAdminCar)
					addEventHandler("onElementDestroy", veh, AdminSys.destroyAdminCar)
					
				else
					infoBox(player, "Du sitzt bereits in einem Auto", 125, 0, 0, 5000)
				end
			else
				infoBox(player, "Du hast bereits ein Admin-Auto!", 125, 0, 0, 5000)
			end
		end
	end
end

function AdminSys.destroyAdminCar(player, seat)
	if seat == 0 then
		local veh = AdminCars[player]
		removeEventHandler("onVehicleExit", veh, AdminSys.destroyAdminCar)
		destroyElement(veh)
		setElementAlpha(player, 255)
		AdminCars[player] = nil
	end
end

function AdminSys.gotoPlayer(admin, _, target)
	if AdminSys.hasPermission(admin, "teleport") then
		if target then
			local target = getPlayerFromName(target)
			if target then
				local veh = getPedOccupiedVehicle(admin)
				if not veh then
					local tX, tY, tZ = getElementPosition(target)
					local tInt, tDim = getElementInterior(target), getElementDimension(target)

					setElementPosition(admin, tX + 2, tY + 2, tZ)
					setElementInterior(admin, tInt)
					setElementDimension(admin, tDim)
					outputChatBox("Admin "..getPlayerName(admin).." hat sich zu dir teleportiert.", target,  0, 125, 0)
					outputChatBox("Du hast dich zu  "..getPlayerName(target).." teleportiert.", admin,  0, 125, 0)
				else
					removePedFromVehicle(admin)
					local tX, tY, tZ = getElementPosition(target)
					local tInt, tDim = getElementInterior(target), getElementDimension(target)
					local tVeh = getPedOccupiedVehicle(target)
					if tVeh then
						warpPedIntoVehicle(admin, tVeh, 1)
					else
						setElementPosition(admin, tX + 2, tY + 2, tZ)
						setElementInterior(admin, tInt)
						setElementDimension(admin, tDim)
						outputChatBox("Admin "..getPlayerName(admin).." hat sich zu dir teleportiert.", target,  0, 125, 0)
						outputChatBox("Du hast dich zu  "..getPlayerName(target).." teleportiert.", admin,  0, 125, 0)
					end

				end
			else
				infoBox(admin, "Der Spieler ist nicht online!", 125, 0, 0, 5000)
			end
		else
			infoBox(admin, "Du musst einen Spier angeben!\n /warpto [Spieler]", 125, 0, 0, 5000)	
		end
	end
end

function AdminSys.getPlayerHere(admin, _, target)
	if AdminSys.hasPermission(admin, "teleport") then
		if target then
			local target = getPlayerFromName(target)
			if target then
				local pX, pY, pZ = getElementPosition(admin)
				local pInt, pDim = getElementInterior(admin), getElementDimension(admin)

				setElementPosition(target, pX + 2, pY + 2, pZ)
				setElementInterior(target, pInt)
				setElementDimension(target, pDim)
				outputChatBox("Du wurdest zum Admin "..getPlayerName(admin).." teleportiert.", target,  0, 125, 0)
				outputChatBox("Du hast "..getPlayerName(target).." zu dir teleportiert.", admin,  0, 125, 0)
			else
				infoBox(admin, "Der Spieler ist nicht online!", 125, 0, 0, 5000)
			end
		else
			infoBox(admin, "Du musst einen Spier angeben!\n /warphere [Spieler]", 125, 0, 0, 5000)	
		end
	end
end

function AdminSys.warp(admin, _, player, target)
	if AdminSys.hasPermission(admin, "teleport") then
		if player and target then
			local player = getPlayerFromName(player)
			local target = getPlayerFromName(target)

			if player and target then
				local tX, tY, tZ = getElementPosition(target)
				local tInt, tDim = getElementInterior(target), getElementDimension(target)

				setElementPosition(player, tX + 2, tY + 2, tZ)
				setElementInterior(player, tInt)
				setElementDimension(player, tDim)

				outputChatBox("Admin "..getPlayerName(admin).." hat "..getPlayerName(player).." zu dir teleportiert.", target,  0, 125, 0)
				outputChatBox("Du wurdest vom Admin "..getPlayerName(admin).." zu "..getPlayerName(target).." teleportiert.", player,  0, 125, 0)
				outputChatBox("Du hast "..getPlayerName(player).." zu "..getPlayerName(target).." teleportiert.", admin,  0, 125, 0)
			else
				infoBox(admin, "Einer oder beide Spieler sind offline!", 125, 0, 0, 5000)
			end
		else
			infoBox(admin, "Du musst zwei Spier angeben!\n /warp [Spieler1] --> [Spieler2]", 125, 0, 0, 5000)	
		end
	end
end

function AdminSys.spectate(admin, _, player)
	if AdminSys.hasPermission(admin, "spectate") then
		if player then
			local player = getPlayerFromName(player)
			if player then
				if not getElementData(admin, "spectate") then
					setCameraTarget(admin, player)
					infoBox(admin, "Du beobachtest nun "..getPlayerName(player).."!", 0, 125, 0, 5000)
					setElementData(admin, "spectate", true)
					bindKey(admin, "space", "down", AdminSys.specExit)
				else
					infoBox(admin, "Du beobachtest bereits einen Spieler!", 125, 0, 0, 5000)
				end
			else
				infoBox(admin, "Der Spieler ist offline!", 125, 0, 0, 5000)
			end

		else
			infoBox(admin, "Du musst einen Spier angeben!\n /spec [Spieler]", 125, 0, 0, 5000)	
		end
	end
end

function AdminSys.specExit(admin, key, state)
	setCameraTarget(admin)
	unbindKey(admin, "space", "down", AdminSys.specExit)
	setElementData(admin, "spectate", false)
	infoBox(admin, "Du hast aufgehört zu beobachten!", 0, 125, 0, 5000)	

end

function AdminSys.showAdmins(player, _)
	table.sort(AdminsOnline)
	for player, rang in pairs(AdminsOnline) do
		outputChatBox(AdminSys.getRankName(player)..":#FF8000    "..getPlayerName(player), player, 255, 0, 0, true )
	end
end

function AdminSys.showKickAndBans(kickedPlayer, typ, reason, response, banTyp)
	if typ == "Kicked" or typ == "Banned" then
		
		local admin = "Server"
		if not isElement(response) then
			admin = HAC.Standarts.AntiCheatName
		else
			admin = getPlayerName(response)
		end
		
		local Kplayer = kickedPlayer
		if isElement(Kplayer) then
			Kplayer = getPlayerName(Kplayer)
		end
		for player, boolean in pairs(AdminsInSupport) do 
			if boolean then
				if typ == "Banned" then
					if banTyp == "temp" then
						outputChatBox("Der Spieler #FFFFFF"..Kplayer.."#FFA500 wurde von #FF0000"..admin.."#FFA500 wegen #FFFFFF"..reason.."#FFA500 vom Server #FFA500temporär gebannt.", player, 255, 165, 0, true)
					else 
						outputChatBox("Der Spieler #FFFFFF"..Kplayer.."#FFA500 wurde von #FF0000"..admin.."#FFA500 wegen #FFFFFF"..reason.."#FFA500 vom Server #FFFFFFpermanent #FFA500gebannt.", player, 255, 165, 0, true)
					end
				elseif typ == "Kicked" then
					outputChatBox("Der Spieler #FFFFFF"..Kplayer.."#FFA500 wurde von #FF0000"..admin.."#FFA500 wegen #FFFFFF"..reason.."#FFA500 vom Server gekickt.", player, 255, 165, 0, true)
				end
			end
		end
	end
end

AdminSys.init()