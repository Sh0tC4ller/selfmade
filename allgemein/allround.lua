﻿

local time = getRealTime()
local hour = time.hour
local minute = time.minute
setTime(hour,minute)
setMinuteDuration(60000)

local gameTyp = setGameType("Hood Reallife")
if not gameTyp then
	outputDebugString("Fehler beim setzen des Game Types", 0, 255, 0, 0)
end

addEventHandler("onPlayerChangeNick",getRootElement(),function ()
	outputChatBox("Du kannst dein Namen nicht ändern!",player,125,0,0)
	cancelEvent()
end)

addEventHandler("onPlayerChat",getRootElement(),function (message,typ)
	if typ == 0 then
		cancelEvent()
		local x,y,z = getElementPosition(source)
		local col = createColSphere(x,y,z,20)
		for _,player in pairs(getElementsWithinColShape(col,"player")) do 
			outputChatBox(getPlayerName(source).." sagt "..message,player,255,255,255)
		end
		destroyElement(col)
	end
end)

addEventHandler("onPlayerWasted",getRootElement(),function ()
	local x,y,z = getElementPosition(source)
	local skin = getElementModel(source)
	spawnPlayer(source,1180.4365234375,-1324.3359375,13.784277915955,0,skin,0,0)
end)

function infoBox(player,text,r,g,b, zeit)
	triggerClientEvent(player,"infoBox",player,text,r,g,b, zeit)
end