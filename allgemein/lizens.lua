
addEventHandler("onVehicleEnter",getRootElement(),function (player,seat)
	if seat == 0 then
		local typ =  getVehicleType(source)
		local model = getElementModel(source)
		if typ == "Helicopter" then
			if tonumber(getElementData(player,"Helicopterschein")) == 0 then
				outputLizensMessage(player)
				return
			end
		elseif typ == "Boat" then
			if tonumber(getElementData(player,"Bootfuehrerschein")) == 0 then
				outputLizensMessage(player)
				return
			end
		end
		if cars[model] then
			if tonumber(getElementData(player,"Autofuehrerschein")) == 0 then
				outputLizensMessage(player)
				return
			end
		elseif bikes[model] then
			if tonumber(getElementData(player,"Motorradschein")) == 0 then
				outputLizensMessage(player)
				return
			end
		elseif lkws[model] then
			if tonumber(getElementData(player,"LKWfuehrerschein")) == 0 then
				outputLizensMessage(player)
				return
			end
		elseif planea[model] then
			if tonumber(getElementData(player,"Leichtfluegschein")) == 0 then
				outputLizensMessage(player)
				return
			end
		elseif planeb[model] then
			if tonumber(getElementData(player,"Schwerfluegschein")) == 0 then
				outputLizensMessage(player)
				return
			end
		elseif model == 531 then
			if tonumber(getElementData(player,"Traktorschein")) == 0 then
				outputLizensMessage(player)
				return
			end
		end
	end
end)

function outputLizensMessage(player)
	outputChatBox("Ich sollte nicht ohne einen Schein fahren...",player,125,0,0)
end

addEvent("lizensBuy",true)
addEventHandler("lizensBuy",getRootElement(),function (lic )
	if client == source then
		if getElementData(source, lic) == 0 then
			if getPlayerMoney(source) >= tonumber(Config.licensePrice[lic]) then
				takePlayerMoney(source,tonumber(Config.licensePrice[lic]))
				setElementData(source,lic,1)
				outputChatBox(("Du hast dir einen %s gekauft!"):format(lic),source,0,125,0)
			else
				outputChatBox(("Du benoetigst %s Dollar fuer diesen Schein!"):format(tostring(Config.licensePrice[lic])),source,125, 0,0)
			end
		else
			outputChatBox(("Du hast bereits einen %s!"):format(lic), source, 125, 0, 0)
		end
	end
end)