

function spawnThePlayer ( player )
	local x,y,z = getElementData(player,"LastX"),getElementData(player,"LastY"),getElementData(player,"LastZ")
	local rot = getElementData(player,"LastRZ")
	local int = tonumber(getElementData(player,"LastInt"))
	local dim = tonumber(getElementData(player,"LastDim"))
	local skin = getElementData(player,"Skin")
	spawnPlayer(player,x,y,z,rot,skin)
	setElementDimension(player,dim)
	setElementInterior(player,int)
	setCameraTarget(player,player)
end

