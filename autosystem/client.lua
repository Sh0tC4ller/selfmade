
local screenW, screenH = guiGetScreenSize()

addEventHandler("onClientRender",getRootElement(),function ()
	for _,veh in pairs(getElementsByType("vehicle"),root,true) do 
		local lx,ly,lz = getPedBonePosition(localPlayer,8)
		local x2,y2,z2 = getElementPosition(getLocalPlayer())
		local x,y,z = getElementPosition(veh)
		local x1,y1 = getScreenFromWorldPosition(x,y,z+0.5)
		if getDistanceBetweenPoints3D(x2,y2,z2,x,y,z) < 10 then
			if x1 and y1 then
				if getElementData(veh,"isCarBuyable") then
					dxDrawText(getVehicleName(veh).."\n".."Kostet: "..tostring(convertNumber(getElementData(veh,"CarPrice"))).."$\nFuer weitere Infos klick mich an",x1,y1,x1,y1,tocolor(255,255,255,255),2,"default-bold","center")
				end
			end
		end
	end
end)

local Autoname = ""
local Autokosten = ""
local Autotyp = ""
local Autocar

addEvent("showCarHouseBuy",true)
addEventHandler("showCarHouseBuy",getRootElement(),function (clickedCar)
	Autoname = getVehicleName(clickedCar)
	Autokosten = tostring(convertNumber(getElementData(clickedCar,"CarPrice")))
	Autotyp = Config.motorTypes[getVehicleHandling(clickedCar)["engineType"]]
	Autocar = clickedCar
	showCursor(true)
	addEventHandler("onClientRender",getRootElement(),renderCarHouse)
	addEventHandler("onClientClick",getRootElement(),onCarHouseMouseMove)
end)

function onCarHouseMouseMove(btn , btnclick)
	if (btn == "left" and btnclick == "down") then
		if isCursorOnElement(((screenW - 204) / 2) - 128, ((screenH - 33) / 2) + 40, 204, 33) then--Buy
			triggerServerEvent("onPlayerBuyCar",getLocalPlayer(),Autocar)
			showCursor(false)
			removeEventHandler("onClientRender",getRootElement(),renderCarHouse)
			removeEventHandler("onClientClick",getRootElement(),onCarHouseMouseMove)
		end
		if isCursorOnElement(((screenW - 204) / 2) + 128, ((screenH - 33) / 2) + 40, 204, 33) then--Abbrechen
			showCursor(false)
			removeEventHandler("onClientRender",getRootElement(),renderCarHouse)
			removeEventHandler("onClientClick",getRootElement(),onCarHouseMouseMove)
		end
	end
end

function renderCarHouse()
	dxDrawRectangle((screenW - 476) / 2, ((screenH - 21) / 2) - 84, 476, 21, tocolor(0, 125, 0, 255), true)
	dxDrawRectangle((screenW - 476) / 2, (screenH - 153) / 2, 476, 153, tocolor(0, 0, 0, 200), true)
	dxDrawRectangle((screenW - 476) / 2, ((screenH - 21) / 2) + 84, 476, 21, tocolor(0, 125, 0, 255), true)
	dxDrawText("Autokauf", (screenW - 475) / 2, ((screenH - 17) / 2) - 84, ((screenW - 475) / 2) + 475, ( ((screenH - 17) / 2) - 84) + 17, tocolor(255, 255, 255, 255), 1.00, "default", "center", "center", false, false, true, false, false)
	dxDrawText("Du möchtest dir gerne ein neues Auto anlegen? Hier bist du genau richtig.\nWir bieten verschiedene Autos an von Klein bis Groß.\nDu schaust dir gerade das Modell "..Autoname.." an. \nDies kostet "..Autokosten.."$ und hat einen "..Autotyp.."motor.", (screenW - 727) / 2 + 140, (screenH - 474) / 2 + 180, (screenW - 1188) / 2 + 280, (screenH - 536) / 2, tocolor(255, 255, 255, 255), 1.00, "default", "left", "top", false, true, true, false, false)
	if isCursorOnElement(((screenW - 204) / 2) - 128, ((screenH - 33) / 2) + 40, 204, 33) then
		dxDrawRectangle(((screenW - 204) / 2) - 128, ((screenH - 33) / 2) + 40, 204, 33, tocolor(0, 0, 0, 255), true)
	else
		dxDrawRectangle(((screenW - 204) / 2) - 128, ((screenH - 33) / 2) + 40, 204, 33, tocolor(0, 125, 0, 255), true)
	end
	if isCursorOnElement(((screenW - 204) / 2) + 128, ((screenH - 33) / 2) + 40, 204, 33) then
		dxDrawRectangle(((screenW - 204) / 2) + 128, ((screenH - 33) / 2) + 40, 204, 33, tocolor(0, 0, 0, 255), true)
	else
		dxDrawRectangle(((screenW - 204) / 2) + 128, ((screenH - 33) / 2) + 40, 204, 33, tocolor(0, 125, 0, 255), true)
	end
	dxDrawText("Kaufen", ((screenW - 205) / 2) - 128, ((screenH - 35) / 2) + 40, (((screenW - 205) / 2) - 128) + 205, ( ((screenH - 35) / 2) + 40) + 35, tocolor(255, 255, 255, 255), 1.00, "default-bold", "center", "center", false, false, true, false, false)
	dxDrawText("Abbrechen", ((screenW - 205) / 2) + 128, ((screenH - 35) / 2) + 40, (((screenW - 205) / 2) + 128) + 205, ( ((screenH - 35) / 2) + 40) + 35, tocolor(255, 255, 255, 255), 1.00, "default-bold", "center", "center", false, false, true, false, false)
end