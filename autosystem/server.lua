userCars = {}

function createAllAutoHausCars()
	for id,carhousetable in pairs(Autohaus) do 
		local count = 0
		for _,cartable in pairs(carhousetable["CarHouseCarSpawns"]) do 
			count = count + 1
			car = createVehicle(cartable[1],cartable[2],cartable[3],cartable[4],0,0,cartable[5])
			if not car then
				outputChatBox(count.." -> "..id)
			end
			setElementInterior(car,0)
			setElementDimension(car,0)
			setVehicleLocked(car,true)
			setElementFrozen(car,true)
			setVehicleDamageProof(car,true)
			setElementData(car,"isCarBuyable",true)
			setElementData(car,"CarPrice",cartable[6])
			setElementData(car,"CarHouseID",id)
		end
	end
end
createAllAutoHausCars()

function createAllCars()
	local sql = dbQuery(Mysql.handler,"SELECT * FROM cars")
	local result, num = dbPoll(sql,-1)
	if (result and num > 0) then
		for _,vehtable in ipairs(result) do
			if not userCars[vehtable["Besitzer"]] then
				userCars[vehtable["Besitzer"]] = {}
			end
			local car = createVehicle(tonumber(vehtable["VehID"]),tonumber(vehtable["X"]),tonumber(vehtable["Y"]),tonumber(vehtable["Z"]),0,0,tonumber(vehtable["RZ"]),vehtable["Besitzer"])
			setElementData(car,"Besitzer",vehtable["Besitzer"])
			setElementData(car,"Slot",tonumber(vehtable["Slot"]))
			local Color = fromJSON(vehtable["Color"])
			setVehicleLocked(car,true)
			setVehicleColor(car,tonumber(Color["R"]),tonumber(Color["G"]),tonumber(Color["B"]),tonumber(Color["R2"]),tonumber(Color["G2"]),tonumber(Color["B2"]))
			userCars[vehtable["Besitzer"]][tonumber(vehtable["Slot"])] = car
		end
	end
	dbFree(sql)
	return true
end

addEventHandler("onResourceStart", root, function ()
	createAllCars()
end)




addCommandHandler("towveh",function (player,cmd,slot)
	local pname = getPlayerName(player)
	if slot then
		if (tonumber(slot)) then
			local slot = tonumber(slot)
			if userCars[pname][tonumber(slot)] then
				destroyElement(userCars[pname][tonumber(slot)])
				userCars[pname][tonumber(slot)] = nil
				local sql = dbQuery(Mysql.handler,"SELECT * FROM cars WHERE Besitzer = ? AND Slot = ?",pname,slot)
				local result = dbPoll(sql,-1)
				if (result and result[1]) then
						local vehtable = result[1]
						local car = createVehicle(tonumber(vehtable["VehID"]),tonumber(vehtable["X"]),tonumber(vehtable["Y"]),tonumber(vehtable["Z"]),0,0,tonumber(vehtable["RZ"]),pname)
						setElementData(car,"Besitzer",pname)
						setElementData(car,"Slot",tonumber(vehtable["Slot"]))
						local Color = fromJSON(vehtable["Color"])
						setVehicleLocked(car,true)
						setVehicleColor(car,tonumber(Color["R"]),tonumber(Color["G"]),tonumber(Color["B"]),tonumber(Color["R2"]),tonumber(Color["G2"]),tonumber(Color["B2"]))
						userCars[pname][tonumber(vehtable["Slot"])]=car
						infoBox(player,"Du hast dein Auto respawnt", 0, 200, 0, 5000)
				else
					infoBox(player,"Du hast kein Fahrzeug in diesem Slot",125,0,0,5000)
				end
				dbFree(sql)
			else
				infoBox(player,"Du hast kein Fahrzeug in diesem Slot",125,0,0)
			end
		else
			infoBox(player, "Syntax:\n\n/towveh [SLOT]",125,0,0,5000)
		end
	else
		infoBox(player, "Syntax:\n\n/towveh [SLOT]",125,0,0,5000)
	end
end)

addCommandHandler("lock",function ( player,cmd,slot)
	local pname = getPlayerName(player)
	if slot then
		if (tonumber(slot)) then
			if userCars[pname][tonumber(slot)] then
				local slot = tonumber(slot)
				setVehicleLocked(userCars[pname][tonumber(slot)] ,not isVehicleLocked(userCars[pname][tonumber(slot)] ))
				if isVehicleLocked(userCars[pname][tonumber(slot)] ) then
					outputChatBox("Du hast dein Auto abgeschlossen!",player,0,125,0)
					blinkLights(userCars[pname][tonumber(slot)], 250 )
				else
					outputChatBox("Du hast dein Auto aufgeschlossen!",player,0,125,0)
					blinkLights(userCars[pname][tonumber(slot)], 250)
				end
			else
				outputChatBox("Du hast kein Fahrzeug in diesem Slot",player,125,0,0)
			end
		else
			outputChatBox("Syntax: /lock [Slot]",player,125,0,0)
		end
	else
		outputChatBox("Syntax: /lock [Slot]",player,125,0,0)
	end
end)

function blinkLights(car, duration)
	if getElementData(car, "Light") then
		setVehicleOverrideLights(car, 1)
		setTimer(function()
			setVehicleOverrideLights(car, 2)
		end, duration, 1)
	else
		setVehicleOverrideLights(car, 2)
		setTimer(function()
			setVehicleOverrideLights(car, 1)
		end, duration, 1)
	end
end


addEventHandler("onVehicleEnter",getRootElement(),function ( player,seat) 
	if not getElementData(source,"Engine") then
		setElementData(source,"Engine",false)
		setElementData(source,"Light",false)
	end
	setVehicleEngineState(source,getElementData(source,"Engine"))
	if seat == 0 then
		bindKey(player,"X","down",vehicleMotor,player)
		bindKey(player,"L","down",vehicleLight,player)
	end
end)

addEventHandler("onVehicleStartExit",getRootElement(),function (player,seat)
	if seat == 0 then
		unbindKey(player,"X","down",vehicleMotor,player)
		unbindKey(player,"L","down",vehicleLight,player)
	end
end)

function vehicleMotor (player)
	local car = getPedOccupiedVehicle(player)
	if car then
		if (getElementData(car,"Besitzer")) then
			if (getElementData(car,"Besitzer") == getPlayerName(player) or getElementData(car, "Besitzer") == Config.fraktionNamen[getElementData(player, "Fraktion")]) or getElementData(car, "Besitzer") == "Admins"then
				setElementData(car,"Engine",not getElementData(car,"Engine"))
				setVehicleEngineState(car,getElementData(car,"Engine"))
			end
		else
			setElementData(car,"Engine",not getElementData(car,"Engine"))
			setVehicleEngineState(car,getElementData(car,"Engine"))
		end
	end
end

function vehicleLight ( player )
	local car = getPedOccupiedVehicle(player)
	if car then
		setElementData(car,"Light",not getElementData(car,"Light"))
		if getElementData(car,"Light") then
			setVehicleOverrideLights(car,2)
		else
			setVehicleOverrideLights(car,1)
		end
	end
end

function getFreeSlot (player)
	local pname = getPlayerName(player)
	local isFound = false
	for i = 1,5 do 
		local sql = dbQuery(Mysql.handler,"SELECT * FROM cars WHERE Besitzer = ? AND Slot = ?",pname,i)
		local result,num = dbPoll(sql,-1)
		if (num == 0 ) then
			isFound = i
			break
		end
		dbFree(sql)
	end
	return isFound
end

addEvent("onPlayerBuyCar",true)
addEventHandler("onPlayerBuyCar",getRootElement(),function ( car)
	local pname = getPlayerName(source)
	local CarHouseID = tonumber(getElementData(car,"CarHouseID"))
	local x,y,z,rz = Autohaus[CarHouseID]["CarHouseSpawnPositon"][1],Autohaus[CarHouseID]["CarHouseSpawnPositon"][2],Autohaus[CarHouseID]["CarHouseSpawnPositon"][3],Autohaus[CarHouseID]["CarHouseSpawnPositon"][4]
	local Price = tonumber(getElementData(car,"CarPrice"))
	if (getPlayerMoney(source) >= Price ) then
		if (getFreeSlot(source)) then
			takePlayerMoney(source,Price)
			local r,g,b,r2,g2,b2 = math.random(1,255),math.random(1,255),math.random(1,255),math.random(1,255),math.random(1,255),math.random(1,255)
			local ColorTable = {["R"] = r,["G"] = g,["B"] = b, ["R2"] = r2, ["G2"] = g2, ["B2"] = b2}
			local Color = toJSON(ColorTable)
			local Slot = getFreeSlot(source)
			local ID = getElementModel(car)
			if dbExec(Mysql.handler,"INSERT INTO `cars` (`ID`, `Besitzer`, `VehID`, `X`, `Y`, `Z`, `RZ`, `Slot`, `Color`) VALUES (NULL, ?, ?, ?, ?, ?, ?, ?, ?);",pname,ID,x,y,z,rz,Slot,Color) then
				local car = createVehicle(ID,x,y,z,0,0,rz,pname)
				setElementData(car,"Besitzer",pname)
				setElementData(car,"Slot",Slot)
				local Color = fromJSON(Color)
				setVehicleLocked(car,true)
				setVehicleColor(car,tonumber(Color["R"]),tonumber(Color["G"]),tonumber(Color["B"]),tonumber(Color["R2"]),tonumber(Color["G2"]),tonumber(Color["B2"]))
				if not userCars[source] then
					userCars[source] = {}
				end
				userCars[source][Slot]=car
				warpPedIntoVehicle(source,car,0)
			end
		else
			outputChatBox("Du hast kein Slot mehr frei!",source,125,0,0)
		end
	else
		outputChatBox("Du benoetigst "..tostring(Price).." Dollar fuer diesen Fahrzeug!",source,125,0,0)
	end
end)

addCommandHandler("carinfo",function (player)
	if userCars[player] then
		for i = 1,5 do 
			if userCars[player][i] then
				local x,y,z = getElementPosition(userCars[player][i])
				outputChatBox("Slot "..tostring(i).." , "..getVehicleName(userCars[player][i]).." befindet sich in "..getZoneName(x,y,z).." ( "..getZoneName(x,y,z,true).." )",player,0,125,0)
			end
		end
	else
		outputChatBox("Du hast keine Autos",player,125,0,0)
	end
end)

addCommandHandler("park",function ( player )
	local pname = getPlayerName(player)
	local car = getPedOccupiedVehicle(player)
	if car then
		if (getElementData(car,"Besitzer")) then
			if (getElementData(car,"Besitzer") == getPlayerName(player)) then
				local x,y,z = getElementPosition(car)
				local _,_,rz = getElementRotation(car)
				dbExec(Mysql.handler,"UPDATE `cars` SET `X` = ?, `Y` = ?, `Z` = ?, `RZ` = ? WHERE Besitzer = ? AND Slot = ?",x,y,z,rz,pname,getElementData(car,"Slot"))
				outputChatBox("Du hast das Auto erfolgreich geparkt!",player,0,125,0)
			else
				outputChatBox("Das Auto gehoert nicht dir!",player,125,0,0)
			end
		else
			outputChatBox("Das Auto gehoert nicht dir!",player,125,0,0)
		end
	else
		outputChatBox("Du sitzt in keinem Fahrzeug!",player,125,0,0)
	end
end)