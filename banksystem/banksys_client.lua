

local screenW, screenH = guiGetScreenSize()

local currentScroll = 0
local currentTab = "home"
local summeText = ""
local summeEmpfaenger = ""
local summeBetrag = ""
local summeVerwendung = ""
local BankSummEdit = guiCreateEdit((screenW - 279) / 2 + 40, (screenH - 28) / 2 - 60, 279, 28, "", false)
local KontoEmpfaenger = guiCreateEdit((screenW - 476) / 2- 12, (screenH - 22) / 2 - 107, 476, 22, "", false)
local KontoBetrag  = guiCreateEdit((screenW - 205) / 2 + 120, (screenH - 19) / 2 - 20, 205, 19, "", false)
local KontoVerwendung = guiCreateEdit((screenW - 473) / 2 - 15, (screenH - 23) / 2 + 15, 473, 23, "", false)
local ausZugData = {}
guiSetVisible(BankSummEdit,false)
guiSetVisible(KontoEmpfaenger,false)
guiSetVisible(KontoBetrag,false)
guiSetVisible(KontoVerwendung,false)
guiEditSetMaxLength(BankSummEdit,15)

function renderBank()
	dxDrawRectangle((screenW - 537) / 2, (screenH - 21) / 2 - 192, 537, 21, tocolor(0, 125, 0, 255), true)
	dxDrawRectangle((screenW - 537) / 2, (screenH - 365) / 2, 537, 365, tocolor(0, 0, 0, 200), false)
	dxDrawRectangle((screenW - 537) / 2, (screenH - 21) / 2 + 192, 537, 21, tocolor(0, 125, 0, 255), true)
	dxDrawText("Bankautomat", (screenW - 537) / 2, (screenH - 20) / 2 -192, ((screenW - 537) / 2) + 537, ( (screenH - 20) / 2 -192) + 20, tocolor(255, 255, 255, 255), 1.00, "default-bold", "center", "center", false, false, true, false, false)
	dxDrawRectangle((screenW - 536) / 2, (screenH - 42) / 2 - 160, 536, 42, tocolor(0, 0, 0, 210), true)
	if isCursorOnElement((screenW - 95) / 2 - 215, (screenH - 27) / 2 - 165, 95, 27) then
		dxDrawRectangle((screenW - 95) / 2 - 215, (screenH - 27) / 2 - 165, 95, 27, tocolor(0, 0, 0, 255), true)
	else
		dxDrawRectangle((screenW - 95) / 2 - 215, (screenH - 27) / 2 - 165, 95, 27, tocolor(0, 125, 0, 255), true)
	end
	if isCursorOnElement((screenW - 95) / 2 -5, (screenH - 27) / 2 - 165, 95, 27) then
		dxDrawRectangle((screenW - 95) / 2 -5, (screenH - 27) / 2 - 165, 95, 27, tocolor(0, 0, 0, 255), true)
	else
		dxDrawRectangle((screenW - 95) / 2 -5, (screenH - 27) / 2 - 165, 95, 27, tocolor(0, 125, 0, 255), true)
	end
	if isCursorOnElement((screenW - 95) / 2 -110, (screenH - 27) / 2 - 165, 95, 27) then
		dxDrawRectangle((screenW - 95) / 2 -110, (screenH - 27) / 2 - 165, 95, 27, tocolor(0, 0, 0, 255), true)
	else
		dxDrawRectangle((screenW - 95) / 2 -110, (screenH - 27) / 2 - 165, 95, 27, tocolor(0, 125, 0, 255), true)
	end
	if isCursorOnElement((screenW - 95) / 2 + 100, (screenH - 27) / 2 - 165, 95, 27) then
		dxDrawRectangle((screenW - 95) / 2 + 100, (screenH - 27) / 2 - 165, 95, 27, tocolor(0, 0, 0, 255), true)
	else
		dxDrawRectangle((screenW - 95) / 2 + 100, (screenH - 27) / 2 - 165, 95, 27, tocolor(0, 125, 0, 255), true)
	end
	if isCursorOnElement((screenW - 95) / 2 + 205, (screenH - 27) / 2 - 165, 95, 27) then
		dxDrawRectangle((screenW - 95) / 2 + 205, (screenH - 27) / 2 - 165, 95, 27, tocolor(0, 0, 0, 255), true)
	else
		dxDrawRectangle((screenW - 95) / 2 + 205, (screenH - 27) / 2 - 165, 95, 27, tocolor(0, 125, 0, 255), true)
	end
	dxDrawText("Ein/Auszahlen", (screenW - 94) / 2 - 110, (screenH - 28) / 2 - 165, ((screenW - 94) / 2 - 110) + 94, ( (screenH - 28) / 2 - 165) + 28, tocolor(255, 255, 255, 255), 1.00, "default-bold", "center", "center", false, false, true, false, false)
	dxDrawText("Home", (screenW - 94) / 2 - 215, (screenH - 28) / 2 - 165, ((screenW - 94) / 2 - 215) + 94, ( (screenH - 28) / 2 - 165) + 28, tocolor(255, 255, 255, 255), 1.00, "default-bold", "center", "center", false, false, true, false, false)
	dxDrawText("Ueberweisen", (screenW - 94) / 2 - 5, (screenH - 28) / 2 - 165, ((screenW - 94) / 2 - 5) + 94, ( (screenH - 28) / 2 - 165) + 28, tocolor(255, 255, 255, 255), 1.00, "default-bold", "center", "center", false, false, true, false, false)
	dxDrawText("Kontoauszug", (screenW - 94) / 2 + 100, (screenH - 28) / 2 - 165, ((screenW - 94) / 2 + 100) + 94, ( (screenH - 28) / 2 - 165) + 28, tocolor(255, 255, 255, 255), 1.00, "default-bold", "center", "center", false, false, true, false, false)
	dxDrawText("Schliessen", (screenW - 94) / 2 + 205, (screenH - 28) / 2 - 165, ((screenW - 94) / 2 + 205) + 94, ( (screenH - 28) / 2 - 165) + 28, tocolor(255, 255, 255, 255), 1.00, "default-bold", "center", "center", false, false, true, false, false)
	if (currentTab == "home") then
		dxDrawImage((screenW - 247) / 2, (screenH - 179) / 2, 247, 179, "images/bank_1.png", 0, 0, 0, tocolor(255, 255, 255, 255), false)
		dxDrawText("Willkommen in der Bank of San Andereas. Hier können sie ihre Ein und Auszahlungen,Überweisungen und ihren Kontoauszug einsehen.", (screenW - 489) / 2, (screenH - 37) / 2 + 120, ((screenW - 489) / 2) + 489, ( (screenH - 37) / 2 + 120) + 37, tocolor(255, 255, 255, 255), 1.00, "default-bold", "left", "top", false, true, false, false, false)
	elseif (currentTab == "einzahlen") then
		dxDrawText("Ihr zurzeitiges auf vollstaendige gerundetes Guthaben betraegt: \n#00FF00"..string.gsub(convertNumber(getElementData(localPlayer,"Bankgeld")), ",", ".").."$", (screenW - 434) / 2, (screenH - 37) / 2 - 110, ((screenW - 434) / 2) + 434, ( (screenH - 37) / 2 - 110) + 37, tocolor(255, 255, 255, 255), 1.00, "default", "center", "top", false, false, true, true, false)
        dxDrawText("Betrag:", (screenW - 111) / 2 - 160, (screenH - 27) / 2 - 60, ((screenW - 111) / 2 - 160) + 111, ( (screenH - 27) / 2 - 60) + 27, tocolor(255, 255, 255, 255), 1.00, "default", "center", "center", false, false, true, false, false)
        dxDrawRectangle((screenW - 278) / 2 + 40, (screenH - 28) / 2 - 60, 278, 28, tocolor(255, 255, 255, 255), true)
        dxDrawText(summeText.."$", (screenW - 278) / 2 + 45, (screenH - 23) / 2 -60, ((screenW - 278) / 2 + 45) + 278, ( (screenH - 23) / 2 -60) + 23, tocolor(0, 0, 0, 255), 1.00, "default", "left", "center", false, false, true, false, false)
		if isCursorOnElement((screenW - 124) / 2 - 80, (screenH - 31) / 2 , 124, 31) then
			dxDrawRectangle((screenW - 124) / 2 - 80, (screenH - 31) / 2 , 124, 31, tocolor(0, 0, 0, 255), true)
		else
			 dxDrawRectangle((screenW - 124) / 2 - 80, (screenH - 31) / 2 , 124, 31, tocolor(0, 125, 0, 255), true)
		end
		if isCursorOnElement((screenW - 124) / 2 + 80, (screenH - 31) / 2 , 124, 31) then
			dxDrawRectangle((screenW - 124) / 2 + 80, (screenH - 31) / 2 , 124, 31, tocolor(0, 0, 0, 255), true)
		else
			dxDrawRectangle((screenW - 124) / 2 + 80, (screenH - 31) / 2 , 124, 31, tocolor(0, 125, 0, 255), true)
		end
        dxDrawText("Einzahlen", (screenW - 125) / 2 - 80, (screenH - 27) / 2, ((screenW - 125) / 2 - 80) + 125, ( (screenH - 27) / 2) + 27, tocolor(255, 255, 255, 255), 1.00, "default-bold", "center", "center", false, false, true, false, false)
        dxDrawText("Auszahlen", (screenW - 125) / 2 + 80, (screenH - 27) / 2, ((screenW - 125) / 2 + 80) + 125, ( (screenH - 27) / 2) + 27, tocolor(255, 255, 255, 255), 1.00, "default-bold", "center", "center", false, false, true, false, false)
	elseif (currentTab == "ueberweisen") then
		dxDrawImage((screenW - 515) / 2, (screenH - 269) / 2 - 5, 515, 269, "images/bank_3.png", 0, 0, 0, tocolor(255, 255, 255, 255), false)
		if isCursorOnElement((screenW - 144) / 2, (screenH - 29) / 2 + 155, 144, 29) then
			dxDrawRectangle((screenW - 144) / 2, (screenH - 29) / 2 + 155, 144, 29, tocolor(0, 0, 0, 255), true)
		else
			  dxDrawRectangle((screenW - 144) / 2, (screenH - 29) / 2 + 155, 144, 29, tocolor(0, 125, 0, 255), true)
		end
        dxDrawText("Ueberweisen!", (screenW - 144) / 2, (screenH - 29) / 2 +155, ((screenW - 144) / 2) + 144, ( (screenH - 29) / 2 +155) + 29, tocolor(255, 255, 255, 255), 1.00, "default-bold", "center", "center", false, false, true, false, false)
        dxDrawRectangle((screenW - 475) / 2 - 12, (screenH - 22) / 2 - 107, 475, 22, tocolor(255, 255, 255, 255), true)
        dxDrawText(summeEmpfaenger, (screenW - 474) / 2 - 12, (screenH - 16) / 2 - 107, ((screenW - 474) / 2 - 12) + 474, ( (screenH - 16) / 2 - 107) + 16, tocolor(0, 0, 0, 255), 1.00, "default", "left", "top", false, false, true, false, false)
        dxDrawText("5646454", (screenW - 169) / 2 - 165, (screenH - 19) / 2 - 80, ((screenW - 169) / 2 - 165) + 169, ( (screenH - 19) / 2 - 80) + 19, tocolor(0, 0, 0, 255), 1.00, "default", "left", "center", false, false, true, false, false)
        dxDrawText("544545456", (screenW - 169) / 2 + 175, (screenH - 19) / 2 - 80, ((screenW - 169) / 2 + 175) + 169, ( (screenH - 19) / 2 - 80) + 19, tocolor(0, 0, 0, 255), 1.00, "default", "left", "center", false, false, true, false, false)
        dxDrawText("Bank of San Andereas", (screenW - 169) / 2 - 165, (screenH - 19) / 2 - 50, ((screenW - 169) / 2 - 165) + 169, ( (screenH - 19) / 2 - 50) + 19, tocolor(0, 0, 0, 255), 1.00, "default", "left", "center", false, false, true, false, false)
        dxDrawRectangle((screenW - 206) / 2 + 120, (screenH - 20) / 2 - 20, 206, 20, tocolor(255, 255, 255, 255), true)
        dxDrawText(summeBetrag, (screenW - 205) / 2 + 120, (screenH - 20) / 2 - 20, ((screenW - 205) / 2 + 120) + 205, ( (screenH - 20) / 2 - 20) + 20, tocolor(0, 0, 0, 255), 1.00, "default", "left", "center", false, false, true, false, false)
        dxDrawRectangle((screenW - 472) / 2 -15, (screenH - 25) / 2 + 15, 472, 25, tocolor(255, 255, 255, 255), true)
        dxDrawText(summeVerwendung, (screenW - 472) / 2 -15, (screenH - 25) / 2 + 15, ((screenW - 472) / 2 -15) + 472, ( (screenH - 25) / 2 + 15) + 25, tocolor(0, 0, 0, 255), 1.00, "default", "left", "center", false, false, true, false, false)
        dxDrawText(getPlayerName(localPlayer), (screenW - 476) / 2 - 10, (screenH - 23) / 2 + 74, ((screenW - 476) / 2 - 10) + 476, ( (screenH - 23) / 2 + 74) + 23, tocolor(0, 0, 0, 255), 1.00, "default", "left", "center", false, false, true, false, false)
        dxDrawText("556565665", (screenW - 172) / 2 - 160, (screenH - 21) / 2 + 108, ((screenW - 172) / 2 - 160) + 172, ( (screenH - 21) / 2 + 108) + 21, tocolor(0, 0, 0, 255), 1.00, "default", "left", "center", false, false, true, false, false)
	elseif (currentTab == "kontoauszug") then
		dxDrawRectangle((screenW - 518) / 2, (screenH - 291) / 2 + 20, 518, 291, tocolor(0, 0, 0, 180), true)
        dxDrawLine((screenW - 0) / 2 - 105,  (screenH - 290) / 2 + 20, ((screenW - 0) / 2 - 105) + 0, ( (screenH - 290) / 2 + 20) + 290, tocolor(255, 255, 255, 255), 1, true)
        dxDrawText("Uhr - Datum", (screenW - 140) / 2 - 25, (screenH - 21) / 2 - 115, ((screenW - 140) / 2 - 25) + 140, ( (screenH - 21) / 2 - 115) + 21, tocolor(255, 255, 255, 255), 1.00, "default", "center", "center", false, false, true, false, false)
        dxDrawLine((screenW - 0) / 2 + 55,  (screenH - 290) / 2 + 20, ((screenW - 0) / 2 + 55) + 0, ( (screenH - 290) / 2 + 20) + 290, tocolor(255, 255, 255, 255), 1, true)
        dxDrawLine((screenW - 0) / 2 + 160,  (screenH - 290) / 2 + 20, ((screenW - 0) / 2 + 160) + 0, ( (screenH - 290) / 2 + 20) + 290, tocolor(255, 255, 255, 255), 1, true)
        dxDrawLine((screenW - 515) / 2,  (screenH - 0) / 2 - 100, ((screenW - 515) / 2) + 515, ( (screenH - 0) / 2 - 100) + 0, tocolor(255, 255, 255, 255), 1, true)
        dxDrawText("Verwendung", (screenW - 140) / 2 - 185, (screenH - 21) / 2 - 115, ((screenW - 140) / 2 - 185) + 140, ( (screenH - 21) / 2 - 115) + 21, tocolor(255, 255, 255, 255), 1.00, "default", "center", "center", false, false, true, false, false)
        dxDrawText("Von", (screenW - 88) / 2 + 108, (screenH - 21) / 2 - 115, ((screenW - 88) / 2 + 108) + 88, ( (screenH - 21) / 2 - 115) + 21, tocolor(255, 255, 255, 255), 1.00, "default", "center", "center", false, false, true, false, false)
        dxDrawText("Betrag", (screenW - 88) / 2 + 210, (screenH - 21) / 2 - 115, ((screenW - 88) / 2 + 210) + 88, ( (screenH - 21) / 2 - 115) + 21, tocolor(255, 255, 255, 255), 1.00, "default", "center", "center", false, false, true, false, false)
		local count = 0
		local count2 = -20
		for i = 1+currentScroll,13+currentScroll do 
			local font
			if kontoAusZugData[i] then
				count = count + 40
				count2 = count2 + 20
				if isCursorOnElement( (screenW - 515) / 2, (screenH - 13) / 2 -85 + count2 , 515, 13 ) then
					font = 'default-bold'
				else
					font = 'default'
				end
				dxDrawText(kontoAusZugData[i][1], (screenW - 140) / 2 - 185, (screenH - 21) / 2 - 95 + count, ((screenW - 140) / 2 - 185) + 140, ( (screenH - 21) / 2 - 115) + 21, tocolor(255, 255, 255, 255), 1.00, font, "center", "center", false, false, true, false, false)
				dxDrawText(kontoAusZugData[i][3], (screenW - 140) / 2 - 25, (screenH - 21) / 2 - 95 + count, ((screenW - 140) / 2 - 25) + 140, ( (screenH - 21) / 2 - 115) + 21, tocolor(255, 255, 255, 255), 1.00, font, "center", "center", false, false, true, false, false)
				dxDrawText(kontoAusZugData[i][4], (screenW - 88) / 2 + 108, (screenH - 21) / 2 - 95 + count, ((screenW - 88) / 2 + 108) + 88, ( (screenH - 21) / 2 - 115) + 21, tocolor(255, 255, 255, 255), 1.00, font, "center", "center", false, false, true, false, false)
				dxDrawText(kontoAusZugData[i][5], (screenW - 88) / 2 + 210, (screenH - 21) / 2 - 95 + count, ((screenW - 88) / 2 + 210) + 88, ( (screenH - 21) / 2 - 115) + 21, tocolor(255, 255, 255, 255), 1.00, font, "center", "center", false, false, true, false, false)
			end
		end
	end
end

function ausZugRender()
	dxDrawRectangle((screenW - 435) / 2, (screenH - 21) / 2 - 156, 435, 21, tocolor(0, 125, 0, 255), true)
	dxDrawRectangle((screenW - 435) / 2, (screenH - 292) / 2, 435, 292, tocolor(0, 0, 0, 170), true)
	dxDrawRectangle((screenW - 435) / 2, (screenH - 21) / 2 + 156, 435, 21, tocolor(0, 125, 0, 255), true)
	dxDrawText(ausZugData[1], (screenW - 435) / 2, (screenH - 20) / 2 - 157, ((screenW - 435) / 2) + 435, ( (screenH - 20) / 2 - 157) + 20, tocolor(255, 255, 255, 255), 1.00, "default-bold", "center", "center", false, false, true, false, false)
	dxDrawText("Datum: "..ausZugData[3].."\nVon: "..ausZugData[4].."\nBetrag: "..getRightColor(ausZugData[5]).."\nVerwendungszweck:\n"..ausZugData[2], (screenW - 415) / 2, (screenH - 240) / 2 - 20, ((screenW - 415) / 2) + 415, ( (screenH - 240) / 2 - 20) + 240, tocolor(255, 255, 255, 255), 1.00, "default", "left", "top", false, false, true, true, false)
	if isCursorOnElement((screenW - 145) / 2, (screenH - 28) / 2 + 120, 145, 28) then
		dxDrawRectangle((screenW - 145) / 2, (screenH - 28) / 2 + 120, 145, 28, tocolor(0, 0, 0, 255), true)
	else
		dxDrawRectangle((screenW - 145) / 2, (screenH - 28) / 2 + 120, 145, 28, tocolor(0, 125, 0, 255), true)
	end
	dxDrawText("Schliessen", (screenW - 145) / 2, (screenH - 28) / 2 + 120, ((screenW - 145) / 2) + 145, ( (screenH - 28) / 2 + 120) + 28, tocolor(255, 255, 255, 255), 1.00, "default-bold", "center", "center", false, false, true, false, false)
end

function round(num, idp)
	local mult = 10^(idp or 0)
	return math.floor(num * mult + 0.5) / mult
end

function syncEditBoxWithDXBank()
	if (source == BankSummEdit) then
		summeText = guiGetText(BankSummEdit)
	elseif (source == KontoEmpfaenger) then
		summeEmpfaenger = guiGetText(KontoEmpfaenger)
	elseif (source == KontoBetrag) then
		summeBetrag = guiGetText(KontoBetrag)
	elseif (source == KontoVerwendung) then
		summeVerwendung = guiGetText(KontoVerwendung)
	end
end

function onBankClick(btn,state)
	if btn == "left" and state == "down" then
		if isCursorOnElement((screenW - 95) / 2 - 215, (screenH - 27) / 2 - 165, 95, 27) then
			currentTab = "home"
			guiSetVisible(BankSummEdit,false)
			guiSetVisible(KontoEmpfaenger,false)
			guiSetVisible(KontoBetrag,false)
			guiSetVisible(KontoVerwendung,false)
		end
		if isCursorOnElement((screenW - 95) / 2 -5, (screenH - 27) / 2 - 165, 95, 27) then--Ueberweisen
			currentTab = "ueberweisen"
			guiSetVisible(BankSummEdit,false)
			guiSetVisible(KontoEmpfaenger,true)
			guiSetVisible(KontoBetrag,true)
			guiSetVisible(KontoVerwendung,true)
		end
		if isCursorOnElement((screenW - 95) / 2 -110, (screenH - 27) / 2 - 165, 95, 27) then--Ein/Auszahlen
			currentTab = "einzahlen"
			guiSetVisible(KontoEmpfaenger,false)
			guiSetVisible(KontoBetrag,false)
			guiSetVisible(KontoVerwendung,false)
			guiSetVisible(BankSummEdit,true)
		end
		if isCursorOnElement((screenW - 95) / 2 + 100, (screenH - 27) / 2 - 165, 95, 27) then--Kontoauszug
			currentTab = "kontoauszug"
			guiSetVisible(BankSummEdit,false)
			guiSetVisible(KontoEmpfaenger,false)
			guiSetVisible(KontoBetrag,false)
			guiSetVisible(KontoVerwendung,false)
		end
		if isCursorOnElement((screenW - 95) / 2 + 205, (screenH - 27) / 2 - 165, 95, 27) then--Schliessen
			removeEventHandler("onClientRender",getRootElement(),renderBank)
			removeEventHandler("onClientClick",getRootElement(),onBankClick)
			removeEventHandler("onClientGUIChanged",getRootElement(),syncEditBoxWithDXBank)
			unbindKey("mouse_wheel_down","down",scrollDownBank)
			unbindKey("mouse_wheel_up","down",scrollUPBank)
			showCursor(false)
			guiSetVisible(BankSummEdit,false)
			guiSetVisible(KontoEmpfaenger,false)
			guiSetVisible(KontoBetrag,false)
			guiSetVisible(KontoVerwendung,false)
			setTimer(setElementData, 2000, 1, getLocalPlayer(), "inGui", false)
		end
		if (currentTab == "einzahlen") then
			if isCursorOnElement((screenW - 124) / 2 - 80, (screenH - 31) / 2 , 124, 31) then--Einzahlen
				triggerServerEvent("BankEinzahlen",localPlayer,tonumber(summeText))
			end
			if isCursorOnElement((screenW - 124) / 2 + 80, (screenH - 31) / 2 , 124, 31) then--Auszahlen
				triggerServerEvent("BankAuszahlen",localPlayer,tonumber(summeText))
			end
		end
		if (currentTab == "showauszug") then
			if isCursorOnElement((screenW - 145) / 2, (screenH - 28) / 2 + 120, 145, 28) then
				removeEventHandler("onClientRender",getRootElement(),ausZugRender)
				addEventHandler("onClientRender",getRootElement(),renderBank)
				currentTab = "kontoauszug"
			end
		end
		if (currentTab == "ueberweisen") then
			if isCursorOnElement((screenW - 144) / 2, (screenH - 29) / 2 + 155, 144, 29) then
				triggerServerEvent("BankUeberweiseung", localPlayer, summeEmpfaenger, summeBetrag, summeVerwendung)
			end
		end
		if (currentTab == "kontoauszug" ) then
			local count2 = -20
			for i = 1+currentScroll,13+currentScroll do 
				local font
				if kontoAusZugData[i] then
					count2 = count2 + 20
					if isCursorOnElement( (screenW - 515) / 2, (screenH - 13) / 2 -85 + count2 , 515, 13 ) then
						ausZugData = kontoAusZugData[i]
						addEventHandler("onClientRender",getRootElement(),ausZugRender)
						removeEventHandler("onClientRender",getRootElement(),renderBank)
						currentTab = "showauszug"
					end
				end
			end
		end
	end
end

addEvent("showBank", true )	
addEventHandler("showBank",getRootElement(),function ( )
	addEventHandler("onClientRender",getRootElement(),renderBank)
	addEventHandler("onClientClick",getRootElement(),onBankClick)
	addEventHandler("onClientGUIChanged",getRootElement(),syncEditBoxWithDXBank)
	bindKey("mouse_wheel_down","down",scrollDownBank)
	bindKey("mouse_wheel_up","down",scrollUPBank)
	setElementData(source, "inGui", true)
	showCursor(true)
end)

function scrollUPBank()
	if (currentScroll == 0) then
	else
		currentScroll = currentScroll -1
	end
end

function scrollDownBank()
	if (currentScroll == #kontoAusZugData + 1) then
	else
		currentScroll = currentScroll +1
	end
end

kontoAusZugData = {}

addEvent("newKontoauszug",true)
addEventHandler("newKontoauszug",getRootElement(),function (title,von,betrag,verwendungszweck)
	local xml = xmlLoadFile("bank.xml")
	local node = xmlCreateChild(xml,"line")
	xmlNodeSetAttribute(node,"Title",title)
	xmlNodeSetAttribute(node,"Verwendungszweck",verwendungszweck)
	xmlNodeSetAttribute(node,"Datum",getRealStap())
	xmlNodeSetAttribute(node,"Von",von)
	xmlNodeSetAttribute(node,"Betrag",betrag)
	xmlSaveFile(xml)
	xmlUnloadFile(xml)
	loadKontoAuszug()
end)


if (not fileExists("bank.xml")) then
	local xml = xmlCreateFile("bank.xml","nodes")
	xmlSaveFile(xml)
	xmlUnloadFile(xml)
end

function loadKontoAuszug()
	kontoAusZugData = nil
	kontoAusZugData = {}
	local xml = xmlLoadFile("bank.xml")
	for _,node in pairs(xmlNodeGetChildren(xml)) do 
		table.insert(kontoAusZugData,{xmlNodeGetAttribute(node,"Title"),xmlNodeGetAttribute(node,"Verwendungszweck"),xmlNodeGetAttribute(node,"Datum"),xmlNodeGetAttribute(node,"Von"),xmlNodeGetAttribute(node,"Betrag")})
	end
	local tmpTable = kontoAusZugData
	kontoAusZugData = nil
	kontoAusZugData = {}
	local i = #tmpTable
	while(true) do 
		if i == 0 then
			break
		end
		table.insert(kontoAusZugData,tmpTable[i])
		i = i -1
	end
end
loadKontoAuszug()