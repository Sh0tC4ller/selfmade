﻿

addEvent("BankEinzahlen",true)
addEventHandler("BankEinzahlen",getRootElement(),function ( summe )
	if tonumber(summe) then
		local summe = tonumber(summe)
		local summe = math.floor(summe)
		local summe = math.abs(summe)
		if getElementData(source,"Geld") >= summe then
			setElementData(source,"Geld" , getElementData(source,"Geld") - summe )
			setElementData(source,"Bankgeld", getElementData(source,"Bankgeld") + summe)
			outputChatBox("Du hast "..tostring(summe).."$ eingezahlt!",source,0,125,0)
		else
			outputChatBox("Du hast nicht genügend Geld!",source,125,0,0)
		end
	end
end)

addEvent("BankAuszahlen",true)
addEventHandler("BankAuszahlen",getRootElement(),function ( summe )
	if tonumber(summe) then
		local summe = tonumber(summe)
		local summe = math.floor(summe)
		local summe = math.abs(summe)
		if getElementData(source,"Bankgeld") >= summe then
			setElementData(source,"Geld" , getElementData(source,"Geld") + summe )
			setElementData(source,"Bankgeld", getElementData(source,"Bankgeld") - summe)
			outputChatBox("Du hast "..tostring(summe).."$ ausgezahlt!",source,0,125,0)
		else
			outputChatBox("Du hast nicht genügend Geld!",source,125,0,0)
		end
	end
end)

addEvent("BankUeberweiseung",true)
addEventHandler("BankUeberweiseung",getRootElement(),function (target,summe,grund)
	if getPlayerFromName(target) then
		if tonumber(summe) then
			local summe = tonumber(summe)
			local summe = math.floor(summe)
			local summe = math.abs(summe)
			local target = getPlayerFromName(target)
			if source == target then
				outputChatBox("Du kannst dir nicht selbst überweisen!",source,125,0,0)
				return
			end
			if getElementData(source,"Bankgeld") >= summe then
				setElementData(source,"Bankgeld", getElementData(source,"Bankgeld") - summe)
				setElementData(target,"Bankgeld", getElementData(target,"Bankgeld") + summe)
				triggerClientEvent(target,"newKontoauszug",target,"Überweisung",getPlayerName(source), summe ,"Überweisung von "..getPlayerName(source).." in Höhe von "..summe)
				outputChatBox("Überweisung wurde abgesendet!",source,125,0,0)
				outputChatBox("Du hast eine Überweisung erhalten in der Höhe von "..tostring(summe).." von "..getPlayerName(source),target,0,125,0)
				outputChatBox("Mit den Verwendungszweck "..grund,target,0,125,0)
			else
				outputChatBox("Du hast nicht genügend Geld!",source,125,0,0)
			end
		else
			outputChatBox("Ungültige Eingabe!",source,125,0,0)
		end
	else
		outputChatBox("Der Spieler ist nicht Online!",source,125,0,0)
	end
end)