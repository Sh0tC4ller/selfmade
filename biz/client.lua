local bizName = ""
local bizID = 0
local bizDescription = ""
addEvent("showBizBuy", true)
addEventHandler("showBizBuy", getRootElement(), function(id, name, description, pickup)
	bizName = name
	bizID = id
	bizDescription = description
	showCursor(true)
	addEventHandler("onClientRender", getRootElement(), renderBuyBiz)
	addEventHandler("onClientClick", getRootElement(), clickBuyBiz)
end)

local screenW, screenH = guiGetScreenSize()
function renderBuyBiz()
	dxDrawRectangle((screenW - 382) / 2, (screenH - 21) / 2 - 170, 439, 21, tocolor(0, 125, 0, 255), true)
    dxDrawRectangle((screenW - 382) / 2, (screenH - 373) / 2, 439, 373, tocolor(0, 0, 0, 200), false)
    dxDrawRectangle((screenW - 382) / 2, (screenH - 21) / 2 + 170, 439, 21, tocolor(0, 125, 0, 255), true)
    dxDrawImage((screenW + 150) / 2, (screenH - 267) / 2, 157, 123, "images/biz/"..bizID..".png", 0, 0, 0, tocolor(255, 255, 255, 255), false)
    dxDrawText(bizName, (screenW - 307) / 2, (screenH - 198) / 2, ((screenW - 307) / 2) + 150, ((screenH - 198) / 2) + 20, tocolor(255, 255, 255, 255), 0.70, "bankgothic", "center", "center", false, true, false, false, false)
    dxDrawText(bizDescription, (screenW - 301) / 2, (screenH - 258) / 2 + 100, ((screenW - 301) / 2) + 180, ((screenH - 258) / 2) + 300, tocolor(255, 255, 255, 255), 1.00, "default-bold", "left", "top", false, true, false, false, false)
    
    if isCursorOnElement((screenW - 321) / 2, ((screenH - 398) / 2) + 280, 155, 41) then
    	dxDrawRectangle((screenW - 321) / 2, ((screenH - 398) / 2) + 280, 155, 41, tocolor(0, 0, 0, 255), false) 
    else
    	dxDrawRectangle((screenW - 321) / 2, ((screenH - 398) / 2) + 280, 155, 41, tocolor(0, 125, 0, 255), false) 
    end
    dxDrawText("Kaufen", ((screenW - 321) / 2 ) - 165 , ((screenH - 398) / 2) + 210, (screenW - 321) / 2 + 321, ((screenH - 398) / 2) + 386, tocolor(255, 255, 255, 255), 0.50, "bankgothic", "center", "center", false, false, false, false, false)
        
    if isCursorOnElement((screenW - 546) / 2 + 320, (screenH - 398) / 2 + 280, 155, 41) then
    	 dxDrawRectangle((screenW - 546) / 2 + 320, (screenH - 398) / 2 + 280, 155, 41, tocolor(0, 0, 0, 255), false)
    else
    	 dxDrawRectangle((screenW - 546) / 2 + 320, (screenH - 398) / 2 + 280, 155, 41, tocolor(0, 125, 0, 255), false)
    end
    dxDrawText("Abbrechen", (screenW - 546) / 2 + 320 + 150, (screenH - 398) / 2 + 280 + 35, (screenW - 546) / 2 + 320, (screenH - 398) / 2 + 280, tocolor(255, 255, 255, 255), 0.50, "bankgothic", "center", "center", false, false, false, false, false)

end

function clickBuyBiz(btn, state)
	if btn == "left" and state == "down" then
		if isCursorOnElement((screenW - 546) / 2 + 320, (screenH - 398) / 2 + 280, 155, 41) then -- Schließen
			showCursor(false)
			removeEventHandler("onClientRender", getRootElement(), renderBuyBiz)
			removeEventHandler("onClientClick", getRootElement(), clickBuyBiz)
		end
		if isCursorOnElement((screenW - 321) / 2, ((screenH - 398) / 2) + 280, 155, 41) then -- Kaufen
			showCursor(false)
			removeEventHandler("onClientRender", getRootElement(), renderBuyBiz)
			removeEventHandler("onClientClick", getRootElement(), clickBuyBiz)
			triggerServerEvent("onBuyBiz", source, getLocalPlayer(), bizID)

		end
	end
end