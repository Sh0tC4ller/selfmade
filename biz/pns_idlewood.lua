﻿local GARAGE_ID = 8

local pns_pizzahut = Biz:add("Pay N Spray Idlewood", 2072.0224609375, -1823.5478515625, 13.54687)
pns_pizzahut:setDescription("Das Pay N Spray in Idlewood ist eine oft besucht Werkstatt, um sein Auto zu reparieren")

addEventHandler("onResourceStart", getResourceRootElement(getThisResource()),
function (resource)
	local garageCube = createColSphere(2071.5782714844, -1831.3693847656, 15.394200325012, 7)
	addEventHandler("onColShapeHit", garageCube, onGarageCubeHit)
	addEventHandler("onColShapeLeave", garageCube, onGarageCubeLeave)
end)
 
function onGarageCubeHit(hitElement, matchingDimension)
	if (getElementType(hitElement) == "player") then
		if (not isGarageOpen(GARAGE_ID)) then
			setGarageOpen(GARAGE_ID, true)
		end
	end
end
 

function onGarageCubeLeave(leaveElement, matchingDimension)
	if (getElementType(leaveElement) == "player") then
		if (isGarageOpen(GARAGE_ID)) then
			setGarageOpen(GARAGE_ID, false)
			
			local x, y, z = getElementPosition(leaveElement)
			if getDistanceBetweenPoints3D(x, y, z, 2059.400390625, -1831.7001953125, 13.5468) < 8 then
				local veh = getPedOccupiedVehicle( leaveElement )
					if veh then 
					setElementFrozen(veh, true)
					toggleControl(leaveElement, "enter_exit", false) 
					setTimer(repair10sectimer, 1600, 1, leaveElement)
					setTimer(repairVehicle, 11600, 1, leaveElement)
					setTimer(function()
					toggleControl(leaveElement, "enter_exit", true)
					end, 11600, 1)
				end
			end
		end
	end
end


function repairVehicle( thePlayer )
	local veh = getPedOccupiedVehicle( thePlayer )
	if veh then
		local driver = getVehicleOccupant(veh)
		local charge = math.floor(1000-getElementHealth(veh))
		if (getPlayerMoney(driver) >= charge) then
			takePlayerMoney(driver,charge)
			fixVehicle(veh, 1000)
			infoBox(thePlayer, "Dein Fahrzeug wurde für\n"..charge.." $\nrepariert!",255,255,0, 7500)
			setElementFrozen(veh, false)
			pns_pizzahut:setKasse(pns_pizzahut:getKasse() + math.abs(math.floor(charge/2)))
		else
			local extraCost = math.floor(charge-getPlayerMoney(driver))
			infoBox(thePlayer, "Du hast nicht genug Geld für die Reperatur\nSie würde noch\n"..extraCost.." $ mehr kosten",255,0,0, 10000)
		end
	end
end


function repair10sectimer( thePlayer )
	local veh = getPedOccupiedVehicle( thePlayer )
	if veh then
		local driver = getVehicleOccupant(veh)
		local charge = math.floor(1000-getElementHealth(veh))
		if (getPlayerMoney(driver) >= charge) then
			infoBox(thePlayer, "Dein Fahrzeug wird repariert!", 0, 255, 50, 10000)
		else
			local extraCost = math.floor(charge-getPlayerMoney(driver))
			infoBox(thePlayer, "Du hast nicht genug Geld für die Reperatur\nSie würde noch\n"..extraCost.." $ mehr kosten",255,0,0, 10000)
		end
	else
		infoBox(thePlayer,"Du sitzt in keinem Fahrzeug!", 200, 0, 0, 5000)
	end
end		