local pns_temple = Biz:add("Pay N Spray Temple", 1018.94140625, -1029.8896484375, 32.088966369629)
pns_temple:setDescription("Das ist das Pay N Spray Temple Diese Werkstatt wird oft besucht, um sein Auto zu reparieren")
local garage_id = 11

addEventHandler("onResourceStart", getResourceRootElement(getThisResource()),
function (resource)
	local garageCube = createColSphere(1024.89453125, -1029.7568359375, 32.1015625, 5)
	addEventHandler("onColShapeHit", garageCube, onPnsTempleGarageCubeHit)
	addEventHandler("onColShapeLeave", garageCube, onPnsTempleGarageCubeLeave)
end)

function onPnsTempleGarageCubeHit(hitElement, matchingDimension)
	if (getElementType(hitElement) == "player") then
		if (not isGarageOpen(garage_id)) then
			setGarageOpen(garage_id, true)
		end
	end
end

function onPnsTempleGarageCubeLeave(leaveElement)
	if (getElementType(leaveElement) == "player") then
		if (isGarageOpen(garage_id)) then
			setGarageOpen(garage_id, false)
			
			local x, y, z = getElementPosition(leaveElement)
			if getDistanceBetweenPoints3D(x, y, z, 1024.89453125, -1023.7568359375, 32.1015625) < 8 then
				local veh = getPedOccupiedVehicle( leaveElement )
					if veh then 
					setElementFrozen(veh, true)
					toggleControl(leaveElement, "enter_exit", false) 
					setTimer(Templerepair10sectimer, 1600, 1, leaveElement)
					setTimer(TemplerepairVehicle, 11600, 1, leaveElement)
					setTimer(function()
					toggleControl(leaveElement, "enter_exit", true)
					end, 11600, 1)
				end
			end
		end
	end
end

function TemplerepairVehicle( thePlayer )
	local veh = getPedOccupiedVehicle( thePlayer )
	if veh then
		local driver = getVehicleOccupant(veh)
		local charge = math.floor(1000-getElementHealth(veh))
		if (getPlayerMoney(driver) >= charge) then
			takePlayerMoney(driver,charge)
			fixVehicle(veh, 1000)
			infoBox(thePlayer, "Dein Fahrzeug wurde für\n"..charge.." $\nrepariert!",125,125,0, 7500)
			setElementFrozen(veh, false)
			pns_temple:setKasse(pns_temple:getKasse() + math.abs(math.floor(charge/2)))
		else
			local extraCost = math.floor(charge-getPlayerMoney(driver))
			infoBox(thePlayer, "Du hast nicht genug Geld für die Reperatur\nSie würde noch\n"..extraCost.." $ mehr kosten",125,0,0, 10000)
		end
	end
end


function Templerepair10sectimer( thePlayer )
	local veh = getPedOccupiedVehicle( thePlayer )
	if veh then
		local driver = getVehicleOccupant(veh)
		local charge = math.floor(1000-getElementHealth(veh))
		if (getPlayerMoney(driver) >= charge) then
			infoBox(thePlayer, "Dein Fahrzeug wird repariert!", 0, 125, 50, 10000)
		else
			local extraCost = math.floor(charge-getPlayerMoney(driver))
			infoBox(thePlayer, "Du hast nicht genug Geld für die Reperatur\nSie würde noch\n"..extraCost.." $ mehr kosten",125,0,0, 10000)
		end
	else
		infoBox(thePlayer,"Du sitzt in keinem Fahrzeug!", 125, 0, 0, 5000)
	end
end		