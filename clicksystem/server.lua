﻿
addEventHandler("onPlayerClick",getRootElement(),function ( mouseButton,  buttonState, clickedElement,  worldPosX,  worldPosY,  worldPosZ,  screenPosX,  screenPosY)
	local x,y,z = getElementPosition(source)
	if buttonState == "down" then
		if getElementData(source,"isLoggedIn") then
			if clickedElement then					
				if getElementData(source, "inGui") == false then
					if getElementType(clickedElement) == "object" then
						if getElementModel(clickedElement) == 2942 then
							if getDistanceBetweenPoints3D(worldPosX, worldPosY, worldPosZ, x, y, z) <= 5 then
								triggerClientEvent(source,"showBank",source)
							end
						end
					elseif getElementType(clickedElement) == "vehicle" then
						if getElementData(clickedElement,"isCarBuyable") then
							triggerClientEvent(source,"showCarHouseBuy",source,clickedElement)
						else
							if getElementData(clickedElement,"Besitzer") ~= false then
								autobesitzer = getElementData(clickedElement,"Besitzer")
								if getDistanceBetweenPoints3D(worldPosX, worldPosY, worldPosZ, x, y, z) <= 20 then
									infoBox(source,"Dieses Auto gehört:\n"..autobesitzer,81, 211, 144, 5000)
								end
							end
						end
					end
				end
			end
		end
	end
end)

addEventHandler("reallife:onLogin",getRootElement(),function ()
	bindKey(source,"ralt","down",function (player)
		showCursor(player,not isCursorShowing(player))
	end,source)
	bindKey(source,"m","down",function (player)
		showCursor(player,not isCursorShowing(player))
	end,source)
end)