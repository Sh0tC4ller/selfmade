local screenW, screenH = guiGetScreenSize()

function renderHud()
	-- BOX Mit Grünen Rändern
	dxDrawRectangle(screenW - 228, screenH-screenH, 228, 21, tocolor(0, 125, 0, 255), true) -- Oben
	dxDrawRectangle(screenW - 228, screenH-screenH, 228, 171, tocolor(0, 0, 0, 200), false) -- Hintergrund 
	dxDrawRectangle(screenW - 228, screenH-screenH + 150, 228, 21, tocolor(0, 125, 0, 255), true)
	
	-- Uhrzeit und Geld auf Rändern
	local hour, minute = getTime()
	if hour < 10 then
		hour = "0"..hour
	end
	if minute < 10 then
		minute = "0"..minute
	end
	dxDrawImage(screenW-175, screenH-screenH + 2, 16, 16, "images/hud/hud_time.png", 0, 0, 0, tocolor(255, 255, 255, 255), true)
    dxDrawText(hour..":"..minute.." Uhr", screenW-175+43, screenH-screenH+3, screenW-175+43+72, screenH-screenH+3+13, tocolor(255, 255, 255, 255), 0.65, "bankgothic", "center", "center", false, false, true, false, false)
        
	local geld = getElementData(getLocalPlayer(), "Geld")
	if geld < 100000 then
		dxDrawImage(screenW-175, screenH-screenH + 151, 16, 16, "images/hud/hud_money.png", 0, 0, 0, tocolor(255, 255, 255, 255), true)
	elseif geld < 1000000 then
		dxDrawImage(screenW-185, screenH-screenH + 151, 16, 16, "images/hud/hud_money.png", 0, 0, 0, tocolor(255, 255, 255, 255), true)
	else
		dxDrawImage(screenW-205, screenH-screenH + 151, 16, 16, "images/hud/hud_money.png", 0, 0, 0, tocolor(255, 255, 255, 255), true)
	end
	dxDrawText(string.gsub(convertNumber(geld), ",", ".").."$", screenW - 175+33, screenH-screenH+153, screenW-175+43+62, screenH-screenH+153+13, tocolor(255, 255, 255, 255), 0.65, "bankgothic", "center", "center", false, false, true, false, false)
	
	-- Leben --
	local leben = math.abs(math.floor(getElementHealth(getLocalPlayer())))
	dxDrawImage(screenW-142, screenH-screenH+31, 22, 22, "images/hud/hud_leben.png", 0, 0, 0, tocolor(180, 0, 0, 255), true)
	dxDrawRectangle(screenW-120, screenH-screenH+37, 100, 10, tocolor(255, 255, 255, 255), false)
    dxDrawRectangle(screenW-120, screenH-screenH+37, tonumber(leben), 10, tocolor(125, 0, 0, 255), false)
    dxDrawText(tonumber(leben).."%", screenW-90, screenH-screenH+36, screenW-90+43, screenH-screenH+47, tocolor(0, 125, 0, 255), 0.50, "bankgothic", "center", "center", false, false, true, false, false)
    
    -- Hunger --
    local hunger = math.abs(math.floor(getElementData(getLocalPlayer(), "Hunger")))
    dxDrawImage(screenW-142, screenH-screenH+57, 22, 22, "images/hud/hud_hunger.png", 0, 0, 0, tocolor(251, 128, 0, 255), true)
    dxDrawRectangle(screenW-120, screenH-screenH+63, 100, 10, tocolor(255, 255, 255, 255), false)
    dxDrawRectangle(screenW-120, screenH-screenH+63, tonumber(hunger), 10, tocolor(231, 108, 0, 255), false)
    dxDrawText(getHungerName(tonumber(hunger)), screenW-117, screenH-screenH+62, screenW-117+95, screenH-screenH+73, tocolor(0, 125, 0, 255), 0.42, "bankgothic", "center", "center", false, false, true, false, false)
	
	-- Armor --
	local armor = math.abs(math.floor(getPedArmor(getLocalPlayer())))
	if armor > 0 then
		dxDrawImage(screenW-142, screenH-screenH+84, 22, 22, "images/hud/hud_amour.png", 0, 0, 0, tocolor(155, 155, 155, 255), true)
        dxDrawRectangle(screenW-120, screenH-screenH+90, 100, 10, tocolor(255, 255, 255, 255), false)
        dxDrawRectangle(screenW-120, screenH-screenH+90, tonumber(armor), 10, tocolor(155, 155, 155, 255), false)
    	dxDrawText(tonumber(armor).."%", screenW-90, screenH-screenH+90, screenW-90+43, screenH-screenH+90+10, tocolor(0, 125, 0, 255), 0.50, "bankgothic", "center", "center", false, false, true, false, false)
        
    end
	
	if isElementInWater(getLocalPlayer()) then
		-- Oxygen --
		local oxygen = getPedOxygenLevel(getLocalPlayer())
		oxygen = math.abs(math.floor(oxygen/10))
		dxDrawImage(screenW-142, screenH-screenH+112, 22, 22, "images/hud/hud_oxygen.png", 0, 0, 0, tocolor(255, 255, 255, 255), true)
        dxDrawRectangle(screenW-120, screenH-screenH+116, 100, 10, tocolor(255, 255, 255, 255), false)
        dxDrawRectangle(screenW-120, screenH-screenH+116, tonumber(oxygen), 10, tocolor(0, 0, 155, 255), false)
    	dxDrawText(tonumber(oxygen).."%", screenW-90, screenH-screenH+115, screenW-90+40, screenH-screenH+126, tocolor(0, 125, 0, 255), 0.50, "bankgothic", "center", "center", false, false, true, false, false)
    end

    local weapon = getPlayerWeapon(getLocalPlayer())
   	dxDrawImage(screenW - 224, screenH-screenH+31, 68, 79, "images/hud/weapons/"..weapon..".png", 0, 0, 0, tocolor(255, 255, 255, 255), true)
    if weapon > 0 then
  	  local total = getPlayerTotalAmmo(getLocalPlayer())
  	  total = total - getPlayerAmmoInClip(getLocalPlayer())
    	dxDrawText(getPlayerAmmoInClip(getLocalPlayer()).."/"..tonumber(total), screenW - 200, screenH-screenH+116, screenW-200+15, screenH-screenH+140, tocolor(255, 255, 255, 255), 0.65, "bankgothic", "center", "center", false, false, true, false, false)
   	end  

   	-- Wanteds --
   	local wanteds = getElementData(getLocalPlayer(), "Wanteds")
   	if wanteds > 0 then
	   	for i = 1, wanteds, 1 do
		   	dxDrawImage(screenW-253+i*36, screenH-screenH+177, 32, 32, "images/hud/hud_wanted.png", 0, 0, 0, tocolor(125, 125, 0, 255), true)
	   	end
	end
end

addEvent("showHUD", true)
addEventHandler("showHUD", getRootElement(), function()
	addEventHandler("onClientRender", getRootElement(), renderHud)
	setPlayerHudComponentVisible("all", false)
	setPlayerHudComponentVisible("radar", true)
	setPlayerHudComponentVisible("crosshair", true)
end)