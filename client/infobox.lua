
local screenW, screenH = guiGetScreenSize()

local InfoBox = ""
local r,g,b = 255,255,255
local Timer

function renderInfoBox()
	dxDrawRectangle((screenW - 433) / 2, screenH - 132, 433, 132, tocolor(0, 0, 0, 210), true)
	dxDrawRectangle((screenW - 433) / 2, screenH - 132 - 21, 433, 21, tocolor(0, 125, 0, 255), true)
	dxDrawText("Infobox/Information", (screenW - 434) / 2, screenH - 153, ((screenW - 434) / 2) + 434, ( screenH - 153) + 21, tocolor(255, 255, 255, 255), 1.00, "default-bold", "center", "center", false, false, true, false, false)
	dxDrawText(InfoBox, (screenW - 418) / 2, screenH - 105 - 20, ((screenW - 418) / 2) + 418, ( screenH - 105 - 20) + 105, tocolor(r, g, b, 255), 1.50, "default-bold", "center", "top", false, true, true, false, false)
end

function disableInfoBox()
	removeEventHandler("onClientRender",getRootElement(),renderInfoBox)
end

function infoBox(text,r1,g1,b1,zeit)
	r,g,b = r1,g1,b1
	InfoBox = text
	timee = zeit
	if Timer and isTimer(Timer) then
		killTimer(Timer)
		Timer = nil
		Timer = setTimer(disableInfoBox,timee,1)
		playSoundFrontEnd(32)
	else
		addEventHandler("onClientRender",getRootElement(),renderInfoBox)
		Timer = setTimer(disableInfoBox,timee,1)
		playSoundFrontEnd(32)
	end
end
addEvent("infoBox",true)
addEventHandler("infoBox",getRootElement(),infoBox)