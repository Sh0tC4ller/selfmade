nameTagRange = 20
nameSphere = createColSphere ( 0, 0, 0, nameTagRange )

nameTagPlayers = {}
nameTagVisible = {}
nameTagHP = {}
nameTagAimTarget = getLocalPlayer()

local players = getElementsByType ( "player" )
for key, index in pairs ( players ) do
	setPlayerNametagShowing ( index, false )
end
addEventHandler ( "onClientPlayerJoin", getRootElement(),
	function ()
		setPlayerNametagShowing ( source, false )
	end
)

function nameTagSpawn ()

	detachElements ( nameSphere )
	if isElement ( getLocalPlayer() ) then
		attachElements ( nameSphere, getLocalPlayer() )
	end
end
setTimer ( nameTagSpawn, 500, -1 )

function nameTagSphereHit ( element, dim )

	if getElementType ( element ) == "player" and not ( element == getLocalPlayer() ) then
		nameTagPlayers[element] = true
		nameTagCheckPlayerSight ( element )
	end
end
addEventHandler ( "onClientColShapeHit", nameSphere, nameTagSphereHit )

function nameTagCheckPlayerSight ( player )

	if isElement ( player ) then
		local x1, y1, z1 = getPedBonePosition ( player, 8 )
		local x2, y2, z2 = getPedBonePosition ( getLocalPlayer(), 8 )
		local hit = processLineOfSight ( x1, y1, z1, x2, y2, z2, true, false, false, true, false )
		nameTagVisible[player] = not hit
		if nameTagVisible[player] then
			nameTagHP[player] = getElementHealth ( getLocalPlayer() )
		end
	else
		nameTagPlayers[player] = nil
		nameTagVisible[player] = nil
		nameTagHP[player] = nil
	end
end

function nameTagSphereLeave ( element )

	nameTagPlayers[element] = nil
	nameTagVisible[element] = nil
	nameTagHP[element] = nil
end
addEventHandler ( "onClientColShapeLeave", nameSphere, nameTagSphereLeave )

function nameTagRender ()

	local x, y, z, sx, sy
	local name, social
	local r, g, b
	for key, index in pairs ( nameTagVisible ) do
		if isElement ( key ) then
			if nameTagVisible[key] then
				x, y, z = getElementPosition ( key )
				if x and y and z then
					sx, sy = getScreenFromWorldPosition ( x, y, z + 1.1, 1000, true )
					if sx and sy then
						r, g, b = calcRGBByHP ( key )
						name = getPlayerName ( key )
						social = "Spieler"
						if getElementData ( key, "socialState" ) then
							social = getElementData ( key, "socialState" )
						end
						dxDrawText(name, sx, sy, sx, sy, tocolor(0, 0, 0, 255), 2 , "default-bold", "center", "center", false, false, false, false, false)
						dxDrawText(name, sx, sy - 2, sx, sy-2, tocolor(0, 0, 0, 255), 2, "default-bold", "center", "center", false, false, false, false, false)
						dxDrawText(name, sx-2, sy, sx - 2, sy, tocolor(0, 0, 0, 255),2, "default-bold", "center", "center", false, false, false, false, false)
						dxDrawText(name, sx-2, sy - 3, sx - 2, sy-2, tocolor(0, 0, 0, 255), 2, "default-bold", "center", "center", false, false, false, false, false)
						dxDrawText(name, sx-1, sy - 3, sx - 1, sy-1, tocolor(r, g, b, 255), 2, "default-bold", "center", "center", false, false, false, false, false)
					end
				end
			end
		else
			nameTagCheckPlayerSight ( key )
		end
	end
end
addEventHandler ( "onClientRender", getRootElement(), nameTagRender )

function calcRGBByHP ( player )

	local hp = getElementHealth ( player )
	local armor = getPedArmor ( player )
	if armor <= 0 then
		if hp <= 0 then
			return 0, 0, 0
		else
			hp = math.abs ( hp - 0.01 )
			return ( 100 - hp ) * 2.55 / 2, ( hp * 2.55 ), 0
		end
	else 
		armor = math.abs ( armor - 0.01 )
		return (armor * 1.80) ,(armor * 1.80), (armor * 1.80)
	end
end

function reCheckNameTag ()

	if ( isElement ( getCameraTarget () ) ) then
		detachElements ( nameSphere )
		attachElements ( nameSphere, getCameraTarget () )
	end
	setElementInterior ( nameSphere, getElementInterior ( getLocalPlayer() ) )
	setElementDimension ( nameSphere, getElementDimension ( getLocalPlayer() ) )
	nameTagPlayers[nameTagAimTarget] = nil
	nameTagVisible[nameTagAimTarget] = nil
	for key, index in pairs ( nameTagPlayers ) do
		nameTagCheckPlayerSight ( key )
	end
end
setTimer ( reCheckNameTag, 500, -1 )
	--[[for key, index in pairs(players) do
		if isElement(index) then
			x, y, z = getElementPosition(index)
			if x and y and z then
				sx, sy = getScreenFromWorldPosition(x, y ,z + 1.1, 1000, true)
				if sx and sy then
					r, g, b = getRGBbyArmorAndHP( index )
					name = getPlayerName(index)
					dxDrawText(name, sx, sy, sx, sy, tocolor(0, 0, 0, 255), 2.50, "default-bold", "center", "center", false, false, false, false, false)
						dxDrawText(name, sx, sy - 2, sx, sy-2, tocolor(0, 0, 0, 255), 2.50, "default-bold", "center", "center", false, false, false, false, false)
						dxDrawText(name, sx-2, sy, sx - 2, sy, tocolor(0, 0, 0, 255), 2.50, "default-bold", "center", "center", false, false, false, false, false)
						dxDrawText(name, sx-2, sy - 3, sx - 2, sy-2, tocolor(0, 0, 0, 255), 2.50, "default-bold", "center", "center", false, false, false, false, false)
						dxDrawText(name, sx-1, sy - 3, sx - 1, sy-1, tocolor(78, 253, 1, 255), 2.50, "default-bold", "center", "center", false, false, false, false, false)

				end
			end
		else
			nameTagCheckSight(key)
		end
	end
end
addEventHandler("onClientRender", getRootElement(), nameTagRender)

function getRGBbyArmorAndHP(player)
	local hp = getElementHealth(player)
	local armor = getPedArmor(player)
	if armor <= 0 then
		if hp <= 0 then
			return 0, 0, 0
		else
			hp = math.abs( hp - 0.01)
			return ( 100 - hp ) * 2.55 / 2, ( hp * 2.55 ), 0
		end
	else
		armor = math.abs(armor - 0.01)
		return (armor * 1.80) ,(armor * 1.80), (armor * 1.80) 
	end
end

function CheckNameTagAgain()
	for key, index in pairs(players) do
		removeEventHandler("onClientRender", getRootElement(), nameTagRender)
		addEventHandler("onClientRender", getRootElement(), nameTagRender)
	end
end
setTimer(CheckNameTagAgain, 500, -1)]]