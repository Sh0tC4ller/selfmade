local screenW, screenH = guiGetScreenSize()

function renderRadial()
	-- TEST
	
	dxDrawImage((screenW + 65)/2 + 85, screenH - screenH + 100, 80, 125, "images/radial_menu/wrench.png", 0, 0, 0, tocolor(255, 255, 255, 255), true)
	
	-- Handy
	if isCursorOnElement((screenW - 388)/2 + 100, screenH - screenH + 50, 190, 61) then
		dxDrawImage((screenW - 388)/2 + 70, screenH - screenH + 50, 246, 61, "images/radial_menu/radial.png", 180, 0, 0, tocolor(0, 0, 0, 200), false)
   		dxDrawImage((screenW - 255)/2 + 100, screenH - screenH + 50, 50, 61, "images/radial_menu/handy.png", 0, 0, 0, tocolor(255, 255, 255, 200), true)
   	else 
    	dxDrawImage((screenW - 388)/2 + 70, screenH - screenH + 50, 246, 61, "images/radial_menu/radial.png", 180, 0, 0, tocolor(0, 0, 0, 200), false)
    	dxDrawImage((screenW - 255)/2 + 100, screenH - screenH + 50, 50, 61, "images/radial_menu/handy.png", 0, 0, 0, tocolor(255, 255, 255, 255), true)
    end
    dxDrawImage((screenW - 388)/2 + 70, screenH -screenH + 380, 246, 61, "images/radial_menu/radial.png", 0, 0, 0, tocolor(0, 0, 0, 200), false)
    dxDrawImage((screenW - 592)/2 + 370, screenH - screenH + 301, 174, 53, "images/radial_menu/radial.png", 300, 0, 0, tocolor(0, 0, 0, 200), false)
    dxDrawImage((screenW - 592)/2 + 370, screenH - screenH + 136, 174, 53, "images/radial_menu/radial.png", 240, 0, 0, tocolor(0, 0, 0, 200), false)
    dxDrawImage((screenW - 261)/2 - 120, screenH - screenH + 301, 174, 53, "images/radial_menu/radial.png", 60, 0, 0, tocolor(0, 0, 0, 200), false)
    dxDrawImage((screenW - 261)/2 - 120, screenH - screenH + 136, 174, 53, "images/radial_menu/radial.png", 120, 0, 0, tocolor(0, 0, 0, 200), false)
end

addEvent("showRadialMenu", true)
addEventHandler("showRadialMenu", getRootElement(), function()
	if not getElementData(source, "inGui") then
		addEventHandler("onClientRender", getRootElement(), renderRadial)
		showCursor(true)
	end
end)

addEvent("closeRadialMenu", true)
addEventHandler("closeRadialMenu", getRootElement(), function()
	if isCursorOnElement((screenW - 388)/2 + 100, screenH - screenH + 50, 190, 61) then
		outputChatBox("Öffne Handy", 125, 0, 0)
	end
	removeEventHandler("onClientRender", getRootElement(), renderRadial)
	showCursor(false)
end)