CEase = {}
CEase.__index = CEase

function CEase:start(time, typ)	
	local data = {}
	data.start = getTickCount()
	data.ending = data.start+time
	data.typ = typ
	data.value = 0	
	setmetatable(data, self)	
	data.easeFunc = function()
		local tick = getTickCount()
		local elapsed = tick-data.start
		local duration = data.ending - data.start
		local progress = elapsed / duration 
		data.value = getEasingValue(progress, data.typ) 
		if tick > data.ending then			
			removeEventHandler("onClientRender", getRootElement(), data.easeFunc)
		end
	end
	addEventHandler("onClientRender", getRootElement(), data.easeFunc)	
	return data
end

---

local screenW, screenH = guiGetScreenSize()
local scroll = 0
local playerTable = getElementsByType("player")

function getStringWithoutCodes( s )
    return string.gsub( s, "#%x%x%x%x%x%x", "" );
end

function getPingColor ( ping )

	if ping <= 50 then
		return 0, 200, 0
	elseif ping <= 150 then
		return 200, 200, 0
	else
		return 200, 0, 0
	end
end

function renderScoreboard()
	playerTable = getElementsByType("player")
	dxDrawRectangle((screenW - 623) / 2, ((screenH - 24) / 2) - 212, 623, 24, tocolor(0, 125, 0, myEase.value*255), true)
	local backAlpha = myEase.value*255
	
	if (backAlpha > 240) then
		backAlpha = 240
	end
	
	local sind = "sind"

	if (table.getn(playerTable)) == 1 then
		sind = "ist"
	else
		sind = "sind"
	end

	dxDrawRectangle((screenW - 623) / 2, (screenH - 400) / 2, 623, 400, tocolor(0, 0, 0, backAlpha), true)
	dxDrawRectangle((screenW - 623) / 2, ((screenH - 24) / 2) + 212, 623, 24, tocolor(0, 125, 0, myEase.value*255), true)
	dxDrawText(Config.ServerName.." Spielerliste", (screenW - 622) / 2, ((screenH - 24) / 2) - 212, ((screenW - 622) / 2) + 622, ( ((screenH - 24) / 2) - 212) + 24, tocolor(255, 255, 255, myEase.value*255), 1.00, "default", "center", "center", false, false, true, false, false)
	dxDrawLine(((screenW - 0) / 2) - 179,  (screenH - 398) / 2, (((screenW - 0) / 2) - 179) + 0, ( (screenH - 398) / 2) + 398, tocolor(255, 255, 255, myEase.value*255), 1, true)
	dxDrawText("Name", ((screenW - 125) / 2) - 246, ((screenH - 22) / 2) - 188, (((screenW - 125) / 2) - 246) + 125, ( ((screenH - 22) / 2) - 188) + 22, tocolor(255, 255, 255, myEase.value*255), 1.00, "default", "center", "center", false, false, true, false, false)
	dxDrawText("Fraktion", ((screenW - 125) / 2) - 111, ((screenH - 22) / 2) - 188, (((screenW - 125) / 2) - 111) + 83, ( ((screenH - 22) / 2) - 188) + 24, tocolor(255, 255, 255, myEase.value*255), 1.00, "default", "center", "center", false, false, true, false, false)
	dxDrawLine(((screenW - 0) / 2) - 85,  (screenH - 398) / 2, (((screenW - 0) / 2) - 85) + 0, ( (screenH - 398) / 2) + 398, tocolor(255, 255, 255, myEase.value*255), 1, true)
	dxDrawText("Sozialer Status", ((screenW - 125) / 2) - 20, ((screenH - 22) / 2) - 188, (((screenW - 125) / 2) - 20) + 125, ( ((screenH - 22) / 2) - 188) + 22, tocolor(255, 255, 255, myEase.value*255), 1.00, "default", "center", "center", false, false, true, false, false)
	dxDrawLine(((screenW - 0) / 2) + 50,  (screenH - 398) / 2, (((screenW - 0) / 2) + 50) + 0, ( (screenH - 398) / 2) + 398, tocolor(255, 255, 255, myEase.value*255), 1, true)
	dxDrawText("Spielzeit", ((screenW - 125) / 2) + 115, ((screenH - 22) / 2) - 188, (((screenW - 125) / 2) + 115) + 97, ( ((screenH - 22) / 2) - 188) + 24, tocolor(255, 255, 255, myEase.value*255), 1.00, "default", "center", "center", false, false, true, false, false)
	dxDrawLine(((screenW - 0) / 2) + 150,  (screenH - 398) / 2, (((screenW - 0) / 2) + 150) + 0, ( (screenH - 398) / 2) + 398, tocolor(255, 255, 255, myEase.value*255), 1, true)
	dxDrawText("Ping", ((screenW - 94) / 2) + 285, ((screenH - 22) / 2) - 188, (((screenW - 94) / 2) + 285) + 73, ( ((screenH - 22) / 2) - 188) + 24, tocolor(255, 255, 255, myEase.value*255), 1.00, "default", "center", "center", false, false, true, false, false)
	dxDrawLine((screenW - 621) / 2,  ((screenH - 0) / 2) - 175, ((screenW - 621) / 2) + 621, ( ((screenH - 0) / 2) - 175) + 0, tocolor(255, 255, 255, myEase.value*255), 1, true)
	dxDrawText("Forum: "..Config.ForumAdress, ((screenW - 285) / 2) - 162, ((screenH - 25) / 2) + 212, (((screenW - 285) / 2) - 162) + 285, ( ((screenH - 25) / 2) + 212) + 25, tocolor(255, 255, 255, myEase.value*255), 1.00, "default", "left", "center", false, false, true, false, false)
    dxDrawText("Es "..sind.." zurzeit "..(table.getn(playerTable)).." Spieler online!", ((screenW - 285) / 2) + 165, ((screenH - 25) / 2) + 212, (((screenW - 285) / 2) + 165) + 285, ( ((screenH - 25) / 2) + 212) + 25, tocolor(255, 255, 255, myEase.value*255), 1.00, "default", "right", "center", false, false, true, false, false)
	dxDrawText("Telefonnr.", ((screenW - 85) / 2) + 196, ((screenH - 24) / 2) - 188, (((screenW - 85) / 2) + 196) + 85, ( ((screenH - 24) / 2) - 188) + 24, tocolor(255, 255, 255, myEase.value*255), 1.00, "default", "center", "center", false, false, true, false, false)
	dxDrawLine(((screenW - 0) / 2) + 238,  (screenH - 398) / 2, (((screenW - 0) / 2) + 238) + 0, ( (screenH - 398) / 2) + 398, tocolor(255, 255, 255, myEase.value*255), 1, true)
		
		--Player Render
		local count  = 0
		for i = 1 + scroll,23 + scroll do 
			if (playerTable[i]) then
				count = count + 30
				local faction = getElementData ( playerTable[i], "Fraktion" )
				local factionname = "..."
				local socialState = getElementData(playerTable[i],"SozialerStatus") or "..."
				if (string.len(socialState) > 19) then
					socialState = string.sub(socialState,0,17)
				end
				
				r2,g2,b2 = 255,255,255
				
				if (not faction) then
					r,g,b = 255,255,255
				else
					faction = tonumber(faction)
					factionname =  Config.fraktionNamen[faction]
					r, g, b = Config.fraktionColor[faction][1], Config.fraktionColor[faction][2], Config.fraktionColor[faction][3]
				end
				dxDrawText(getStringWithoutCodes(getPlayerName(playerTable[i])), ((screenW - 125) / 2) - 246, ((screenH - 22) / 2) - 170 + count, (((screenW - 125) / 2) - 246) + 125, ( ((screenH - 22) / 2) - 188) + 22, tocolor(r2,g2,b2, myEase.value*255), 1.00, "default", "center", "center", false, false, true, false, false)
				dxDrawText(factionname, ((screenW - 125) / 2) - 111, ((screenH - 22) / 2) - 170 + count, (((screenW - 125) / 2) - 111) + 83, ( ((screenH - 22) / 2) - 188) + 24, tocolor(r,g,b, myEase.value*255), 1.00, "default", "center", "center", false, false, true, false, false)
				dxDrawText(socialState, ((screenW - 125) / 2) - 20, ((screenH - 22) / 2) - 170 + count, (((screenW - 125) / 2) - 20) + 125, ( ((screenH - 22) / 2) - 188) + 22, tocolor(r2,g2,b2, myEase.value*255), 1.00, "default", "center", "center", false, false, true, false, false)
				local playingtime = getElementData ( playerTable[i], "Spielzeit" ) or "..."
				if (tonumber(playingtime)) then
					playingtime = math.floor ( playingtime / 60 )..":"..( playingtime - math.floor ( playingtime / 60 ) * 60 )
				end
				dxDrawText(playingtime, ((screenW - 125) / 2) + 115, ((screenH - 22) / 2) - 170 + count, (((screenW - 125) / 2) + 115) + 97, ( ((screenH - 22) / 2) - 188) + 24, tocolor(r2,g2,b2, myEase.value*255), 1.00, "default", "center", "center", false, false, true, false, false)
				dxDrawText(getElementData ( playerTable[i], "Telefonnr" ) or "...", ((screenW - 85) / 2) + 196, ((screenH - 24) / 2) - 170 + count, (((screenW - 85) / 2) + 196) + 85, ( ((screenH - 24) / 2) - 188) + 24, tocolor(r2, g2, b2, myEase.value*255), 1.00, "default", "center", "center", false, false, true, false, false)
				local ping = getPlayerPing(playerTable[i])
				local Pr,Pg,Pb = getPingColor(ping)
				dxDrawText(tostring(ping), ((screenW - 94) / 2) + 285, ((screenH - 22) / 2) - 170 + count, (((screenW - 94) / 2) + 285) + 73, ( ((screenH - 22) / 2) - 188) + 24, tocolor(Pr, Pg, Pb, myEase.value*255), 1.00, "default", "center", "center", false, false, true, false, false)
			end
		end
end

function ScoreBoardScrollUp()
	if (scroll > 0) then
		scroll = scroll - 1
	end
end

function ScoreBoardScrollDown()
	if (playerTable[23+scroll]) then
		scroll = scroll + 1
	end
end

bindKey("tab","both",function (_,state)
	if state == "down" then
		myEase = CEase:start(1000, "InOutQuad")
		addEventHandler("onClientRender",getRootElement(),renderScoreboard)
		bindKey("mouse_wheel_down","down",ScoreBoardScrollDown)
		bindKey("mouse_wheel_up","down",ScoreBoardScrollUp)
	else
		myEase = CEase:start(1000, "InOutQuad")
		removeEventHandler("onClientRender",getRootElement(),renderScoreboard)
		unbindKey("mouse_wheel_down","down",ScoreBoardScrollDown)
		unbindKey("mouse_wheel_up","down",ScoreBoardScrollUp)
	end
end)