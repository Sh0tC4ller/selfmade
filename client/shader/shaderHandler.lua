local resRoot = getResourceRootElement(getThisResource())
local screenWidth, screenHeight = guiGetScreenSize()

shader = {}

addEventHandler("onClientResourceStart", resRoot, function()
	shader["blur"] = dxCreateShader("images/shader/BlurShader.fx")
	
	if not shader["blur"] then
		outputDebugString("Fehler beim starten des Blur Shaders", 0, 255, 0, 0)
	end
end)

--[[function renderBlur(strength)
	addEventHandler("onClientPreRender", getRootElement(), function()
		if blurShader then
			dxUpdateScreenSource(ScreenSource)

			dxSetShaderValue(blurShader, "ScreenSource", ScreenSource)
			dxSetShaderValue(blurShader, "BlurStrength", strength)
			dxSetShaderValue(blurShader, "UVSize", screenWidth, screenHeight)

			dxDrawImage(0, 0, screenWidth, screenHeight, blurShader)
		end
	end)
end]]

function renderBlur(screen, strength, sw, sh)
	if shader["blur"] then
		dxUpdateScreenSource(screen)

		dxSetShaderValue(shader["blur"], "ScreenSource", screen)
		dxSetShaderValue(shader["blur"], "BlurStrength", strength)
		dxSetShaderValue(shader["blur"], "UVSize", sw, sh)

		dxDrawImage(0, 0, sw, sh, shader["blur"])
	else
		if (fileExists("images/shader/BlurShader.fx")) then
		shader["blur"] = dxCreateShader("images/shader/BlurShader.fx")
		end
	end
end

function destroyBlur()
	if shader["blur"] then
		destroyElement(shader["blur"])
		shader["blur"] = nil
	end
end
addEventHandler("onClientResourceStop", getRootElement(), destroyBlur)