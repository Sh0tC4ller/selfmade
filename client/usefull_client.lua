setAmbientSoundEnabled( "gunfire", false )


local x,y = guiGetScreenSize()

function isCursorOnElement(x,y,w,h)
	local mx,my = getCursorPosition ()
	if not mx then
		return false
	end
	local fullx,fully = guiGetScreenSize()
	cursorx,cursory = mx*fullx,my*fully
	if cursorx > x and cursorx < x + w and cursory > y and cursory < y + h then
		return true
	else
		return false
	end
end


function getLicState(state)
	if (tonumber(state) == 0) then
		return "[_]"
	else
		return "[X]"
	end
end


function convertNumber ( number )  
	local formatted = number  
	while true do      
		formatted, k = string.gsub(formatted, "^(-?%d+)(%d%d%d)", '%1,%2')    
		if ( k==0 ) then      
			break   
		end  
	end  
	return formatted
end

function getRealStap()
	local time = getRealTime()
	local date = time.monthday.."."..(time.month + 1).."."..(time.year + 1900)
	local hours = time.hour..":"..time.minute
	return hours.." "..date
end

function getRightColor ( amount)
	local amount = tostring(amount:gsub(",",""))
	local amount = tonumber(amount)
	if (amount and amount > -1) then
		return "#00FF00"..amount.."$#FFFFFF"
	else
		return "#FF0000"..amount.."$#FFFFFF"
	end
end


addCommandHandler("dev", function (player, _, mode, dx)
	setDevelopmentMode(not getDevelopmentMode())
	outputChatBox("Du bist nun im DEV-MODE", player)
end)