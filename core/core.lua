

_setElementData = setElementData
_getElementData = getElementData
notAllowClientElementData = {}
notAllowClientElementData["Passwort"]=true
ElementData = {}

function setElementData(element,data,value)
	if element then
		if not ElementData[element] then 
			ElementData[element]={}
		end
		ElementData[element][data]=value
		if not notAllowClientElementData[data] then
			_setElementData(element,data,value)
		end
		return true
	end
end

function getElementData(element,data)
	if ElementData[element] then 
		if ElementData[element][data] then 
			return ElementData[element][data]
		else
			return _getElementData(element,data)
		end
	else
		return _getElementData(element,data)
	end
end

takePlayerMoney = nil
givePlayerMoney = nil
getPlayerMoney = nil
_setPlayerMoney = setPlayerMoney

function takePlayerMoney(player,amount)
	if player and amount then
		if (tonumber(amount)) then
			setElementData(player,"Geld",tonumber(getElementData(player,"Geld")) - tonumber(amount))
		else
			outputDebugString("@takePlayerMoney Bad Arguments!")
		end
	else
		outputDebugString("@takePlayerMoney Bad Arguments!")
	end
end

function givePlayerMoney(player,amount)
	if player and amount then
		if (tonumber(amount)) then
			setElementData(player,"Geld",tonumber(getElementData(player,"Geld")) + tonumber(amount))
		else
			outputDebugString("@givePlayerMoney Bad Arguments!")
		end
	else
		outputDebugString("@givePlayerMoney Bad Arguments!")
	end
end

function getPlayerMoney(player)
	return tonumber(getElementData(player,"Geld")) or 0
end

function setPlayerMoney(player,amount)
	if player and amount then
		if (tonumber(amount)) then
			setElementData(player,"Geld",amount)
			_setPlayerMoney(player,amount,true)
		else
			outputDebugString("@setPlayerMoney Bad Arguments!")
		end
	else
		outputDebugString("@setPlayerMoney Bad Arguments!")
	end
end

addEventHandler("onElementDataChange",getRootElement(),function (dataname )
	if getElementType(source) == "player" then
		if dataname == "Geld" then
			setPlayerMoney(source,tonumber(getElementData(source,"Geld")))
		end
	end
end)

function createTeleportMarker( x,y,z,int,dim,tx,ty,tz,tint,tdim)
	local marker = createMarker(x,y,z,"cylinder",2)
	setElementInterior(marker,int)
	setElementDimension(marker,dim)
	addEventHandler("onMarkerHit",marker,function ( hit )
		if getElementType(hit) == "player" then
			if getElementDimension(source) == getElementDimension(hit) and getElementInterior(source) == getElementInterior(hit) then
				setElementPosition(hit,tx,ty,tz)
				setElementDimension(hit,tdim)
				setElementInterior(hit,tint)
			end
		end
	end)
	return marker
end

addCommandHandler("kor",function (player)
	local x,y,z = getElementPosition(player)
	local _,_,rz = getElementRotation(player)
	local dim, int = getElementDimension(player), getElementInterior(player)
	outputChatBox(x..", "..y..", "..z..", RotZ: "..rz.." Dim: "..dim..", Int: "..int, player)
end)