
function fraktion_invite( player, cmd, target)
	if target then
		if getElementData(player, "Fraktion") ~= 0 then
			if getElementData(player, "Fraktionrang") >= 4 then
				if getPlayerFromName(target) then
					--if getElementData(target, "Fraktion") ~= 0 then
						local WTF = getPlayerFromName(target)
						local InviteFraktion = getElementData(player, "Fraktion")
						local ficken = tonumber(getElementData(WTF, "Fraktion"))
						outputChatBox(InviteFraktion)
						outputChatBox(ficken)
						setElementData(WTF, "Fraktion", tonumber(InviteFraktion))
						setElementData(WTF, "Fraktionrang", 1)
					--else
						--infoBox(player, "Der Spieler ist bereits in einer Fraktion.", 125, 0, 0, 5000)
					--end
			else
				infoBox(player, "Der Spieler "..target.." ist nicht online!", 125, 0, 0, 5000)
			end
			else
				infoBox(player, "Nur der Co-Leader oder Leader darf Spieler einladen.", 125, 0, 0, 5000)
			end
		else
			infoBox(player, "Du musst in einer Fraktion sein.", 125, 0, 0, 5000)
		end
	else
		infoBox(player, "Syntax: /invite [Spieler]", 0, 0, 125, 5000)
	end
end
addCommandHandler("invite", fraktion_invite)

function fraktion_giveRank(player, cmd, target, rang)
	local player_fraktion = tonumber(getElementData(player, "Fraktion"))
	local player_rang = tonumber(getElementData(player, "Fraktionrang"))
	if target and rang then
		if target == player then
			infoBox(player, "Du kannst dir nicht selbst einen Rang geben!", 125, 0, 0, 5000)
			return
		end 
		if getPlayerFromName(target) then
			local target_fraktion = tonumber(getElementData(target, "Fraktion"))
			local target_rang = tonumber(getElementData(target, "Fraktionrang"))
			if player_fraktion == target_fraktion then
				if player_rang >= 4 then
					setElementData(target, "Fraktionrang", rang )
					if rang > target_rang then
						outputChatBox("Du hast "..getPlayerName(target).." befördert", player, 0, 125, 0)
						outputChatBox("Du wurdest von "..getPlayerName(player).." befördert!", target, 0, 125, 0)
					else
						outputChatBox("Du hast "..getPlayerName(target).." degradiert", player, 0, 125, 0)
						outputChatBox("Du wurdest von "..getPlayerName(player).." degradiert!", target, 125, 0, 0)
					end
				else
					infoBox(player, "Du bist nicht der Co-Leader oder Leader der Fraktion", 125, 0, 0, 5000)
				end
			else
				infoBox(player, "Der Spieler ist nicht in deiner Fraktion", 125, 0, 0, 5000)
			end
		else
			infoBox(player, "Der Spieler ist nicht online", 125, 0, 0, 5000)
		end
	else
		infoBox(player, "Syntax: /giveRank [Spieler][1-5]", 125, 0, 0, 5000)
	end
end
addCommandHandler("giveRank", fraktion_giveRank)

function isPlayerInFraktion( player, fraktion )
	local PlayerFraktion = tonumber(getElementData(player, "Fraktion"))
	if PlayerFraktion == tonumber(fraktion) then
		return true
	else
		return false
	end
end

function isPlayerInNoFraktion( player )
	local fraktion = tonumber(getElementData(player, "Fraktion"))
	if fraktion == 0 then
		return true
	else
		return false
	end
end

local ValidWepParts = {"Munition", "Lauf", "Visier", "Magazin", "Schaft"}


local dealer = createPed(178, 353.6552734375, -103.7197265625, 1.294090270996, 87)

setElementFrozen(dealer, true)
addEventHandler("onPedWasted", dealer, function()
	destroyElement(dealer)
	dealer = createPed(178, 353.6552734375, -103.7197265625, 1.294090270996, 87)
	setElementFrozen(dealer, true)
end)
addEventHandler("onPlayerClick", getRootElement(), function(mouse, btn, element, wX, wY, wZ, sX, sY)
	if mouse == "left" and btn == "down" and element == dealer then
		outputChatBox("Kaufe Waffen Teile!", source, 0, 125, 0)
	end
end)

function addWeaponsFromTruck(player,_ , muni, lauf, visier, magazin, schaft)
	local fraktion = tonumber(getElementData(player, "Fraktion"))
	if fraktion == 4 then
		outputChatBox("Mafia")
		outputChatBox(muni)
		outputChatBox(lauf)
		outputChatBox(visier)
		outputChatBox(magazin)
		outputChatBox(schaft)
		mafia:addWeaponPart("Munition", muni)
		mafia:addWeaponPart("Lauf", lauf)
		mafia:addWeaponPart("Visier", visier)
		mafia:addWeaponPart("Magazin", magazin)
		mafia:addWeaponPart("Schaft", schaft)
		mafia:saveWeaponData()
		for _, part in pairs(ValidWepParts) do
			outputChatBox(tostring(part)..": "..mafia:getWeaponPart(part))
		end
	else
		outputChatBox("Keine Fraktion!")
	end
end
addCommandHandler("addw",addWeaponsFromTruck)
	


function mafiatest()
mafia:addWeaponPart("Visier", 5)
end

function mafiatest2()
local test = mafia:getWeaponPart("Visier")
outputChatBox(test)
end

function mafiatest3()
mafia:removeWeaponPart("Visier", 1)
end

addCommandHandler("mafia", mafiatest)
addCommandHandler("mafia2", mafiatest2)
addCommandHandler("mafia3", mafiatest3)
