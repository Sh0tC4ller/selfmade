﻿function giveWanteds(player, target, anzahl, reason)
	if getElementData(player, "Fraktion") == 1 and getElementData(player, "onDuty") then
		if target then 
			if anzahl then
				if reason then
					local ziel = getPlayerFromName(target)
					local wanteds = getPlayerWantedLevel(ziel) + anzahl
					if wanteds > 6 then
						wanteds = 6
					end
					setPlayerWantedLevel( ziel, wanteds )
					setElementData(ziel, "Wanteds", wanteds)
					playSoundFrontEnd ( ziel, 42)
					outputChatBox("Du hast "..target.." "..anzahl.." Wanteds gegeben", player, 0, 0, 220)
					outputChatBox(target.." besitzt nun "..wanteds.." Wanteds!", player, 0, 0, 220)
					outputChatBox("Du hast: "..anzahl.." Wanteds für "..reason.." erhalten!", ziel, 0, 0, 220)
					outputChatBox("Du besitzt nun insgesamt: "..wanteds.." Wanteds!", ziel, 0, 0, 220)
				else
					infoBox(player,"Du hast keinen Grund angegeben!", 125, 0, 0, 5000)
				end
			else
				infoBox(player,"Du hast keine Wantedanzahl angegeben!", 125, 0, 0, 5000)
			end
		else
			infoBox(player,"Du hast keinen Spieler angegeben!", 125, 0, 0, 5000)
		end
	else
		infoBox(player,"Du bist kein Polizist im Dienst!", 125, 0, 0, 5000)
	end
end
addEvent("onGiveWanteds", true)
addEventHandler("onGiveWanteds", getRootElement(), giveWanteds)

function giveStVO(player, target, anzahl, reason)
	if getElementData(player, "Fraktion") == 1 and getElementData(player, "onDuty") then
		if target and anzahl and reason then
			local target = getPlayerFromName(target)
			local stvo = tonumber(getElementData(target, "StVOPunkte")) + anzahl
			if stvo > 15 then
				stvo = 15
			end
			setElementData(target, "StVOPunkte", stvo)
			playSoundFrontEnd(target, 42)
			outputChatBox("Du hast "..getPlayerName(target).." "..anzahl.." StVO-Punkte gegeben. ", player, 0, 0, 220)
			outputChatBox(getPlayerName(target).." besitzt nun "..stvo.." StVO-Punkte!", player, 0, 0, 220)
			outputChatBox("Du hast: "..anzahl.." StVO-Punkte für "..reason.." erhalten!", target, 0, 0, 220)
			outputChatBox("Du besitzt nun insgesamt: "..stvo.." StVO-Punkte!", target, 0, 0, 220)
		else
			outputDebugString("@giveStVO: Bad Arguments!")
		end
	else
		infoBox(player,"Du bist kein Polizist im Dienst!", 125, 0, 0, 5000)
	end
end
addEvent("onGiveStvo", true)
addEventHandler("onGiveStvo", getRootElement(), giveStVO)

function wantedsclear(player, cmd, target)
	if getElementData(player, "Fraktion") == 1 and getElementData(player, "onDuty") then
		if target then 
			local ziel = getPlayerFromName(target)
			setPlayerWantedLevel( ziel, 0 )
			setElementData(ziel, "Wanteds", 0)
			outputChatBox("Du hast die Wanteds von "..target.." gelöscht!", player, 0, 0, 220)
			outputChatBox("Deine Wanteds wurden gelöscht!", ziel, 0, 0, 220)
		else
			infoBox(player,"Du hast keinen Spieler angegeben!", 200, 0, 0, 5000)
		end
	else
		infoBox(player,"Du bist kein Polizist im Dienst!", 200, 0, 0, 5000)
	end
end
addCommandHandler("delw", wantedsclear)

function einknasten(player, cmd, target)
	if getElementData(player, "Fraktion") == 1 and getElementData(player, "onDuty") then
		if target then 
			local ziel = getPlayerFromName(target)
			setPlayerWantedLevel( ziel, 0 )
			setElementData(ziel, "Wanteds", 0)
			setElementFrozen(ziel, true)

		else
			infoBox(player,"Du hast keinen Spieler angegeben!", 200, 0, 0, 5000)
		end
	else
		infoBox(player,"Du bist kein Polizist im Dienst!", 200, 0, 0, 5000)
	end
end
addCommandHandler("opfer", einknasten)
