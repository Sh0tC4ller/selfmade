local screenW, screenH = guiGetScreenSize()

local summeGrundWt = ""
local summeWandets = ""
local summeGrundSt = ""
local summeStVO = ""
local GrundWTEdit = guiCreateEdit(screenW-84-150, screenH-30-35, 84, 30, "", false)
local WandetsEdit = guiCreateEdit(screenW-84-50, screenH-30-35, 30, 30, "0", false)
local GrundStVOEdit = guiCreateEdit(screenW-84-150, screenH-55-64, 84, 30, "", false)
local StVOEdit = guiCreateEdit(screenW-84-50, screenH-55-64, 30, 30, "0", false)
guiSetVisible(GrundWTEdit, false)
guiSetVisible(WandetsEdit, false)
guiSetVisible(GrundStVOEdit, false)
guiSetVisible(StVOEdit, false)
guiEditSetMaxLength(WandetsEdit, 1)
guiEditSetMaxLength(StVOEdit, 1)

local currentPlayer = nil
local currentID = 0
local show = false

function renderComputer()
	dxDrawRectangle(screenW-336, screenH-353, 337, 17, tocolor(0, 125, 0, 255), true) -- Grüner Balken Oben
	dxDrawText("Polizeicomputer", (screenW-336)+150, screenH-353, (screenW-336)+50, (screenH-353)+20, tocolor(255, 255, 255, 255), 1.00, "default-bold", "left", "top", false, false, true, false, false)
	dxDrawRectangle(screenW-336, screenH-353, 336, 353, tocolor(0, 0, 0, 230), false)
    dxDrawRectangle(screenW-336, screenH-17, 337, 17, tocolor(0, 125, 0, 255), true) -- Grüner Balken Unten

    if isCursorOnElement(screenW-26, screenH-337, 26, 21) then
    	dxDrawRectangle(screenW-26, screenH-337, 26, 21, tocolor(255, 255, 255, 175), false)
    else
    	dxDrawRectangle(screenW-26, screenH-337, 26, 21, tocolor(255, 255, 255, 50), false)
    end
    dxDrawText("X", screenW-26, screenH-337, screenW, screenH-337+22, tocolor(255, 0, 0, 255), 1.00, "default-bold", "center", "center", false, true, false, false, false)
        
    if isCursorOnElement(screenW-84-10, screenH-30-35, 84, 30) then
    	dxDrawRectangle(screenW-84-10, screenH-30-35, 84, 30, tocolor(180, 180, 0, 255), false)
   	else
    	dxDrawRectangle(screenW-84-10, screenH-30-35, 84, 30, tocolor(0, 125, 0, 255), false)
    end
    dxDrawText("Wanted geben", screenW-84-10+20, screenH-30-35, screenW-84-10+65, screenH-30-35+30, tocolor(255, 255, 255, 255), 1.00, "default-bold", "center", "center", false, true, false, false, false)
    
    guiSetVisible(WandetsEdit, true)
    guiSetVisible(GrundWTEdit, true)

	dxDrawLine(screenW-248, screenH-80, screenW, screenH-80, tocolor(255, 255, 255, 255), 1, true)    

	if isCursorOnElement(screenW-84-10, screenH-55-64, 84, 30) then
		dxDrawRectangle(screenW-84-10, screenH-55-64, 84, 30, tocolor(200, 200, 0, 255), false)
	else
		dxDrawRectangle(screenW-84-10, screenH-55-64, 84, 30, tocolor(0, 125, 0, 255), false)
	end
	dxDrawText("StVO geben", screenW-84-10+20, screenH-55-64, screenW-84-10+65, screenH-55-64+30, tocolor(255, 255, 255, 255), 1.00, "default-bold", "center", "center", false, true, true, false, false)

	guiSetVisible(StVOEdit, true)
    guiSetVisible(GrundStVOEdit, true)

    dxDrawLine(screenW-336+63, screenH-336, screenW-336+63, screenH-336+319, tocolor(255, 255, 255, 255), 1, false)
    dxDrawLine(screenW-336, screenH-336+15, screenW-336+88, screenH-336+15, tocolor(255, 255, 255, 255), 1, false)
    dxDrawLine(screenW-336+88, screenH-336, screenW-336+88, screenH-336+319, tocolor(255, 255, 255, 255), 1, false)
    dxDrawText("Spieler", screenW-336, screenH-336-2, screenW-336+65, screenH-336-2+17, tocolor(255, 255, 255, 255), 1.00, "default", "center", "center", false, false, false, false, false)
    dxDrawText("W", screenW-336+66, screenH-336-2, screenW-336+88, screenH-336-2+17, tocolor(255, 255, 255, 255), 1.00, "default", "center", "center", false, false, true, false, false)

    -- Wantedler Liste --
    local count = 0
    refreshWantedler()

    for _, player in pairs(wantedler) do
    	count = count + 18
    	if isCursorOnElement(screenW-336, screenH-335+count, 88, 10) then
	    	dxDrawText(player, screenW-336, screenH-329+count, screenW-336+65, screenH-329+count, tocolor(255, 255, 255, 255), 1.00, "default-bold", "center", "center", false, false, false, false, false)
	        dxDrawText(getElementData(getPlayerFromName(player), "Wanteds"), screenW-336+66, screenH-329+count, screenW-336+88, screenH-329+count, tocolor(255, 255, 255, 255), 1.00, "default-bold", "center", "center", false, false, true, false, false)
	        dxDrawLine(screenW-336, screenH-336+17+count, screenW-336+88, screenH-336+17+count, tocolor(255, 255, 255, 255), 1, false)
    		currentPlayer = player
    	elseif currentID == count then
    		dxDrawText(player, screenW-336, screenH-329+count, screenW-336+65, screenH-329+count, tocolor(255, 255, 255, 255), 1.00, "default-bold", "center", "center", false, false, false, false, false)
	        dxDrawText(getElementData(getPlayerFromName(player), "Wanteds"), screenW-336+66, screenH-329+count, screenW-336+88, screenH-329+count, tocolor(255, 255, 255, 255), 1.00, "default-bold", "center", "center", false, false, true, false, false)
	        dxDrawLine(screenW-336, screenH-336+17+count, screenW-336+88, screenH-336+17+count, tocolor(255, 255, 255, 255), 1, false)
    	else
    		dxDrawText(player, screenW-336, screenH-329+count, screenW-336+65, screenH-329+count, tocolor(255, 255, 255, 255), 1.00, "default", "center", "center", false, false, false, false, false)
	        dxDrawText(getElementData(getPlayerFromName(player), "Wanteds"), screenW-336+66, screenH-329+count, screenW-336+88, screenH-329+count, tocolor(255, 255, 255, 255), 1.00, "default", "center", "center", false, false, true, false, false)
	        dxDrawLine(screenW-336, screenH-336+17+count, screenW-336+88, screenH-336+17+count, tocolor(255, 255, 255, 255), 1, false)
    	end
    end

    -- Wantedler Infos --
    dxDrawRectangle(screenW-336+88 + 20, screenH-336+50, 201, 120, tocolor(255, 255, 255, 150), true)
      if currentID ~= 0 and currentPlayer ~= nil then
      	if getElementData(getPlayerFromName(currentPlayer), "isLoggedIn") then
	    	local infos = ""
	    	local x, y, z = getElementPosition(getPlayerFromName(currentPlayer))
	    	infos = "Aktueller Standort:\n"
	    	local standort = getZoneName(x, y, z)
	    	local stadt = getZoneName(x, y, z, true)
	    	infos = infos .."    "..standort..", "..stadt.."\n"
	    	local veh = getPedOccupiedVehicle(getPlayerFromName(currentPlayer))
	    	infos = infos .."Unterwegs:\n"
	    	if veh then
	    		infos = infos .."    "..getVehicleNameFromModel(getElementModel(veh)).."\n"
	    	else
	    		infos = infos .."    zu Fuß\n"
	    	end
	    	infos = infos .."Grund für die Wanteds:\n"
	    	local grund = getElementData(getPlayerFromName(currentPlayer), "Wantedgrund")
	    	if grund ~= false then
	    		infos = infos .."    "..tostring(grund)
	    	else
	    		infos = infos .."    /"
	    	end

	    	-- Position mit Blip anzeigen, wenn Handy nicht aus ist --



	    	dxDrawText(infos, screenW-336+110, screenH-336+55, screenW-336+92+201, screenH-336+50+120, tocolor(0, 0, 0, 255), 1.00, "default", "left", "top", false, false, true, false, false)
	    end
	end
end

function refreshWantedler()
	wantedler = nil
	wantedler = {}
	for index, player in ipairs(getElementsByType("player")) do
		if getElementData(player, "isLoggedIn") then
				table.insert(wantedler, getPlayerName(player))
	    end
    end
end

function onComputerClick(btn, state)
	if btn == "left" then
		if state == "down" then
			if isCursorOnElement(screenW-26, screenH-337, 26, 21) then
				showCursor(false)
				setElementData(getLocalPlayer(), "inGui", false)
				guiSetInputMode("allow_binds")
				show = false
				removeEventHandler("onClientRender", getRootElement(), renderComputer)
				removeEventHandler("onClientClick", getRootElement(), onComputerClick)
				removeEventHandler("onClientGUIChanged", getRootElement(), syncEditBoxWithDXComputer)
				guiSetVisible(GrundWTEdit, false)
				guiSetVisible(WandetsEdit, false)
				guiSetVisible(GrundStVOEdit, false)
				guiSetVisible(StVOEdit, false)
			end
			if isCursorOnElement(screenW-84-10, screenH-30-35, 84, 30) then
				if currentID ~= 0 and currentPlayer ~= nil then
					if tonumber(summeWandets) ~= nil then
						if tonumber(summeWandets) < 7 and tonumber(summeWandets) > 0 then
							triggerServerEvent("onGiveWanteds", getLocalPlayer(), getLocalPlayer(), currentPlayer, tonumber(summeWandets), summeGrundWt)
						else
							outputChatBox("Nur Wanteds zwischen 1 - 6 möglich !", 125, 0, 0)
						end
					end
				else
					outputChatBox("Wähle einen Spieler aus!", 125, 0, 0)
				end
				
			end
			if isCursorOnElement(screenW-84-10, screenH-55-64, 84, 30) then
				if currentID ~= 0 and currentPlayer ~= nil then
					if tonumber(summeStVO) ~= nil then
						if tonumber(summeStVO) < 16 and tonumber(summeStVO) > 0 then
							triggerServerEvent("onGiveStvo", getLocalPlayer(), getLocalPlayer(), currentPlayer, tonumber(summeStVO), summeGrundSt)
						else
							outputChatBox("Nur StVO-Punkte zwischen 1 - 15 möglich !", 125, 0, 0)
						end
					end
				else
					outputChatBox("Wähle einen Spieler aus!", 125, 0, 0)
				end
			end
			local count = 0
			for _, player in pairs(wantedler) do
				count = count + 18
				if isCursorOnElement(screenW-336, screenH-335+count, 88, 10) then
					currentID = count
					currentPlayer = player
				end
			end
		end
	else
		currentPlayer = nil
	end
end

function syncEditBoxWithDXComputer()
	if source == GrundWTEdit then
		summeGrundWt = guiGetText(GrundWTEdit)
	elseif source == WandetsEdit then
		summeWandets = guiGetText(WandetsEdit)
	elseif source == GrundStVOEdit then
		summeGrundSt = guiGetText(GrundStVOEdit)
	elseif source == StVOEdit then
		summeStVO = guiGetText(StVOEdit)
	end
end


addEvent("showPolicePC", true)
addEventHandler("showPolicePC", getRootElement(), function()
	if not show then
		show = true
		setElementData(source, "inGui", true)
		addEventHandler("onClientRender", getRootElement(), renderComputer)
		addEventHandler("onClientClick", getRootElement(), onComputerClick)
		addEventHandler("onClientGUIChanged", getRootElement(), syncEditBoxWithDXComputer)
		guiSetInputMode("no_binds")
		showCursor(true)
	end
end)