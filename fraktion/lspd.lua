﻿lspd = Fraktion:New("LSPD")
lspd:addRank("Polizeikommissaranwärter")
lspd:addRank("Polizeimeisteranwärter")
lspd:addRank("Polizeimeister")
lspd:addRank("Polizeirat")
lspd:addRank("Polizeidirektor")

lspd:addCar(411,1586.1999500,-1667.5999800,5.7000000,268.0000000,0,0,0,0,0,0)
lspd:addCar(415,1586.0999800,-1671.3000500,5.7000000,270.0000000,0,0,0,0,0,0)
lspd:addCar(427,1587.3000500,-1679.4000200,6.1000000,272.0000000,0,0,0,0,0,0)
lspd:addCar(427,1587.5000000,-1675.9000200,6.1000000,272.0000000,0,0,0,0,0,0)
lspd:addCar(599,1602,-1684.3000500,6.3000000,90.0000000,0,0,0,0,0,0)
lspd:addCar(599,1602,-1687.5999800,6.3000000,90.0000000,0,0,0,0,0,0)
lspd:addCar(599,1602,-1691.8000500,6.3000000,90.0000000,0,0,0,0,0,0)
lspd:addCar(599,1602,-1696.5999800,6.3000000,90.0000000,0,0,0,0,0,0)
lspd:addCar(599,1602,-1704.0999800,6.3000000,90.0000000,0,0,0,0,0,0)
lspd:addCar(599,1602,-1700.6992200,6.3000000,90.0000000,0,0,0,0,0,0)
lspd:addCar(596,1595.3000500,-1712,5.7000000,0.0000000,0,0,0,0,0,0)
lspd:addCar(596,1591.6999500,-1712,5.7000000,0.0000000,0,0,0,0,0,0)
lspd:addCar(596,1587.5999800,-1712,5.7000000,0.0000000,0,0,0,0,0,0)
lspd:addCar(596,1583.6999500,-1712,5.7000000,0.0000000,0,0,0,0,0,0)
lspd:addCar(596,1579.0000000,-1712,5.7000000,0.0000000,0,0,0,0,0,0)
lspd:addCar(596,1574.5000000,-1712,5.7000000,0.0000000,0,0,0,0,0,0)
lspd:addCar(596,1570.5999800,-1712,5.7000000,0.0000000,0,0,0,0,0,0)
lspd:addCar(596,1566.5000000,-1712,5.7000000,0.0000000,0,0,0,0,0,0)
lspd:addCar(596,1563.1999500,-1712,5.7000000,0.0000000,0,0,0,0,0,0)
lspd:addCar(596,1559.4000200,-1712,5.7000000,0.0000000,0,0,0,0,0,0)
lspd:addCar(433,1577.3000500,-1692.5000000,6.8000000,92.0000000,0,0,0,0,0,0)
lspd:addCar(596,1543.4000200,-1684.1999500,5.7000000,90.0000000,0,0,0,0,0,0)
lspd:addCar(596,1543.5999800,-1680.5000000,5.7000000,90.0000000,0,0,0,0,0,0)
lspd:addCar(596,1543.6999500,-1676.8000500,5.7000000,90.0000000,0,0,0,0,0,0)
lspd:addCar(596,1543.6999500,-1671.8000500,5.7000000,90.0000000,0,0,0,0,0,0)
lspd:addCar(596,1543.5999800,-1667.8000500,5.7000000,90.0000000,0,0,0,0,0,0)
lspd:addCar(596,1543.5999800,-1663.5999800,5.7000000,90.0000000,0,0,0,0,0,0)
lspd:addCar(596,1543.5999800,-1659.0999800,5.7000000,90.0000000,0,0,0,0,0,0)
lspd:addCar(596,1543.5000000,-1655.5000000,5.7000000,90.0000000,0,0,0,0,0,0)
lspd:addCar(596,1538.5999800,-1646.5000000,5.7000000,180.0000000,0,0,0,0,0,0)
lspd:addCar(596,1543.3994100,-1651.3994100,5.7000000,90.0000000,0,0,0,0,0,0)
lspd:addCar(596,1527.1999500,-1646.0000000,5.7000000,179.9950000,0,0,0,0,0,0)
lspd:addCar(596,1534.8994100,-1646.5996100,5.7000000,179.9950000,0,0,0,0,0,0)
lspd:addCar(596,1530.8994100,-1646.5996100,5.7000000,179.9950000,0,0,0,0,0,0)

lspd:addTor(980,1587.5,-1637.90002,15.2,0)

lspd:setDutyPoint(258.015625, 77.3408203125, 1003.640625, 6, 0)

--Waffen--
lspd:addWeapon("all", "armor", 100) --Armor für alle
lspd:addWeapon("all", 29, 200) -- MP5 für alle
lspd:addWeapon("all", 25, 50) -- Shotgun für alle

--Rang 5--
lspd:addWeapon(5, 8, 1) --Messer
lspd:addWeapon(5, 24, 35) --Deagle
lspd:addWeapon(5, 31, 200) --M4

--Rang 4--
lspd:addWeapon(4, 8, 1) --Messer
lspd:addWeapon(4, 24, 35) --Deagle

--Rang 3--
lspd:addWeapon(3, 8, 1) --Messer
lspd:addWeapon(3, 24, 35) --Deagle


--Rang 2--
lspd:addWeapon(2, 3, 1) --Nightstick

--Rang 1--
lspd:addWeapon(1, 3, 1) --Nightstick

--Skin--
--Rang 5--
lspd:addSkin(5, 280)

--Rang 4--
lspd:addSkin(4, 281)

--Rang 3--
lspd:addSkin(3, 288)

--Rang 2--
lspd:addSkin(2, 282)

--Rang 1--
lspd:addSkin(1, 284)


--OOP ENDE--

local pdmarkeraussen = createMarker(1568.59375, -1690.6044921875, 5.890625, "corona", 2, 0, 100, 255, 255, getRootElement())
local pdmarkerinnen = createMarker(246.408203125, 86.9345703125, 1003.640625, "corona", 2, 0, 100, 255, 255, getRootElement())
setElementInterior(pdmarkerinnen, 6)
setElementDimension(pdmarkerinnen, 0)

function PDNachInnen(hitPlayer, matchingDimension)
local car = getPedOccupiedVehicle(hitPlayer)
	if not car then
		setElementPosition( hitPlayer, 246.3232421875, 83.875, 1003.64062 ) 
		setElementInterior( hitPlayer, 6)
	end
end
addEventHandler("onMarkerHit", pdmarkeraussen, PDNachInnen)

function PDNachAussen(hitPlayer, matchingDimension)
local car = getPedOccupiedVehicle(hitPlayer)
	if not car then
		setElementPosition(hitPlayer, 1568.560546875, -1693.154296875, 5.89062)
		setElementInterior(hitPlayer, 0)
	end
end
addEventHandler("onMarkerHit", pdmarkerinnen, PDNachAussen)


