-- Mafia Global Objekt --
mafia = Fraktion:New("Mafia")

-- Mafia Ränge --
mafia:addRank("Familienmitglied")
mafia:addRank("Drogendealer")
mafia:addRank("Waffenspezialist")
mafia:addRank("Familienrat")
mafia:addRank("Don")

-- Mafia Autos --
mafia:addCar(579,2232.3999000,-2243.3000000,13.7000000,44.0000000)
mafia:addCar(579,2225.5000000,-2250.6001000,13.7000000,43.9950000)
mafia:addCar(579,2218.3000000,-2258.3999000,13.7000000,43.9950000)
mafia:addCar(579,2210.5000000,-2264.8999000,13.7000000,43.9950000)
mafia:addCar(541,2188.3999000,-2228.1001000,13.2000000,223.9950000)
mafia:addCar(405,2195.8999000,-2220.5000000,13.6000000,223.9960000)
mafia:addCar(405,2198.8000000,-2217.8000000,13.6000000,223.9950000)
mafia:addCar(413,2192.0000000,-2225.3999000,13.7000000,224.0000000)
mafia:addCar(461,2203.2000000,-2275.7000000,13.2000000,37.9960000)
mafia:addCar(461,2206.3000000,-2272.8999000,13.2000000,37.9960000)
mafia:addCar(461,2203.6001000,-2269.8999000,13.2000000,37.9960000)
mafia:addCar(461,2201.0000000,-2272.8999000,13.2000000,37.9960000)

-- Mafia Tor --
mafia:addTor(980, 2234.3, -2215, 15.3, 316)

-- Mafia Ausrüsten PickUp --
--mafia:setDutyPoint(2139.751953125, -2288.9580078125, 20.66463279724, 0, 0)

-- Mafia Waffen --
--mafia:addWeapon("all", "armor", 100)
--mafia:addWeapon("all", 24, 150)

addCommandHandler("addWaffen", function(typ)
	mafia:addWeaponPart("Lauf", 1)
	outputChatBox("Lauf: "..mafia:getWeaponPart("Lauf"))
	mafia:saveWeaponData()
end)

-- Mafia Skins --
mafia:addSkin(1, 125)
mafia:addSkin(2, 126)
mafia:addSkin(3, 124)
mafia:addSkin(4, 127)
mafia:addSkin(5, 113)