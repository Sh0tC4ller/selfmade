local lp = getLocalPlayer()
local hungerProEinheit = 3
local hungerLossTime = 60000

function updateHungerLvl(hlvl)
	setElementData(lp, "Hunger", hlvl)
end
addEvent("updateHungerLvl", true)
addEventHandler("updateHungerLvl", getRootElement(), updateHungerLvl)

function Hunger()
	setElementData(lp, "Hunger", getElementData(lp, "Hunger") + hungerProEinheit)
	if (getElementData(lp, "Hunger") > 100) then
		setElementData(lp, "Hunger", 100)
	end

	--outputChatBox(tostring(getElementData(lp, "Hunger")), 255, 0, 0)

	if getElementData(lp, "Hunger") >= 100 then
		killTimer(HungerTimer)
	end
	if getElementData(lp, "Hunger") >= 80 then
		triggerServerEvent("warnForHunger", lp, lp, getElementData(lp, "Hunger"), true)
		if getElementData(lp, "Hunger") >= 90 then
			triggerServerEvent("killPlayerSlow", lp, lp, getElementData(lp, "Hunger"))
		end
	else
		triggerServerEvent("warnForHunger", lp, lp, getElementData(lp, "Hunger"), false)
	end
end

function createHungerTimer( lvl )
	if isTimer(HungerTimer) then
		killTimer(HungerTimer)
	end
	if lvl ~= nil then
		setElementData(lp, "Hunger", getElementData(lp, "Hunger"))
	else
		setElementData(lp, "Hunger", 100)
	end
	HungerTimer = setTimer(Hunger, hungerLossTime, 0)
end
addEvent("startHungerTimer", true)
addEventHandler("startHungerTimer", getRootElement(), createHungerTimer)

function getHungerName(lvl) 
	if lvl > 0 and lvl < 29 then
		return "Gesättigt"
	elseif lvl > 30 and lvl < 59 then
		return "Kaum Hungrig"
	elseif lvl > 60 and lvl < 79 then
		return "Hungrig"
	elseif lvl > 80 and lvl <= 100 then
		return "Sehr hungrig"
	else
		return "Hungrig"
	end
end