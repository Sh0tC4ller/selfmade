function warnForHungerLevel(source, hlvl, warning)
	if warning then
		if hlvl == 85 or hlvl == 90 or hlvl == 95 then
			outputChatBox("Du brauchst langsam was zu essen oder du wirst verhungern!", source, 125, 0, 0)
		end
	end

	setElementData(source, "Hunger", tonumber(hlvl))
end
addEvent("warnForHunger", true)
addEventHandler("warnForHunger", getRootElement(), warnForHungerLevel)

function addHungerLevel(source, hlvl)
	if hlvl > 0 and hlvl <= 100 then

		setElementData(source, "Hunger", tonumber(getElementData(source, "Hunger")) + tonumber(hlvl))

		if getElementData(source, "Hunger") > 100 then
			setElementData(source, "Hunger", 100)
		end

		triggerClientEvent(source, "updateHungerLvl", source, getElementData(source, "Hunger"))
	
		return true
	else
		return false
	end
end

function removeHungerLevel(source, hlvl)
	if hlvl > 0 and hlvl <= 100 then
		
		setElementData(source, "Hunger", tonumber(getElementData(source, "Hunger") - hlvl))

		if getElementData(source, "Hunger") < 0 then
			setElementData(source, "Hunger", 0)
		end

		triggerClientEvent(source, "updateHungerLvl", source, getElementData(source, "Hunger"))
		
		return true
	else
		return false
	end
end

function setHungerLevel(source, hlvl)
	if hlvl > 0 and hlvl <= 100 then
		setElementData(source, "Hunger", tonumber(hlvl))

		if (getElementData(source, "Hunger") - hlvl) < 0 then
			setElementData(source, "Hunger", 0)
		elseif (getElementData(source, "Hunger") + hlvl) > 100 then
			setElementData(source, "Hunger", 100)
		end

		triggerClientEvent(source, "updateHungerLvl", source, tonumber(getElementData(source, "Hunger")))
	
		return true
	else
		return false
	end
end

function killPlayerSlow(source, hlvl)
	local loss = (hlvl/50)*2
	local health = getElementHealth(source) - loss

	if health >= 10 then
		setElementHealth(source, health)
	end
end
addEvent("killPlayerSlow", true)
addEventHandler("killPlayerSlow", getRootElement(), killPlayerSlow)