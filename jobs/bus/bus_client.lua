
Bus = {
    radiobutton = {}
}
local screenW, screenH = guiGetScreenSize()
Bus.radiobutton[1] = guiCreateRadioButton((screenW - 419) / 2, (screenH - 23) / 2 - 20, 419, 23, Config.busJobLineNames[1].." ( "..Config.jobPrices[1][1].."$ )", false)
Bus.radiobutton[2] = guiCreateRadioButton((screenW - 419) / 2, (screenH - 23) / 2 + 5, 419, 23, Config.busJobLineNames[2].." ( "..Config.jobPrices[1][2].."$ )", false)
Bus.radiobutton[3] = guiCreateRadioButton((screenW - 419) / 2, (screenH - 23) / 2 + 30, 419, 23, Config.busJobLineNames[3].." ( "..Config.jobPrices[1][3].."$ )", false)
for i = 1,3 do 
	guiSetVisible(Bus.radiobutton[i],false)
end


function renderBusStart()
	dxDrawRectangle((screenW - 440) / 2, ((screenH - 21) / 2) + 125, 440, 21, tocolor(0, 125, 0, 255), true)
	dxDrawRectangle((screenW - 439) / 2, (screenH - 230) / 2, 439, 230, tocolor(0, 0, 0, 200), false)
	dxDrawRectangle((screenW - 440) / 2, ((screenH - 21) / 2) - 125, 440, 21, tocolor(0, 125, 0, 255), true)
	dxDrawText("Busfahrer", (screenW - 440) / 2, (screenH - 17) / 2 - 125, ((screenW - 440) / 2) + 440, ( (screenH - 17) / 2 - 125) + 17, tocolor(255, 255, 255, 255), 1.00, "default-bold", "center", "center", false, false, true, false, false)
	dxDrawText("Hey, Neuling es sieht so aus als haettest du es drauf Busfahrer zu sein. Im naechsten Schritt waehlst du eine Wunschlinie aus und faengst direkt mal \nan!\nDu hast folgende Lienen zur Auswahl:", (screenW - 422) / 2, (screenH - 67) / 2 - 75, ((screenW - 422) / 2) + 422, ( (screenH - 67) / 2 - 75) + 67, tocolor(255, 255, 255, 255), 1.00, "default", "left", "top", false, true, true, false, false)
	if isCursorOnElement((screenW - 207) / 2 - 110, (screenH - 30) / 2 + 85, 207, 30) then
		dxDrawRectangle((screenW - 207) / 2 - 110, (screenH - 30) / 2 + 85, 207, 30, tocolor(0, 0, 0, 255), true)
	else
		dxDrawRectangle((screenW - 207) / 2 - 110, (screenH - 30) / 2 + 85, 207, 30, tocolor(0, 125, 0, 255), true)
	end
	if isCursorOnElement((screenW - 207) / 2 + 110, (screenH - 30) / 2 + 85, 207, 30) then
		dxDrawRectangle((screenW - 207) / 2 + 110, (screenH - 30) / 2 + 85, 207, 30, tocolor(0, 0, 0, 255), true)
	else
		dxDrawRectangle((screenW - 207) / 2 + 110, (screenH - 30) / 2 + 85, 207, 30, tocolor(0, 125, 0, 255), true)
	end
	dxDrawText("Starten", (screenW - 201) / 2 - 110, (screenH - 32) / 2 +85, ((screenW - 201) / 2 - 110) + 201, ( (screenH - 32) / 2 +85) + 32, tocolor(255, 255, 255, 255), 1.00, "default-bold", "center", "center", false, false, true, false, false)
	dxDrawText("Abbrechen", (screenW - 201) / 2 + 110, (screenH - 32) / 2 +85, ((screenW - 201) / 2 + 110) + 201, ( (screenH - 32) / 2 +85) + 32, tocolor(255, 255, 255, 255), 1.00, "default-bold", "center", "center", false, false, true, false, false)
end

function onBusClick(btn,state)
	if (btn == "left" and state == "down" ) then
		if isCursorOnElement((screenW - 207) / 2 - 110, (screenH - 30) / 2 + 85, 207, 30) then
			local currentState = false
			for i = 1,3 do 
				if (guiRadioButtonGetSelected(Bus.radiobutton[i]) ) then
					currentState = i
				end
			end
			if (currentState == false) then
				infoBox("Bitte waehle eine Linie aus!",125,0,0, 5000)
			else
				for i = 1,3 do 
					guiSetVisible(Bus.radiobutton[i],false)
				end
				removeEventHandler("onClientRender",getRootElement(),renderBusStart)
				removeEventHandler("onClientClick",getRootElement(),onBusClick)
				showCursor(false)
				triggerServerEvent("startBusJob",getLocalPlayer(),currentState)
			end
		end
		if isCursorOnElement((screenW - 207) / 2 + 110, (screenH - 30) / 2 + 85, 207, 30) then
			removeEventHandler("onClientRender",getRootElement(),renderBusStart)
			removeEventHandler("onClientClick",getRootElement(),onBusClick)
			showCursor(false)
			for i = 1,3 do 
				guiSetVisible(Bus.radiobutton[i],false)
			end
		end
	end
end

addEvent("openBusJob",true)
addEventHandler("openBusJob",getRootElement(),function ()
	addEventHandler("onClientRender",getRootElement(),renderBusStart)
	addEventHandler("onClientClick",getRootElement(),onBusClick)
	showCursor(true)
	for i = 1,3 do 
		guiSetVisible(Bus.radiobutton[i],true)
	end
end)

addEvent("textToSpeak",true)
addEventHandler("textToSpeak",getRootElement(),function (text)
	playSound("http://translate.google.com/translate_tts?tl=de&q="..text:gsub(" ","+"))
end)