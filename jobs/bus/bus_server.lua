

local pickup = createPickup(1742.8681640625,-1862.14453125,13.57665348053,3,1239,1000,1)
addEventHandler("onPickupHit",pickup,function ( player )
	if getElementType(player ) == "player" then
		if (getElementData(player,"Jobs") == "none") then
			triggerClientEvent(player,"openJobAccept",player,"bus")
		elseif (getElementData(player,"Jobs") == "bus" ) then
			triggerClientEvent(player,"openBusJob",player)
		else
			infoBox(player,"Du hast zurzeit ein anderen Job.\nTippe /quitjob um den Job zubeenden!",125,0,0, 5000)
		end
	end
end)


local BusLines = {
	[1] = {
		["HaltestellePos"]	=	{
			[1]	= 	{1483.7138671875,-1728.5888671875,12.3828125},
			[2]	= 	{1360.765625,-1278.98828125,13.3828125},
			[3]	=	{1200.2099609375,-933.6767578125,42.735652923584},
			[4]	=	{535.013671875,-1241.3974609375,16.538986206055},
			[5]	=	{-1988.7607421875,133.546875,27.5390625},
			[6]	=	{-1999.6904296875,275.12109375,32.845748901367},
			[7]	=	{535.013671875,-1241.3974609375,16.538986206055},
			[8]	=	{1200.2099609375,-933.6767578125,42.735652923584},
			[9]	= 	{1360.765625,-1278.98828125,13.3828125},
			[10]	= 	{1483.7138671875,-1728.5888671875,12.3828125},
			[11]	= {1742.02734375,-1857.791015625,13.4140625},
		},
		["HaltestelleName"]	=	{
			[1]	=	"Rathaus , Los Santos",
			[2]	=	"Ammu Nation , Los Santos",
			[3]	=	"Burger Shot Nord Los Santos",
			[4]	=	"Grotti Los Santos",
			[5]	=	"Bahnhof, San Fierror",
			[6]	=	"Wangcars, San Fierror",
			[7]	=	"Grotti Los Santos",
			[8]	=	"Burger Shot Nord Los Santos",
			[9]	=	"Ammu Nation , Los Santos",
			[10]	=	"Rathaus , Los Santos",
			[11]	=	"Bahnhof , Los Santos",
		}
	}
}

local BusCars = {}
local BusBlips = {}

addEvent("startBusJob",true)
addEventHandler("startBusJob",getRootElement(),function (lines)
	if BusLines[lines] then
		local car = createVehicle(437,1745,-1857,14,5) 
		setElementRotation(car,0,0,270)
		warpPedIntoVehicle(source,car,0)
		BusCars[source]=car
		setElementData(source,"CurrentStation",1)
		setElementData(source,"CurrentLines",lines)
		local x,y,z = BusLines[lines]["HaltestellePos"][1][1],BusLines[lines]["HaltestellePos"][1][2],BusLines[lines]["HaltestellePos"][1][3]
		local marker = createMarker(x,y,z,"cylinder",3,0,0,255,255,source)
		setElementData(marker,"Jobber",source)
		speakHaltestelle(car,"Naechste Haltestelle "..BusLines[lines]["HaltestelleName"][1])
		outputChatBox("Du beginnst nun die Linie "..Config.busJobLineNames[lines],source,0,125,0)
		outputChatBox("Achtung: Du erhaelst nur Geld wenn du alle Stationen abfaehrst!",source,125,0,0)
		addEventHandler("onMarkerHit",marker,function ( player)
			if (getElementType(player) == "player" ) then
				if (getElementData(source,"Jobber") == player) then
					destroyElement(BusBlips[player][1])
					destroyElement(BusBlips[player][2])
					BusBlips[player]=nil
					local car = getPedOccupiedVehicle(player)
					setElementFrozen(car,true)
					setTimer(createNewPoint,2500,1,player)
				end
			end
		end)
		local blip = createBlipAttachedTo(marker,0,2,255,0,0,255,0,99999,source)
		BusBlips[source] = {marker,blip}
	else
		infoBox(source,"Diese Linie konnte nicht gefunden werden!",125,0,0, 5000)
	end
end)

function createNewPoint(player)
	setElementData(player,"CurrentStation",getElementData(player,"CurrentStation") + 1)
	local Station = getElementData(player,"CurrentStation")
	local Linie = getElementData(player,"CurrentLines")
	if BusLines[Linie]["HaltestellePos"][Station]  then
		local x,y,z = BusLines[Linie]["HaltestellePos"][Station][1],BusLines[Linie]["HaltestellePos"][Station][2],BusLines[Linie]["HaltestellePos"][Station][3]
		local marker = createMarker(x,y,z,"cylinder",3,0,0,255,255,player)
		local car = getPedOccupiedVehicle(player)
		setElementFrozen(car,false)
		setElementData(marker,"Jobber",player)
		speakHaltestelle(car,"Naechste Haltestelle "..BusLines[Linie]["HaltestelleName"][Station])
		outputChatBox("Naechste Haltestelle "..BusLines[Linie]["HaltestelleName"][Station],player,0,125,0)
		addEventHandler("onMarkerHit",marker,function ( player)
			if (getElementType(player) == "player" ) then
				if (getElementData(source,"Jobber") == player) then
					destroyElement(BusBlips[player][1])
					destroyElement(BusBlips[player][2])
					BusBlips[player]=nil
					local car = getPedOccupiedVehicle(player)
					setElementFrozen(car,true)
					setTimer(createNewPoint,2500,1,player)
				end
			end
		end)
		local blip = createBlipAttachedTo(marker,0,2,255,0,0,255,0,99999,player)
		BusBlips[player] = {marker,blip}
	else
		local health = getElementHealth(BusCars[player])
		local loss = 0
		if (health < 900) then
			if (health < 750) then
				loss = 350
				infoBox(player,"Du hast die Linie Erfolgreich befahren. Du erhaelst dafuer "..(Config.jobPrices[1][Linie] - loss).."$ am PayDay dazu\nDer Cheff meckert dich an, dass du unvorsichtig bist und zieht dir "..loss.."$ Geld ab fuer die Reparaturen!",0,125,0, 5000)
			elseif (health > 750) then
				loss = 200
				infoBox(player,"Du hast die Linie Erfolgreich befahren. Du erhaelst dafuer "..(Config.jobPrices[1][Linie] - loss).."$ am PayDay dazu\nDer Cheff meckert dich an, dass du unvorsichtig bist und zieht dir "..loss.."$ Geld ab fuer die Reparaturen!",0,125,0, 5000)
			else
				loss = 500
				infoBox(player,"Du hast die Linie Erfolgreich befahren. Du erhaelst dafuer "..(Config.jobPrices[1][Linie] - loss).."$ am PayDay dazu\nDer Cheff meckert dich an, dass du unvorsichtig bist und zieht dir "..loss.."$ Geld ab fuer die Reparaturen!",0,125,0, 5000)
			end
		else
			infoBox(player,"Du hast die Linie Erfolgreich befahren. Du erhaelst dafuer "..(Config.jobPrices[1][Linie] - loss).."$ am PayDay dazu\nDer Cheff ist stolz auf dich. Weiter so!",0,125,0, 5000)
		end
		setElementData(player,"Jobgeld",tonumber(getElementData(player,"Jobgeld")) + (tonumber(Config.jobPrices[1][Linie]) - loss))
		destroyElement(BusCars[player])
		BusCars[player]=nil
	end
end

function speakHaltestelle(vehicle,text)
	local outpants = getVehicleOccupants(vehicle)
	for _,player in pairs(outpants) do 
		triggerClientEvent(player,"textToSpeak",player,text)
	end
end