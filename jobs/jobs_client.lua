
local screenW, screenH = guiGetScreenSize()
local Text = ""
local currentJob = ""

function renderJobClient()
	dxDrawRectangle((screenW - 417) / 2, ((screenH - 21) / 2) + 88, 417, 21, tocolor(0, 125, 0, 255), true)
	dxDrawRectangle((screenW - 417) / 2, (screenH - 158) / 2, 417, 158, tocolor(0, 0, 0, 200), true)
	dxDrawText(Text, (screenW - 398) / 2, ((screenH - 86) / 2) - 25, ((screenW - 398) / 2) + 398, ( ((screenH - 86) / 2) - 25) + 86, tocolor(255, 255, 255, 255), 1.00, "default", "left", "top", false, true, true, false, false)
	dxDrawRectangle((screenW - 417) / 2, ((screenH - 21) / 2) - 88, 417, 21, tocolor(0, 125, 0, 255), true)
	dxDrawText("Job Annehmen", (screenW - 417) / 2, ((screenH - 20) / 2) - 88, ((screenW - 417) / 2) + 417, ( ((screenH - 20) / 2) - 88) + 20, tocolor(255, 255, 255, 255), 1.00, "default-bold", "center", "center", false, false, true, false, false)
	if isCursorOnElement(((screenW - 185) / 2) - 100, ((screenH - 32) / 2) + 50, 185, 32) then
		dxDrawRectangle(((screenW - 185) / 2) - 100, ((screenH - 32) / 2) + 50, 185, 32, tocolor(0, 0, 0, 255), true)
	else
		dxDrawRectangle(((screenW - 185) / 2) - 100, ((screenH - 32) / 2) + 50, 185, 32, tocolor(0, 125, 0, 255), true)
	end
	if isCursorOnElement(((screenW - 185) / 2) + 100, ((screenH - 32) / 2) + 50, 185, 32) then
		dxDrawRectangle(((screenW - 185) / 2) + 100, ((screenH - 32) / 2) + 50, 185, 32, tocolor(0, 0, 0, 255), true)
	else
		dxDrawRectangle(((screenW - 185) / 2) + 100, ((screenH - 32) / 2) + 50, 185, 32, tocolor(0, 125, 0, 255), true)
	end
	dxDrawText("Annehmen", ((screenW - 181) / 2) - 100, ((screenH - 34) / 2) + 50, (((screenW - 181) / 2) - 100) + 181, ( ((screenH - 34) / 2) + 50) + 34, tocolor(255, 255, 255, 255), 1.00, "default-bold", "center", "center", false, false, true, false, false)
	dxDrawText("Ablehnen", ((screenW - 181) / 2) + 100, ((screenH - 34) / 2) + 50, (((screenW - 181) / 2) + 100) + 181, ( ((screenH - 34) / 2) + 50) + 34, tocolor(255, 255, 255, 255), 1.00, "default-bold", "center", "center", false, false, true, false, false)
end

function JobClientMouse(btn,state)
	if btn == "left" and state == "down" then
		if isCursorOnElement(((screenW - 185) / 2) + 100, ((screenH - 32) / 2) + 50, 185, 32) then --Abrechen
			showCursor(false)
			removeEventHandler("onClientRender",getRootElement(),renderJobClient)
			removeEventHandler("onClientClick",getRootElement(),JobClientMouse)
		end
		if (isCursorOnElement(((screenW - 185) / 2) - 100, ((screenH - 32) / 2) + 50, 185, 32) ) then --Annehmen
			showCursor(false)
			removeEventHandler("onClientRender",getRootElement(),renderJobClient)
			removeEventHandler("onClientClick",getRootElement(),JobClientMouse)
			triggerServerEvent("jobAcceptServer",getLocalPlayer(),currentJob)
		end
	end
end

addEvent("openJobAccept",true)
addEventHandler("openJobAccept",getRootElement(),function ( jobs )
	if jobs == "bus" then
		Text = "Du moechtest Busfahrer werden? Du bist hier an der richtigen Stelle. Deine Aufgabe ist es als Busfahrer Personen in der Stadt zu befoerdern.Dazu muss du die einzelnen Bushaltestellen abfahren.\n\nDu kannst dein Job jeweils jede Stunde wechseln!"
	elseif jobs == "truck" then
		Text = "Du moechtest Truckfahrer werden? Deine Aufgabe ist es als Truckfahrer waren an die Geschaefte in ganz San Andereas auszuliefern.Dabei muss du Produkte "
	end
	currentJob = jobs
	showCursor(true)
	addEventHandler("onClientRender",getRootElement(),renderJobClient)
	addEventHandler("onClientClick",getRootElement(),JobClientMouse)
end)



function renderQuitJob()
	dxDrawRectangle((screenW - 427) / 2, (screenH - 21) / 2 -100, 427, 21, tocolor(0, 125, 0, 255), true)
	dxDrawRectangle((screenW - 426) / 2, (screenH - 178) / 2, 426, 178, tocolor(0, 0, 0, 200), true)
	dxDrawRectangle((screenW - 427) / 2, (screenH - 21) / 2 + 98, 427, 21, tocolor(0, 125, 0, 255), true)
	dxDrawText("Job Kuendigen?", (screenW - 425) / 2, (screenH - 21) / 2 -100, ((screenW - 425) / 2) + 425, ( (screenH - 21) / 2 -100) + 21, tocolor(255, 255, 255, 255), 1.00, "default-bold", "center", "center", false, false, true, false, false)
	dxDrawText("Hey,\nDu moechtest uns wirklich verlassen?\nWir wuenschen dir noch viel Spaß bei deinen neuen Job.", (screenW - 405) / 2, (screenH - 52) / 2 - 50, ((screenW - 405) / 2) + 405, ( (screenH - 52) / 2 - 50) + 52, tocolor(255, 255, 255, 255), 1.00, "default", "left", "top", false, false, true, false, false)
	if isCursorOnElement((screenW - 170) / 2 - 100, (screenH - 28) / 2 + 40, 170, 28) then
		dxDrawRectangle((screenW - 170) / 2 - 100, (screenH - 28) / 2 + 40, 170, 28, tocolor(0, 0, 0, 255), true)
	else
		dxDrawRectangle((screenW - 170) / 2 - 100, (screenH - 28) / 2 + 40, 170, 28, tocolor(0, 125, 0, 255), true)
	end
	if isCursorOnElement((screenW - 170) / 2 + 100, (screenH - 28) / 2 + 40, 170, 28) then
		dxDrawRectangle((screenW - 170) / 2 + 100, (screenH - 28) / 2 + 40, 170, 28, tocolor(0, 0, 0, 255), true)
	else
		dxDrawRectangle((screenW - 170) / 2 + 100, (screenH - 28) / 2 + 40, 170, 28, tocolor(0, 125, 0, 255), true)
	end
	dxDrawText("Kuendigen", (screenW - 170) / 2 - 100, (screenH - 25) / 2 + 40, ((screenW - 170) / 2 - 100) + 170, ( (screenH - 25) / 2 + 40) + 25, tocolor(255, 255, 255, 255), 1.00, "default-bold", "center", "center", false, false, true, false, false)
	dxDrawText("Abbrechen", (screenW - 170) / 2 + 100, (screenH - 25) / 2 + 40, ((screenW - 170) / 2 + 100) + 170, ( (screenH - 25) / 2 + 40) + 25, tocolor(255, 255, 255, 255), 1.00, "default-bold", "center", "center", false, false, true, false, false)
end

function quitJobClick()
	if isCursorOnElement((screenW - 170) / 2 - 100, (screenH - 28) / 2 + 40, 170, 28) then--Annehmen
		showCursor(false)
		removeEventHandler("onClientRender",getRootElement(),renderQuitJob)
		removeEventHandler("onClientClick",getRootElement(),quitJobClick)
		triggerServerEvent("quitJob",localPlayer)
	end
	if isCursorOnElement((screenW - 170) / 2 + 100, (screenH - 28) / 2 + 40, 170, 28) then --Abrechen
		showCursor(false)
		removeEventHandler("onClientRender",getRootElement(),renderQuitJob)
		removeEventHandler("onClientClick",getRootElement(),quitJobClick)
	end
end

addCommandHandler("quitjob",function ()
	if getElementData(localPlayer,"Jobs") ~= "none" then
		showCursor(true)
		addEventHandler("onClientRender",getRootElement(),renderQuitJob)
		addEventHandler("onClientClick",getRootElement(),quitJobClick)
	else
		infoBox("Du kannst kein Job kuendigen, da du bereits Arbeitslos bist!",125,0,0, 5000)
	end
end)