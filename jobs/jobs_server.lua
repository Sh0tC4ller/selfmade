﻿
addEvent("jobAcceptServer",true)
addEventHandler("jobAcceptServer",getRootElement(),function ( jobs )
	infoBox(source,"Herzlichen Glueckwunsch, Du bist nun "..Config.jobNamen[jobs].."!",0,125,0, 5000)
	setElementData(source,"Jobs",jobs)
end)

addEvent("quitJob",true)
addEventHandler("quitJob",getRootElement(),function ()
	setElementData(source,"Jobs","none")
	infoBox(source,"Du hast dein Job gekuendigt!",0,125,0, 5000)
end)