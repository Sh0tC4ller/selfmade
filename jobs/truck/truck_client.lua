local screenW, screenH = guiGetScreenSize()

function renderTruckStart()
	dxDrawRectangle((screenW - 440) / 2, ((screenH - 21) / 2) + 125, 440, 21, tocolor(0, 125, 0, 255), true)
	dxDrawRectangle((screenW - 439) / 2, (screenH - 230) / 2, 439, 230, tocolor(0, 0, 0, 200), false)
	dxDrawRectangle((screenW - 440) / 2, ((screenH - 21) / 2) - 125, 440, 21, tocolor(0, 125, 0, 255), true)
	dxDrawText("Truckfahrer", (screenW - 440) / 2, (screenH - 17) / 2 - 125, ((screenW - 440) / 2) + 440, ( (screenH - 17) / 2 - 125) + 17, tocolor(255, 255, 255, 255), 1.00, "default-bold", "center", "center", false, false, true, false, false)
	dxDrawText("Hey, Neuling es sieht so aus als haettest du es drauf Trucker zu sein.\nKlicke nun auf Start und du bekommst eine Lieferung zugewiesen.", (screenW - 422) / 2, (screenH - 67) / 2 - 75, ((screenW - 422) / 2) + 422, ( (screenH - 67) / 2 - 75) + 67, tocolor(255, 255, 255, 255), 1.00, "default", "left", "top", false, true, true, false, false)
	if isCursorOnElement((screenW - 207) / 2 - 110, (screenH - 30) / 2 + 85, 207, 30) then
		dxDrawRectangle((screenW - 207) / 2 - 110, (screenH - 30) / 2 + 85, 207, 30, tocolor(0, 0, 0, 255), true)
	else
		dxDrawRectangle((screenW - 207) / 2 - 110, (screenH - 30) / 2 + 85, 207, 30, tocolor(0, 125, 0, 255), true)
	end
	if isCursorOnElement((screenW - 207) / 2 + 110, (screenH - 30) / 2 + 85, 207, 30) then
		dxDrawRectangle((screenW - 207) / 2 + 110, (screenH - 30) / 2 + 85, 207, 30, tocolor(0, 0, 0, 255), true)
	else
		dxDrawRectangle((screenW - 207) / 2 + 110, (screenH - 30) / 2 + 85, 207, 30, tocolor(0, 125, 0, 255), true)
	end
	dxDrawText("Starten", (screenW - 201) / 2 - 110, (screenH - 32) / 2 +85, ((screenW - 201) / 2 - 110) + 201, ( (screenH - 32) / 2 +85) + 32, tocolor(255, 255, 255, 255), 1.00, "default-bold", "center", "center", false, false, true, false, false)
	dxDrawText("Abbrechen", (screenW - 201) / 2 + 110, (screenH - 32) / 2 +85, ((screenW - 201) / 2 + 110) + 201, ( (screenH - 32) / 2 +85) + 32, tocolor(255, 255, 255, 255), 1.00, "default-bold", "center", "center", false, false, true, false, false)
end

function onTruckClick(btn,state)
	if (btn == "left" and state == "down" ) then
		if isCursorOnElement((screenW - 207) / 2 - 110, (screenH - 30) / 2 + 85, 207, 30) then
				removeEventHandler("onClientRender",getRootElement(),renderTruckStart)
				removeEventHandler("onClientClick",getRootElement(),onTruckClick)
				showCursor(false)
				triggerServerEvent("startTruckJob",getLocalPlayer())
		end
		if isCursorOnElement((screenW - 207) / 2 + 110, (screenH - 30) / 2 + 85, 207, 30) then
			removeEventHandler("onClientRender",getRootElement(),renderTruckStart)
			removeEventHandler("onClientClick",getRootElement(),onTruckClick)
			showCursor(false)
		end
	end
end

addEvent("openTruckJob",true)
addEventHandler("openTruckJob",getRootElement(),function ()
	addEventHandler("onClientRender",getRootElement(),renderTruckStart)
	addEventHandler("onClientClick",getRootElement(),onTruckClick)
	showCursor(true)
end)