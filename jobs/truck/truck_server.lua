﻿local pickup = createPickup(2773.25,-2461.5595703125,13.63721370697,3,1239,1000,1)
local blip = createBlipAttachedTo(pickup, 51, 2, 0, 0, 0, 255, 0, 500)
addEventHandler("onPickupHit",pickup,function ( player )
	if getElementType(player ) == "player" then
		if (getElementData(player,"Jobs") == "none" or getElementData(player, "Jobs") == false) then
			triggerClientEvent(player,"openJobAccept",player,"truck")
		elseif (getElementData(player,"Jobs") == "truck" ) then
			triggerClientEvent(player,"openTruckJob",player)
		else
			infoBox(player,"Du hast zurzeit ein anderen Job.\nTippe /quitjob um den Job zu beenden!",125,0,0, 5000)
		end
	end
end)

local TruckZiele = {
	[1] = {
		["ZielPos"] = { 1923.3193359375,-1792.2529296875,13.3828125 },
		["ZielName"] = "Tankstelle GoIng",
		["ZielTyp"] = "fuel",
	},
	[2] = {
		["ZielPos"] = { 1344.953125,-1753.423828125,13.357688903809 },
		["ZielName"] = "Roboi's Food Markt",
		["ZielTyp"] = "waren",
	}
}

local TruckCars = {}
local TruckTrailer = {}
local TruckBlips = {}

addEvent("startTruckJob", true)
addEventHandler("startTruckJob", getRootElement(), function ()
	if getElementData(source, "isInJob") == false then
		local ziel = math.ceil(math.random(1, 2))
		local startx, starty, startz = 2764.1884765625,-2456.107421875,13.510950088501
		if TruckZiele[ziel] then
			setElementData(source, "isInJob", true)
			-- Fuel
			local car
			local trailer
			if TruckZiele[ziel]["ZielTyp"] == "fuel" then
				car = createVehicle(514, startx, starty, startz)
				trailer = createVehicle(584, startx, starty - 12 ,startz)
				attachTrailerToVehicle(car, trailer)

			-- Waren
			elseif TruckZiele[ziel]["ZielTyp"] == "waren" then
				car = createVehicle(515, startx, starty, startz+1 )
				trailer = createVehicle(435, startx, starty - 12, startz)
				attachTrailerToVehicle(car, trailer)
			end
				warpPedIntoVehicle(source, car, 0)
				TruckCars[source] = car
				TruckTrailer[source] = trailer
				local x, y, z = TruckZiele[ziel]["ZielPos"][1], TruckZiele[ziel]["ZielPos"][2], TruckZiele[ziel]["ZielPos"][3]
				local marker = createMarker(x, y, z, "cylinder", 5, 0, 0, 255, 255, source)
				setElementData(marker, "Jobber", source)
				outputChatBox("Bitte fahre die Lieferung zur "..TruckZiele[ziel]["ZielName"], source, 0, 125, 0)
				outputChatBox("Du erhälst dein Geld nur, wenn du die Fracht heil überbringst und wieder hierher zurück kommst", source, 0, 125, 0)
				addEventHandler("onMarkerHit", marker, function( player )
					if (getElementType(player) == "player") then
						if (getElementData(source, "Jobber") == player) then
							
							detachTrailerFromVehicle(car)
							
							destroyElement(TruckTrailer[player])
							destroyElement(TruckBlips[player][1])
							destroyElement(TruckBlips[player][2])
							
							TruckTrailer[player] = nil
							TruckBlips[player] = nil
							
							outputChatBox("Fahre nun zurück zum Startpunkt, um dein Geld am PayDay zu bekommen", player, 0, 125, 0)
							
							local backMarker = createMarker(2756.9521484375,-2456.9541015625,13.468188285828, "cylinder", 5, 0, 0, 255, 255, player)
							setElementData(backMarker, "JobberBack", player)
							
							addEventHandler("onMarkerHit", backMarker, function( BackPlayer )
								if (getElementData(source, "JobberBack") == BackPlayer) then
						
									destroyElement(TruckCars[BackPlayer])
									destroyElement(TruckBlips[BackPlayer][1])
									destroyElement(TruckBlips[BackPlayer][2])
						
									TruckTrailer[BackPlayer] = nil
									TruckBlips[BackPlayer] = nil
									
									if ( getElementData(BackPlayer, "Jobgeld") == false ) then
										setElementData(BackPlayer, "Jobgeld", Config.jobPrices[2][ziel])
									else
										setElementData(BackPlayer, "Jobgeld", tonumber(getElementData(source, "Jobgeld")) + Config.jobPrices[2][ziel])
									end

									infoBox(BackPlayer,"Du hast die Lieferung erfolgreich zugestellt. Du erhaelst dafuer "..(Config.jobPrices[2][ziel]).."$ am PayDay dazu!",0,125,0, 5000)
									setElementData(BackPlayer, "isInJob", false)
								end
							end)
							local backBlip = createBlipAttachedTo(backMarker, 0, 2, 0, 0, 255, 255, 0, 99999, player)
							TruckBlips[player] = {backMarker, backBlip}
						end
					end
				end)
				local blip = createBlipAttachedTo(marker, 0, 2, 0, 0, 255, 255, 0, 99999, source)
				TruckBlips[source] = {marker, blip}

				addEventHandler("onVehicleExit", car, function (player, seat, jacked)
					local vehTimer = setTimer( function()
						triggerEvent("stopTruckJob", getRootElement(), player)
						setElementData(player, "isInJob", false)
					end, 300000, 1)

					infoBox(player, "In 5 Minuten wird dein Truck abgeholt und du bekommst eine Stafe von deinem Chef", 125, 0, 0, 5000)
					
					addEventHandler("onVehicleEnter", car, function ()
						if ( isTimer(vehTimer) ) then
							killTimer(vehTimer)
						end
					end)
				end)

				--[[addEventHandler("onTrailerDetach", getRootElement(), function (truck)
					local player = getVehicleOccupant(truck, 0)
					triggerEvent("stopTruckJob", getRootElement(), player)
					setElementData(player, "isInJob", false)
				end)]]
				--[[addEventHandler("onPlayerWasted", getRootElement(), function ()
					triggerEvent("stopTruckJob", getRootElement(), source)
					setElementData(source, "isInJob", false)
				end)]]
		else
			infoBox(source, "Die Lieferung konnte nicht gefunden werden.", 125, 0, 0, 5000)
		end
	else
		infoBox(source, "Du hast bereits einen Job zugeteilt bekommen!", 125, 0, 0, 5000)
	end
end)

addEvent("stopTruckJob", true)
addEventHandler("stopTruckJob", getRootElement(), function ( player )
	destroyElement(TruckCars[player])
	destroyElement(TruckTrailer[player])
	TruckCars[player] = nil
	TruckTrailer[player] = nil
	destroyElement(TruckBlips[player][1])
	destroyElement(TruckBlips[player][2])
	TruckBlips[player] = nil
	infoBox(player, "Deine Lieferung wurde abgebrochen. Der Chef ist sauer und zieht dir 250$ für die Sauerrei vom Gehalt ab.", 125, 0, 0, 5000)
	setElementData(player, "Jobgeld", getElementData(player, "Jobgeld") - 250)
end)