
local screenW, screenH = guiGetScreenSize()

guiSetInputMode("no_binds_when_editing")

function getRealTimeStap ()
	local time = getRealTime()
	return "["..(time.monthday).."."..(time.month).."."..(time.year + 1900).." : "..(time.hour).." : "..(time.minute).." ]"
end

function writeClientLog(filename,text)
	local time = getRealTime()
	local fname = "logs/"..(time.year + 1900).."/"..(time.month).."/"..(time.monthday).."/"..filename..".html"
	local time = getRealTimeStap()
	if (fileExists(fname)) then
		local fileL = fileOpen(fname)
		fileSetPos(fileL,fileGetSize(fileL))
		fileWrite(fileL,time.. " : "..text.."<br>")
		fileClose(fileL)
	else
		local fileL = fileCreate(fname)
		fileWrite(fileL,time.. " : "..text.."<br>")
		fileClose(fileL)
	end
	return true
end


function renderInfo()
	dxDrawText("Die erforderlichen Daten werden heruntergeladen...", (screenW - 620) / 2, ((screenH - 39) / 2) + screenH/4, ((screenW - 620) / 2) + 620, ( ((screenH - 39) / 2) + screenH/4) + 39, tocolor(255, 0, 255, 255), 2.00, "default-bold", "center", "center", false, false, true, false, false)
end
addEventHandler("onClientRender",getRootElement(),renderInfo)

triggerServerEvent("onClientStartLoadingFiles",getLocalPlayer())

addEventHandler ("onClientDebugMessage",getRootElement(),
function(message,level,file,line)
	if (level == 1) then
		writeClientLog("debug","<font color=\"#FF0000\">"..message.."</font>")
	elseif (level == 2) then
		writeClientLog("debug","<font color=\"#FFA500\">"..message.."</font>")
	elseif (level == 0) then
		writeClientLog("debug","<font color=\"#0000FF\">"..message.."</font>")
	else
		writeClientLog("debug","<font color=\"#00FF00\">"..message.."</font>")
	end
end)

outputDebugString("Clientscript Loader initialising",3)



addEvent("downloadFiles",true)
addEventHandler("downloadFiles",getRootElement(),function (dltable)
	for index,filetable in ipairs(dltable) do 
		outputDebugString("Loading file : "..filetable["Filename"],3)
		local load,err1 = pcall(loadstring,filetable["Inhalt"])
		if (load) then
			load = loadstring(filetable["Inhalt"])
			local fn,err2 = pcall(assert,load)
			if (fn) then
				fn = assert(load)()
			else
				outputDebugString("Cant load file: "..filetable["Filename"],1)
				outputDebugString(err2,1)
			end
		else
			outputDebugString("Cant load file: "..filetable["Filename"],1)
			outputDebugString(err1,1)
		end
	end
	
	triggerEvent("onClientScriptStart",getRootElement())
	outputDebugString("Clientscripts loaded!")
	removeEventHandler("onClientRender",getRootElement(),renderInfo)
end)

addEvent("onClientScriptStart",true)