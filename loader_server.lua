

local clientfiles = {}
local int = 0

local xml = xmlLoadFile("clientfiles.xml")
local childeren = xmlNodeGetChildren(xml)
for _ , node in pairs(childeren) do 
	int = int + 1
	local filename = xmlNodeGetAttribute(node,"src")
	if (fileExists(filename)) then
		local file = fileOpen(filename)
		local string = fileRead(file,fileGetSize(file))
		fileClose(file)
		clientfiles[int]={}
		clientfiles[int]["Filename"]=filename
		clientfiles[int]["Inhalt"]=string
	else
		error("Clientloader: Konnte die Datei "..filename.." nicht finden!")
		stopResource(getThisResource())
	end
end


addEvent("onClientStartLoadingFiles",true)
addEventHandler("onClientStartLoadingFiles",getRootElement(),function ()
	triggerLatentClientEvent(source,"downloadFiles",9999999,source,clientfiles)
end)