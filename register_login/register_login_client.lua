Registerinformation = {}

Register1 = {
    button = {},
    window = {},
    edit = {},
    label = {}
}
local screenW, screenH = guiGetScreenSize()
Register1.window[1] = guiCreateWindow((screenW - 298) / 2, (screenH - 150) / 2, 298, 150, "Registrieren", false)
guiWindowSetSizable(Register1.window[1], false)
guiWindowSetMovable(Register1.window[1], false)
guiSetVisible(Register1.window[1], false)

Register1.label[1] = guiCreateLabel(64, 25, 184, 49, "Scriptversion : NAME 1.0\nHerzlich Willkommen "..getPlayerName(getLocalPlayer()).."\nWie soll ihr Passwort lauten?", false, Register1.window[1])
guiSetFont(Register1.label[1], "default-bold-small")
Register1.edit[1] = guiCreateEdit(12, 74, 276, 28, "", false, Register1.window[1])
guiEditSetMasked(Register1.edit[1],true)
Register1.button[1] = guiCreateButton(14, 108, 274, 32, "Registrieren", false, Register1.window[1])
addEventHandler("onClientGUIClick",Register1.button[1],function ()
	local pw = guiGetText(Register1.edit[1])
	if pw:len() < 5 then
		outputChatBox("Dein Passwort muss mindestens 6 Zeichen haben!",125,0,0)
		return
	end
	Registerinformation.password = pw
	guiSetVisible(Register1.window[1],false)
	guiSetVisible(Register2.window[1],true)
	outputChatBox("SERVER: Willkommen "..getPlayerName(getLocalPlayer()))
	outputChatBox("Willkommen auf NAME fulle noch ein paar Informationen aus.",204,217,26)
	setCameraInterior(15)
	setCameraMatrix(2219.599609375,-1149.599609375,1025.4000244141,2228.3994140625,-1150.19921875,1025.4000244141)
end,false)
guiSetProperty(Register1.button[1], "NormalTextColour", "FFAAAAAA")


Register2 = {
    gridlist = {},
    window = {},
    button = {}
}
Register2.window[1] = guiCreateWindow((screenW - 309) / 2, (screenH - 171) / 2, 309, 171, "Dein Geschlecht", false)
guiWindowSetSizable(Register2.window[1], false)
guiWindowSetMovable(Register2.window[1], false)
guiSetVisible(Register2.window[1], false)

Register2.gridlist[1] = guiCreateGridList(9, 23, 290, 107, false, Register2.window[1])
guiGridListAddColumn(Register2.gridlist[1], "Geschlecht", 0.9)
for i = 1, 2 do
    guiGridListAddRow(Register2.gridlist[1])
end
guiGridListSetItemText(Register2.gridlist[1], 0, 1, "Männlich", false, false)
guiGridListSetItemText(Register2.gridlist[1], 1, 1, "Weiblich", false, false)
Register2.button[1] = guiCreateButton(10, 133, 289, 28, "OK", false, Register2.window[1])
addEventHandler("onClientGUIClick",Register2.button[1],function ()
	local gui = guiGridListGetSelectedItem(Register2.gridlist[1])
	local text = guiGridListGetItemText(Register2.gridlist[1],gui,1)
	if text == "Männlich" then
		Registerinformation.geschlecht = 0
	else
		Registerinformation.geschlecht = 1
	end
	outputChatBox("Ok, du bist also "..text)
	guiSetVisible(Register2.window[1],false)
	guiSetVisible(Register3.window[1],true)
	
end,false)
guiSetProperty(Register2.button[1], "NormalTextColour", "FFAAAAAA")


Register3 = {
    button = {},
    window = {},
    edit = {},
    label = {}
}
local screenW, screenH = guiGetScreenSize()
Register3.window[1] = guiCreateWindow((screenW - 245) / 2, (screenH - 125) / 2, 245, 125, "Dein Alter", false)
guiWindowSetSizable(Register3.window[1], false)
guiSetAlpha(Register3.window[1], 1.00)
guiWindowSetMovable(Register3.window[1], false)
guiSetVisible(Register3.window[1], false)

Register3.label[1] = guiCreateLabel(10, 24, 232, 19, "Wie alt bist du?", false, Register3.window[1])
Register3.edit[1] = guiCreateEdit(9, 48, 233, 28, "", false, Register3.window[1])
Register3.button[1] = guiCreateButton(12, 80, 224, 32, "Weiter", false, Register3.window[1])
addEventHandler("onClientGUIClick",Register3.button[1],function ()
	local text = guiGetText(Register3.edit[1])
	if text:len() == 2 then
		Registerinformation.alter = text
		outputChatBox("Ok, du bist "..text.. " Jahre alt.")
		guiSetVisible(Register3.window[1], false)
		outputChatBox("Alles klar, nun folgt das Tutorial.",125,0,0)
		outputChatBox("Bitte nehme dir dafür Zeit und lies aufmerksam mit!",125,0,0)
		showCursor(false)
		showChat(true)
		showPlayerHudComponent("all", true)
		triggerServerEvent("onRegister",getLocalPlayer(),Registerinformation)
	else
		outputChatBox("Bitte gebe ein gültiges Alter an!",125,0,0)
	end
end,false)
guiSetProperty(Register3.button[1], "NormalTextColour", "FFAAAAAA")

addEvent("showRegister",true)
addEventHandler("showRegister",getRootElement(),function ()
	guiSetVisible(Register1.window[1],true)
	showCursor(true)
	showChat(false)
	showPlayerHudComponent("all", false)
end)

local screenW, screenH = guiGetScreenSize()
local sound 
LoginEditBox = guiCreateEdit(((screenW - 372) / 2) + 30, ((screenH - 33) / 2) - 20, 372, 33, "", false)
guiSetVisible(LoginEditBox,false)

if (fileExists("settings.xml")) then
	local xml = xmlLoadFile("settings.xml")
	guiSetText(LoginEditBox,xmlNodeGetAttribute(xml,"Passwort"))
	Passwort = xmlNodeGetAttribute(xml,"Passwort")
	xmlUnloadFile(xml)
	if (#Passwort > 0) then
		isCheckBoxActive = true
	else
		isCheckBoxActive = false
	end
else
	local xml = xmlCreateFile("settings.xml","settings")
	xmlNodeSetAttribute(xml,"Passwort","")
	xmlSaveFile(xml)
	xmlUnloadFile(xml)
	isCheckBoxActive = false
	Passwort = ""
end

function getPasswortString (length)
	local string = ""
	if length == 0 then
		return ""
	end
	for i = 1,length do 
		string = string.."*"
	end
	return string
end

function onLoginRender()
	dxDrawRectangle((screenW - 456) / 2, ((screenH - 21) / 2) + 93, 456, 21, tocolor(0, 125, 0, 255), true)
	dxDrawRectangle((screenW - 456) / 2, (screenH - 167) / 2, 456, 167, tocolor(0, 0, 0, 170), false)
	dxDrawRectangle((screenW - 456) / 2, ((screenH - 21) / 2) - 93, 456, 21, tocolor(0, 125, 0, 255), true)
	dxDrawText("Willkommen, "..getPlayerName(getLocalPlayer()).."\nfalls du neu bist waehle einen anderen Namen\n\nPasswort:\n\n\nSpeichern?",(screenW - 442) / 2, ((screenH - 49) / 2) - 55, ((screenW - 442) / 2) + 442, ( ((screenH - 49) / 2) - 60) + 49, tocolor(255, 255, 255, 255), 1.00, "default", "left", "top", false, false, true, false, false)
	dxDrawRectangle(((screenW - 371) / 2) + 30, ((screenH - 32) / 2) - 20, 371, 32, tocolor(255, 255, 255, 255), true)
	dxDrawText(getPasswortString(#Passwort), ((screenW - 371) / 2) + 30, ((screenH - 28) / 2) - 20, (((screenW - 371) / 2) + 30) + 371, ( ((screenH - 28) / 2) - 20) + 28, tocolor(0, 0, 0, 255), 1.00, "default-bold", "left", "center", false, false, true, false, false)
	if isCursorOnElement((screenW - 436) / 2, ((screenH - 32) / 2) + 60, 436, 32) then
		dxDrawRectangle((screenW - 436) / 2, ((screenH - 32) / 2) + 60, 436, 32, tocolor(0, 0, 0, 200), true)
	else
		dxDrawRectangle((screenW - 436) / 2, ((screenH - 32) / 2) + 60, 436, 32, tocolor(0, 125, 0, 200), true)
	end
	dxDrawText("Einloggen", (screenW - 436) / 2, ((screenH - 32) / 2) + 60, ((screenW - 436) / 2) + 436, ( ((screenH - 32) / 2) + 60) + 32, tocolor(255, 255, 255, 255), 1.00, "pricedown", "center", "center", false, false, true, false, false)
	dxDrawText("Login", (screenW - 457) / 2, ((screenH - 22) / 2) - 93, ((screenW - 457) / 2) + 457, ( ((screenH - 22) / 2) - 93) + 22, tocolor(255, 255, 255, 255), 1.00, "default-bold", "center", "center", false, false, true, false, false)
	dxDrawRectangle(((screenW - 32) / 2) - 140, ((screenH - 32) / 2) + 16, 32, 32, tocolor(0, 0, 0, 200), true)
	if isCheckBoxActive then
		dxDrawRectangle(((screenW - 20) / 2) - 140, ((screenH - 20) / 2) + 16, 20, 20, tocolor(255, 255, 255, 255), true)
	else
		dxDrawRectangle(((screenW - 20) / 2) - 140, ((screenH - 20) / 2) + 16, 20, 20, tocolor(0, 0, 0, 255), true)
	end

	dxDrawText("Musik: Marleymusik - Cengo", 4, screenH-22, 150, screenH, tocolor(255, 255, 255, 255), 1.00, "arial", "center", "center", false, false, true, false, false)
end

addEvent("showLogin",true)
addEventHandler("showLogin",getRootElement(),function ()
	showCursor(true)
	showChat(false)
	showPlayerHudComponent("all", false)
	addEventHandler("onClientRender",getRootElement(),onLoginRender)
	addEventHandler("onClientGUIChanged",LoginEditBox,onEditChange)
	addEventHandler("onClientClick",getRootElement(),onClick)
	addEventHandler("onClientMinimize", getRootElement(), destroyBlur)
	guiSetText(LoginEditBox,Passwort)
	guiSetVisible(LoginEditBox,true)
	sound = playSound("register_login/Cengo.mp3", true)
	setSoundVolume(sound, 0.3)
end)

addEvent("closeLogin",true)
addEventHandler("closeLogin",getRootElement(),function ()
	guiSetVisible(LoginEditBox,false)
	removeEventHandler("onClientRender",getRootElement(),onLoginRender)
	removeEventHandler("onClientGUIChanged",LoginEditBox,onEditChange)
	removeEventHandler("onClientClick",getRootElement(),onClick)
	removeEventHandler("onClientMinimize", getRootElement(), destroyBlur)
	showCursor(false)
	showChat(true)
	showPlayerHudComponent("all", true)
	destroyElement(sound)
	destroyBlur()
end)

function onEditChange()
	Passwort = guiGetText(LoginEditBox)
end

function onClick(_,state)
	if state == "down" then
		if isCursorOnElement((screenW - 436) / 2, ((screenH - 32) / 2) + 60, 436, 32) then -- Loginbutton
			local xml = xmlLoadFile("settings.xml")
			if isCheckBoxActive then
				xmlNodeSetAttribute(xml,"Passwort",Passwort)
			else
				xmlNodeSetAttribute(xml,"Passwort","")
			end
			xmlSaveFile(xml)
			xmlUnloadFile(xml)
			triggerServerEvent("onLogin",getLocalPlayer(),Passwort)
		end
		if isCursorOnElement(((screenW - 20) / 2) - 140, ((screenH - 20) / 2) + 16, 20, 20) then --CheckBox
			isCheckBoxActive = not isCheckBoxActive
		end
	end
end

triggerServerEvent("checkLogin",getLocalPlayer())