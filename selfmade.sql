-- phpMyAdmin SQL Dump
-- version 3.4.11.1deb2+deb7u1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Mar 04, 2015 at 10:43 PM
-- Server version: 5.5.41
-- PHP Version: 5.4.36-0+deb7u3

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `selfmade`
--

-- --------------------------------------------------------

--
-- Table structure for table `bans`
--

CREATE TABLE IF NOT EXISTS `bans` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL,
  `serial` varchar(255) NOT NULL,
  `admin` varchar(255) NOT NULL,
  `reason` varchar(255) NOT NULL,
  `type` varchar(255) NOT NULL,
  `bantime` varchar(255) NOT NULL,
  `bantime_expire` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `biz`
--

CREATE TABLE IF NOT EXISTS `biz` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(255) NOT NULL,
  `Besitzer` varchar(255) NOT NULL,
  `Kasse` int(255) NOT NULL,
  `X` bigint(11) NOT NULL,
  `Y` bigint(11) NOT NULL,
  `Z` bigint(11) NOT NULL,
  `Description` longtext NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `biz`
--

INSERT INTO `biz` (`ID`, `Name`, `Besitzer`, `Kasse`, `X`, `Y`, `Z`, `Description`) VALUES
(1, 'Pay N Spray Idlewood', 'Der_Dan', 2578, 2072, -1835, 14, 'Das Pay N Spray in Idlewood ist eine oft besucht Werkstatt, um sein Auto zu reparieren'),
(2, 'Pay N Spray Temple', 'jalako', 25, 1018, -1029, 32, 'Das ist das Pay N Spray Temple Diese Werkstatt wird oft besucht, um sein Auto zu reparieren');

-- --------------------------------------------------------

--
-- Table structure for table `cars`
--

CREATE TABLE IF NOT EXISTS `cars` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Besitzer` varchar(255) NOT NULL,
  `VehID` int(11) NOT NULL,
  `X` int(11) NOT NULL,
  `Y` int(11) NOT NULL,
  `Z` int(11) NOT NULL,
  `RZ` int(11) NOT NULL,
  `Slot` int(11) NOT NULL,
  `Color` varchar(255) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `cars`
--

INSERT INTO `cars` (`ID`, `Besitzer`, `VehID`, `X`, `Y`, `Z`, `RZ`, `Slot`, `Color`) VALUES
(1, 'Sh0tC4ller', 540, 1543, -1607, 13, 185, 1, '[ { "B2": 102, "R": 166, "G2": 170, "R2": 92, "G": 243, "B": 7 } ]'),
(2, 'jalako', 534, 2148, -1134, 26, 90, 1, '[ { "B2": 43, "R": 80, "G2": 225, "R2": 38, "G": 70, "B": 83 } ]'),
(3, 'Hi998', 540, 2148, -1134, 26, 90, 1, '[ { "B2": 198, "R": 54, "G2": 93, "R2": 132, "G": 94, "B": 71 } ]');

-- --------------------------------------------------------

--
-- Table structure for table `fraktionsWaffen`
--

CREATE TABLE IF NOT EXISTS `fraktionsWaffen` (
  `FraktionsID` int(11) NOT NULL,
  `Waffen` text NOT NULL,
  PRIMARY KEY (`FraktionsID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `fraktionsWaffen`
--

INSERT INTO `fraktionsWaffen` (`FraktionsID`, `Waffen`) VALUES
(4, '[ { "Munition": 1903053008, "Lauf": 421, "Visier": 2530, "Magazin": 470, "Schaft": 2600 } ]');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(50) NOT NULL,
  `Passwort` varchar(512) NOT NULL,
  `Serial` varchar(100) NOT NULL,
  `Geschlecht` int(11) NOT NULL DEFAULT '0',
  `Skin` int(11) NOT NULL DEFAULT '0',
  `Adminlevel` int(11) NOT NULL,
  `Fraktion` int(2) NOT NULL DEFAULT '0',
  `Fraktionrang` int(2) NOT NULL DEFAULT '0',
  `Wanteds` int(11) NOT NULL DEFAULT '0',
  `StVOPunkte` int(11) DEFAULT '0',
  `Hunger` int(11) NOT NULL DEFAULT '0',
  `LastX` varchar(50) NOT NULL DEFAULT '0',
  `LastY` varchar(50) NOT NULL DEFAULT '0',
  `LastZ` varchar(50) NOT NULL DEFAULT '0',
  `LastRZ` varchar(20) NOT NULL DEFAULT '0',
  `LastInt` int(1) NOT NULL DEFAULT '0',
  `LastDim` int(1) NOT NULL DEFAULT '0',
  `Geld` int(8) NOT NULL DEFAULT '10000',
  `Bankgeld` int(30) NOT NULL DEFAULT '10000',
  `Jobgeld` int(11) NOT NULL,
  `Spielzeit` int(11) DEFAULT '0',
  `Telefonnr` int(11) DEFAULT '0',
  `SozialerStatus` varchar(255) DEFAULT 'Anfaenger',
  `TimeStamp` varchar(50) NOT NULL DEFAULT '0',
  `Autofuehrerschein` int(11) DEFAULT '0',
  `Motorradschein` int(11) DEFAULT '0',
  `LKWfuehrerschein` int(11) DEFAULT '0',
  `Bootfuehrerschein` int(11) DEFAULT '0',
  `Helicopterschein` int(11) DEFAULT '0',
  `Leichtfluegschein` int(11) DEFAULT '0',
  `Schwerfluegschein` int(11) DEFAULT '0',
  `Traktorschein` int(11) DEFAULT '0',
  `Personalausweiss` int(11) DEFAULT '0',
  `SFPass` int(11) DEFAULT '0',
  `LVPass` int(11) DEFAULT '0',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=22 ;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`ID`, `Name`, `Passwort`, `Serial`, `Geschlecht`, `Skin`, `Adminlevel`, `Fraktion`, `Fraktionrang`, `Wanteds`, `StVOPunkte`, `Hunger`, `LastX`, `LastY`, `LastZ`, `LastRZ`, `LastInt`, `LastDim`, `Geld`, `Bankgeld`, `Jobgeld`, `Spielzeit`, `Telefonnr`, `SozialerStatus`, `TimeStamp`, `Autofuehrerschein`, `Motorradschein`, `LKWfuehrerschein`, `Bootfuehrerschein`, `Helicopterschein`, `Leichtfluegschein`, `Schwerfluegschein`, `Traktorschein`, `Personalausweiss`, `SFPass`, `LVPass`) VALUES
(14, 'Sh0tC4ller', '92AB9A18F29A1FADA6A0B18CC196685268A8EAAE55CC972B6EBC4F6657F8525E', '2DEC32B465A1F20DF8FA1EC4B5408142', 0, 260, 4, 4, 5, 6, 0, 100, '1505.012695', '-1713.659180', '15.046875', '267.672241', 0, 0, 18, 340120274, 0, 2250, 0, 'Schwuchtel', '5.9.2014, 15:27', 1, 1, 1, 1, 0, 0, 0, 1, 1, 1, 1),
(15, 'jalako', '5D8E80BEE91ECD0E2AD8F9C30F7387BAFFEF1DF172586DFF3B68F96ADE501E5A', ' C6386DDB866365D057EB5AF304CCBFB4', 0, 10, 4, 4, 1, 1, 5000, 100, '2127.862305', '-1112.246094', '25.905499', '58.505493', 0, 0, 2304725, 51957, 0, 2172, 3, 'Anfaenger', '27.9.2014, 20:6', 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0),
(16, 'ReXx', '5FCF82BC15AEF42CD3EC93E6D4B51C04DF110CF77EE715F62F3F172FF8ED9DE9', '177ED86577A8931633EBF64D1BA6B7F2', 0, 0, 0, 1, 1, 0, 0, 41, '1346.363281', '-1027.942383', '27.677593', '179.906616', 0, 0, 3894, 13484, 0, 234, 0, 'Anfaenger', '0', 1, 1, 1, 1, 0, 0, 0, 1, 1, 1, 1),
(17, 'SuperZucker', '019E596FB8E9166D3EB7A53CDA60BB4BE16EC78191837B9FA1AFBA3368D890AA', '2FC0ED240BC56EFF3C02B01001735C43', 0, 0, 0, 1, 1, 2, 0, 29, '-793.266602', '485.219727', '1402.469238', '60.972290', 1, 1, 999937470, 11407, 0, 35000060, 0, 'Anfaenger', '0', 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1),
(18, 'PantherUlme', 'B2105F7C8CB993F55435B0693FAAA7B40714D3F0F8EA28CF350BFB5259507696', '8575ABFF20CBF22389F897D654399743', 0, 0, 0, 0, 0, 0, 0, 0, '1021.306641', '-1893.818359', '16.254035', '359.310608', 0, 0, 9990, 10000, 0, 24, 0, 'Anfaenger', '0', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(19, 'Thehoett123', '4E4576B004128BDBCB51F6256589B40E126A2195FA7FEF7CE4C45CB4D1FD4AED', '4774EBBB079BC4372C66E59F9BF0C971', 0, 0, 0, 0, 0, 0, 0, 0, '1757.526367', '-1605.399414', '14.377633', '228.038452', 0, 0, 10000, 10000, 0, 8, 0, 'Anfaenger', '0', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(20, 'Der_Dan', '9DCA528F10FB8BDAA4CB641E7A2B5149379D4319F9491E99ABE1AE0DEF3AA02C', '9810F18BD31896045D0CDCA4A1246384', 0, 0, 0, 0, 0, 0, 0, 30, '1235.302734', '-1323.120117', '14.930781', '345.050110', 0, 0, 10000, 10000, 0, 43, 0, 'Anfaenger', '0', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(21, 'Hi998', '5D8E80BEE91ECD0E2AD8F9C30F7387BAFFEF1DF172586DFF3B68F96ADE501E5A', 'A8C4F17B0D8863DB5DD90DE889DF0544', 0, 0, 0, 0, 0, 0, 0, 48, '1472.965820', '-1591.298828', '14.062558', '87.868134', 0, 0, 557, 10810, 1650, 238, 0, 'Anfaenger', '0', 1, 1, 1, 1, 0, 0, 0, 0, 1, 1, 1);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
