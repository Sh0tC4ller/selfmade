function consoleCreateObject ( thePlayer, command, objectid )
   if ( thePlayer ) then
      local x, y, z = getElementPosition ( thePlayer ) -- get the player's position
      local theObject = createObject ( objectid, x + 2, y + 2, z, 0, 0, 0 )
   end
end
addCommandHandler ( "co", consoleCreateObject )

function handleOnPlayerModInfo ( filename, modList )
    local line = ""
    for idx,mod in ipairs(modList) do
        for k,v in pairs(mod) do
			if tonumber(v) then 
			else
				line = line ..tostring(v)
			end
        end
    end
	if string.find(line,".ipl") then
		kickPlayer(source,"Dein GTA ist modifiziert! ( Kartenmod )")
	end
end
addEventHandler ( "onPlayerModInfo", getRootElement(), handleOnPlayerModInfo )

Config = {
	ServerName = "Hood Reallife",
	ForumAdress = "www.hood-rl.de",
	fraktionNamen = {
		[0]	=	"Zivilist",
		[1]	=	"LSPD",
		[2]	=	"GSG 9",
		[3]	=	"Army",
		[4]	=	"Mafia",
		[5]	=	"Groove Street",
		[6]	=	"Ballas",
		[7]	=	"Vagos",
		[8] = 	"San Andreas News"
	},
	fraktionColor = {
		[0] = {255, 255, 255},
		[1] = {19, 59, 191},
		[2] = {102,	205,	170},
		[3] = {19, 191, 59},
		[4] = {50, 50, 50}
	},
	jobNamen = {
		["none"]	=	"Arbeitslos",
		["bus"]		=	"Busfahrer",
		["truck"]	=	"Truckfahrer",
		[3]	=	"Lagerist",
		[4]	=	"Holzfaeller",
		[5]	=	"Drogendealer",
		[6]	=   "Waffendealer",
	},
	jobPrices = {
		[0]	=	0,
		[1]	=	{1850,1500,1400},
		[2]	=	{150,250,350,500},
		[3]	=	50,
		[4]	=	50,
		[5]	=	{10,25,6},-- {Pflanzen Preis,Maximale Ernte (g),Preis pro g}
		[6]	=	{
				["MatsPrice"]	=	300,
				["MatsAnzahl"]	=	100,
				["DeagleMats"]	=	50,
				["9mmMats"]	=	40,
				["M4Mats"]	=	70,
				["AK47Mats"]	=	70,
			}
	},
	licensePrice = {
		["Autofuehrerschein"]	=	1250,
		["Motorradschein"] = 950,
		["LKWfuehrerschein"]	=	2675,
		["Bootfuehrerschein"]	=	125,
		["Helicopterschein"]	=	7950,
		["Leichtfluegschein"]	=	18900,
		["Schwerfluegschein"]	=	29600,
		["Traktorschein"]	= 500,
		["Personalausweiss"]	=	80,
		["SFPass"]	=	250,
		["LVPass"]	=	250,
	},
	motorTypes = {
		["petrol"]	=	"Benzin",
		["diesel"]	=	"Diesel",
		["electric"]	=	"Elektro",
	},
	paydayZuschuesse = {
		[0]	=	1000,
		[1]	=	550,
		[2]	=	650,
		[3]	=	750,
		[4]	=	475,
		[5]	=	485,
		[6]	=	470,
		[7]	=	470,
		[8]	=	470,
		[9]	=	600,
		[10]	=	620,
	},
	busJobLineNames = {
		[1]	= "S1 : LS Bahnhof -> Stadt -> LS Bahnhof",
		[2]	=	"S2 : LS Bahnhof -> SF Bahnhof -> LS Bahnhof",
		[3]	=	"S3: LS Bahnhof -> LV Flughafen -> LS Bahnhof",
	}
}

function trim(s)
  return s:match "^%s*(.-)%s*$"
end

evelse = { [594]=true }
trailers = { [606]=true,  [607]=true, [610]=true, [590]=true, [569]=true, [611]=true, [584]=true, [608]=true, [435]=true, [450]=true, [591]=true}
rc_vehs = { [411]=true, [464]=true, [501]=true, [465]=true, [564]=true }
trains = { [537]=true, [538]=true, [569]=true, [590]=true, [537]=true, [449]=true, }
cars = { [579]=true, [400]=true, [404]=true, [489]=true, [505]=true, [479]=true, [442]=true, [458]=true, [429]=true, [411]=true, [559]=true, [541]=true, [415]=true, [561]=true, [480]=true, [560]=true, [562]=true, [506]=true, [565]=true, 
[451]=true, [434]=true, [558]=true, [555]=true, [477]=true, [503]=true, [502]=true, [494]=true, [434]=true, [565]=true, [568]=true, [557]=true, [424]=true, [504]=true, [495]=true, [539]=true, [483]=true, [508]=true, [500]=true,  [444]=true,
[556]=true, [536]=true, [575]=true, [534]=true, [567]=true, [535]=true, [576]=true, [412]=true, [459]=true, [422]=true, [482]=true, [605]=true, [530]=true, [418]=true, [582]=true, [413]=true, [440]=true, [543]=true, [583]=true, [478]=true,
[554]=true, [602]=true, [496]=true, [401]=true, [518]=true, [527]=true, [589]=true, [419]=true, [533]=true, [526]=true, [474]=true, [545]=true, [517]=true, [410]=true, [600]=true, [436]=true, [580]=true, [439]=true, [549]=true, [491]=true,
[445]=true, [604]=true, [507]=true, [585]=true, [466]=true, [492]=true, [546]=true, [551]=true, [516]=true, [467]=true, [426]=true, [547]=true, [405]=true, [409]=true, [550]=true, [566]=true, [540]=true, [529]=true, [485]=true,
[574]=true, [420]=true, [525]=true, [552]=true, [416]=true, [596]=true, [597]=true, [499]=true, [428]=true, [598]=true, [470]=true, [528]=true, [590]=true }

lkws = { [499]=true, [609]=true, [498]=true, [524]=true, [532]=true, [578]=true, [486]=true, [406]=true, [573]=true, [455]=true, [588]=true, [403]=true, [514]=true, [423]=true, [414]=true, [443]=true, [515]=true, [531]=true, [456]=true,
[433]=true, [427]=true, [407]=true, [544]=true, [432]=true, [431]=true, [437]=true, [408]=true, }

motorboats = { [472]=true, [473]=true, [493]=true, [595]=true, [430]=true, [453]=true, [452]=true, [446]=true }
bikes = { [471]=true, [523]=true, [581]=true, [521]=true, [463]=true, [522]=true, [461]=true, [468]=true, [586]=true }
raftboats = { [484]=true, [454]=true }
helicopters = { [548]=true, [425]=true, [417]=true, [487]=true, [488]=true, [497]=true, [563]=true, [447]=true, [469]=true }
planea = { [512]=true, [593]=true, [476]=true, [460]=true, [513]=true }	-- Propeller
planeb = { [592]=true, [577]=true, [511]=true, [520]=true, [553]=true, [519]=true }	-- Dsenjets