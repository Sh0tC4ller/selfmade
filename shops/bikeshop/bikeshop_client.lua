
local screenW, screenH = guiGetScreenSize()

RadioButtonBikeShop = {}
RadioButtonBikeShop[1] = guiCreateRadioButton(((screenW - 210) / 2) - 130, ((screenH - 24) / 2) -20, 210, 24, "Bike", false)
guiRadioButtonSetSelected(RadioButtonBikeShop[1], true)
RadioButtonBikeShop[2] = guiCreateRadioButton(((screenW - 210) / 2) - 130, ((screenH - 24) / 2) + 10, 210, 24, "Mountain Bike", false)
RadioButtonBikeShop[3] = guiCreateRadioButton(((screenW - 210) / 2) - 130, ((screenH - 24) / 2) + 40, 210, 24, "BMX", false)
for i = 1,3 do 
	guiSetVisible(RadioButtonBikeShop[i],false)
end

function renderFahrradHaus()
	dxDrawRectangle((screenW - 507) / 2, ((screenH - 21) / 2) - 130, 507, 21, tocolor(0, 125, 0, 255), true)
	dxDrawRectangle((screenW - 507) / 2, (screenH - 237) / 2, 507, 237, tocolor(0, 0, 0, 180), false)
	dxDrawRectangle((screenW - 507) / 2, ((screenH - 21) / 2) + 120, 507, 21, tocolor(0, 125, 0, 255), true)
	dxDrawText("Fahrrad Mieten", (screenW - 507) / 2, ((screenH - 22) / 2) - 130, ((screenW - 507) / 2) + 507, ( ((screenH - 22) / 2) - 130) + 22, tocolor(255, 255, 255, 255), 1.00, "default", "center", "center", false, false, true, false, false)
	dxDrawText("Willkommen zum Onkels Peters Fahrradhaus,\nhier kannst du dir als Neuling (Bis 10 Stunden) kostenlos ein Fahrrad mieten und danach fuer 100 Dollar.\n\nBitte waehle dein Fahrrad Typ aus.", ((screenW - 487) /2), ((screenH - 76) / 2) - 80, (((screenW - 487) /2)) + 487, ( ((screenH - 76) / 2) - 80) + 76, tocolor(255, 255, 255, 255), 1.00, "default", "left", "top", false, true, true, false, false)
	if isCursorOnElement(((screenW - 236) / 2) + 125, ((screenH - 28) / 2) + 85, 236, 28) then
		dxDrawRectangle(((screenW - 236) / 2) + 125, ((screenH - 28) / 2) + 85, 236, 28, tocolor(0, 0, 0, 255), true)
	else
		dxDrawRectangle(((screenW - 236) / 2) + 125, ((screenH - 28) / 2) + 85, 236, 28, tocolor(0, 125, 0, 255), true)
	end
	if isCursorOnElement(((screenW - 236) / 2) - 125, ((screenH - 28) / 2) + 85, 236, 28) then
		dxDrawRectangle(((screenW - 236) / 2) - 125, ((screenH - 28) / 2) + 85, 236, 28, tocolor(0, 0, 0, 255), true)
	else
		dxDrawRectangle(((screenW - 236) / 2) - 125, ((screenH - 28) / 2) + 85, 236, 28, tocolor(0, 125, 0, 255), true)
	end
	dxDrawText("Mieten", ((screenW - 237) / 2) - 125, ((screenH - 29) / 2) + 85, (((screenW - 237) / 2) - 125) + 237, ( ((screenH - 29) / 2) + 85) + 29, tocolor(255, 255, 255, 255), 1.00, "default-bold", "center", "center", false, false, true, false, false)
	dxDrawText("Abbrechen", ((screenW - 237) / 2) + 125, ((screenH - 29) / 2) + 85, (((screenW - 237) / 2) + 125) + 237, ( ((screenH - 29) / 2) + 85) + 29, tocolor(255, 255, 255, 255), 1.00, "default-bold", "center", "center", false, false, true, false, false)
end


function onFahrradMouseClick(_,state)
	if state == "down" then
		if isCursorOnElement(((screenW - 236) / 2) + 125, ((screenH - 28) / 2) + 85, 236, 28) then
			removeEventHandler("onClientRender",getRootElement(),renderFahrradHaus)
			removeEventHandler("onClientClick",getRootElement(),onFahrradMouseClick)
			for i = 1,3 do 
				guiSetVisible(RadioButtonBikeShop[i],false)
			end
			showCursor(false)
		end
		if isCursorOnElement(((screenW - 236) / 2) - 125, ((screenH - 28) / 2) + 85, 236, 28) then
			removeEventHandler("onClientRender",getRootElement(),renderFahrradHaus)
			removeEventHandler("onClientClick",getRootElement(),onFahrradMouseClick)
			for i = 1,3 do 
				guiSetVisible(RadioButtonBikeShop[i],false)
				if guiRadioButtonGetSelected(RadioButtonBikeShop[i]) then
					triggerServerEvent("bikeRent",getLocalPlayer(),getLocalPlayer(),i)
				end
			end
			showCursor(false)
		end
	end
end

addEvent("showFahrradHausMenu",true)
addEventHandler("showFahrradHausMenu",getRootElement(),function ()
	showCursor(true)
	addEventHandler("onClientRender",getRootElement(),renderFahrradHaus)
	addEventHandler("onClientClick",getRootElement(),onFahrradMouseClick)
	for i = 1,3 do 
		guiSetVisible(RadioButtonBikeShop[i],true)
	end
end)
