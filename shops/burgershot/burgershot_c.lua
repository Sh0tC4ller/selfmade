

local screenW, screenH = guiGetScreenSize()
local burgerShotNordDriveIn = createMarker(1214.9951171875,-904.75,41.919624328613,"cylinder",3)
local burgerShotNordInterior = createMarker(377.1474609375,-67.4375,1000.5078125,"cylinder",2)
setElementInterior(burgerShotNordInterior,10)

function renderBurgerShot()
	dxDrawRectangle((screenW - 351) / 2, (screenH - 21) / 2 -170, 351, 21, tocolor(0, 125, 0, 255), true)
	dxDrawRectangle((screenW - 351) / 2, (screenH - 321) / 2, 351, 321, tocolor(0, 0, 0, 200), true)
	dxDrawRectangle((screenW - 351) / 2, (screenH - 21) / 2 + 170, 351, 21, tocolor(0, 125, 0, 255), true)
	dxDrawText("Hallo Sir,\nSie sehen hunrig aus. Kann ich ihre Bestellung aufnehmen?\nIch moechte gerne ein ", (screenW - 332) / 2, (screenH - 59) / 2 - 125, ((screenW - 332) / 2) + 332, ( (screenH - 59) / 2 - 125) + 59, tocolor(255, 255, 255, 255), 1.00, "default", "left", "top", false, false, true, false, false)
	dxDrawText("Burger Shot", (screenW - 351) / 2, (screenH - 21) / 2 - 170, ((screenW - 351) / 2) + 351, ( (screenH - 21) / 2 - 170) + 21, tocolor(255, 255, 255, 255), 1.00, "default", "center", "center", false, false, true, false, false)
	if isCursorOnElement((screenW - 147) / 2 - 90, (screenH - 38) / 2 - 70, 147, 38) then
		dxDrawRectangle((screenW - 147) / 2 - 90, (screenH - 38) / 2 - 70, 147, 38, tocolor(0, 0, 0, 255), true)
	else
		dxDrawRectangle((screenW - 147) / 2 - 90, (screenH - 38) / 2 - 70, 147, 38, tocolor(0, 125, 0, 255), true)
	end
	if isCursorOnElement((screenW - 147) / 2 + 90, (screenH - 38) / 2 - 70, 147, 38) then
		dxDrawRectangle((screenW - 147) / 2 + 90, (screenH - 38) / 2 - 70, 147, 38, tocolor(0, 0, 0, 255), true)
	else
		dxDrawRectangle((screenW - 147) / 2 + 90, (screenH - 38) / 2 - 70, 147, 38, tocolor(0, 125, 0, 255), true)
	end
	dxDrawText("Chickenburger\n3$", (screenW - 143) / 2 - 90, (screenH - 39) / 2 - 70, ((screenW - 143) / 2 - 90) + 143, ( (screenH - 39) / 2 - 70) + 39, tocolor(255, 255, 255, 255), 1.00, "default", "center", "center", false, false, true, false, false)
	dxDrawText("Hamburger\n4$", (screenW - 143) / 2+90, (screenH - 39) / 2 - 70, ((screenW - 143) / 2+90) + 143, ( (screenH - 39) / 2 - 70) + 39, tocolor(255, 255, 255, 255), 1.00, "default", "center", "center", false, false, true, false, false)
	if isCursorOnElement((screenW - 147) / 2 - 90, (screenH - 38) / 2 - 5, 147, 38) then
		dxDrawRectangle((screenW - 147) / 2 - 90, (screenH - 38) / 2 - 5, 147, 38, tocolor(0, 0, 0, 255), true)
	else
		dxDrawRectangle((screenW - 147) / 2 - 90, (screenH - 38) / 2 - 5, 147, 38, tocolor(0, 125, 0, 255), true)
	end
	if isCursorOnElement((screenW - 147) / 2 + 90, (screenH - 38) / 2 - 5, 147, 38) then
		dxDrawRectangle((screenW - 147) / 2 + 90, (screenH - 38) / 2 - 5, 147, 38, tocolor(0, 0, 0, 255), true)
	else
		dxDrawRectangle((screenW - 147) / 2 + 90, (screenH - 38) / 2 - 5, 147, 38, tocolor(0, 125, 0, 255), true)
	end
	if isCursorOnElement((screenW - 147) / 2 - 90, (screenH - 38) / 2 + 70, 147, 38) then
		dxDrawRectangle((screenW - 147) / 2 - 90, (screenH - 38) / 2 + 70, 147, 38, tocolor(0, 0, 0, 255), true)
	else
		dxDrawRectangle((screenW - 147) / 2 - 90, (screenH - 38) / 2 + 70, 147, 38, tocolor(0, 125, 0, 255), true)
	end
	if isCursorOnElement((screenW - 147) / 2 + 90, (screenH - 38) / 2 + 70, 147, 38) then
		dxDrawRectangle((screenW - 147) / 2 + 90, (screenH - 38) / 2 + 70, 147, 38, tocolor(0, 0, 0, 255), true)
	else
		dxDrawRectangle((screenW - 147) / 2 + 90, (screenH - 38) / 2 + 70, 147, 38, tocolor(0, 125, 0, 255), true)
	end
	dxDrawText("Ding Menu\n10$", (screenW - 143) / 2 - 90, (screenH - 39) / 2 - 5, ((screenW - 143) / 2 - 90) + 143, ( (screenH - 39) / 2 - 5) + 39, tocolor(255, 255, 255, 255), 1.00, "default", "center", "center", false, false, true, false, false)
	dxDrawText("Unhappy Menu\n8$", (screenW - 143) / 2 + 90, (screenH - 39) / 2 - 5, ((screenW - 143) / 2 + 90) + 143, ( (screenH - 39) / 2 - 5) + 39, tocolor(255, 255, 255, 255), 1.00, "default", "center", "center", false, false, true, false, false)
	dxDrawText("Short Chicken\n5$", (screenW - 143) / 2 - 90, (screenH - 39) / 2 + 70, ((screenW - 143) / 2 - 90) + 143, ( (screenH - 39) / 2 + 70) + 39, tocolor(255, 255, 255, 255), 1.00, "default", "center", "center", false, false, true, false, false)
	dxDrawText("Fish Burger\n5$", (screenW - 143) / 2 + 90, (screenH - 39) / 2 + 70, ((screenW - 143) / 2 + 90) + 143, ( (screenH - 39) / 2 + 70) + 39, tocolor(255, 255, 255, 255), 1.00, "default", "center", "center", false, false, true, false, false)
	if isCursorOnElement((screenW - 192) / 2, (screenH - 33) / 2 + 130, 192, 33) then
		dxDrawRectangle((screenW - 192) / 2, (screenH - 33) / 2 + 130, 192, 33, tocolor(0, 0, 0, 255), true)
	else
		dxDrawRectangle((screenW - 192) / 2, (screenH - 33) / 2 + 130, 192, 33, tocolor(0, 125, 0, 255), true)
	end
	dxDrawText("Schliessen", (screenW - 192) / 2, (screenH - 35) / 2 + 130, ((screenW - 192) / 2) + 192, ( (screenH - 35) / 2 + 130) + 35, tocolor(255, 255, 255, 255), 1.00, "default", "center", "center", false, false, true, false, false)
end

function burgerShotClick(btn,state)
	if btn == "left" and state == "down" then
		if isCursorOnElement((screenW - 192) / 2, (screenH - 33) / 2 + 130, 192, 33) then --Schliessen
			showCursor(false)
			removeEventHandler("onClientRender",getRootElement(),renderBurgerShot)
			removeEventHandler("onClientClick",getRootElement(),burgerShotClick)
		end
		if isCursorOnElement((screenW - 147) / 2 - 90, (screenH - 38) / 2 - 70, 147, 38) then --Chickenburger\n10$
			showCursor(false)
			removeEventHandler("onClientRender",getRootElement(),renderBurgerShot)
			removeEventHandler("onClientClick",getRootElement(),burgerShotClick)
			triggerServerEvent("buyBurger",localPlayer,"chicken")
		end
		if isCursorOnElement((screenW - 147) / 2 + 90, (screenH - 38) / 2 - 70, 147, 38) then--Hamburger
			showCursor(false)
			removeEventHandler("onClientRender",getRootElement(),renderBurgerShot)
			removeEventHandler("onClientClick",getRootElement(),burgerShotClick)
			triggerServerEvent("buyBurger",localPlayer,"hamburger")
		end
		if isCursorOnElement((screenW - 147) / 2 - 90, (screenH - 38) / 2 - 5, 147, 38) then --Dingmenu
			showCursor(false)
			removeEventHandler("onClientRender",getRootElement(),renderBurgerShot)
			removeEventHandler("onClientClick",getRootElement(),burgerShotClick)
			triggerServerEvent("buyBurger",localPlayer,"dingmenu")
		end
		if isCursorOnElement((screenW - 147) / 2 + 90, (screenH - 38) / 2 - 5, 147, 38) then--Unhappy Menu\n10$
			showCursor(false)
			removeEventHandler("onClientRender",getRootElement(),renderBurgerShot)
			removeEventHandler("onClientClick",getRootElement(),burgerShotClick)
			triggerServerEvent("buyBurger",localPlayer,"unhappy")
		end
		if isCursorOnElement((screenW - 147) / 2 - 90, (screenH - 38) / 2 + 70, 147, 38) then--Short Chicken\n0$
			showCursor(false)
			removeEventHandler("onClientRender",getRootElement(),renderBurgerShot)
			removeEventHandler("onClientClick",getRootElement(),burgerShotClick)
			triggerServerEvent("buyBurger",localPlayer,"shortchicken")
		end
		if isCursorOnElement((screenW - 147) / 2 + 90, (screenH - 38) / 2 + 70, 147, 38) then--Fish
			showCursor(false)
			removeEventHandler("onClientRender",getRootElement(),renderBurgerShot)
			removeEventHandler("onClientClick",getRootElement(),burgerShotClick)
			triggerServerEvent("buyBurger",localPlayer,"fishburger")
		end
	end
end

function openBurgerShot(hit)
	if (hit == localPlayer) then
		showCursor(true)
		addEventHandler("onClientRender",getRootElement(),renderBurgerShot)
		addEventHandler("onClientClick",getRootElement(),burgerShotClick)
	end
end
addEventHandler("onClientMarkerHit",burgerShotNordDriveIn,openBurgerShot)
addEventHandler("onClientMarkerHit",burgerShotNordInterior,openBurgerShot)