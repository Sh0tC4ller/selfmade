local burgershotped = createPed( 205, 377.18859863281, -65.427398681641, 1001.8120117188, 180, false)
setElementInterior(burgershotped, 10)
setTimer( function()setElementFrozen(burgershotped, true) end, 1000, 1)


burgerPrices = {
	["chicken"]	=	3,
	["hamburger"]	=	4,
	["dingmenu"]	=	10,
	["unhappy"]	=	8,
	["shortchicken"]	=	5,
	["fishburger"]	=	4,
}

burgerHeal = {
	["chicken"]	=	5,
	["hamburger"]	=	7,
	["dingmenu"]	=	20,
	["unhappy"]	=	15,
	["shortchicken"]	=	17,
	["fishburger"]	=	13,
}

addEvent("buyBurger",true)
addEventHandler("buyBurger",getRootElement(),function (typ )
	if burgerPrices[typ] then
		local price = burgerPrices[typ]
		local hunger = getElementData(client, "Hunger")
		local money = getElementData(client, "Geld")
		if burgerHeal[typ] then
			if money >= price then
				if hunger > 0 and hunger <= 100 then
					takePlayerMoney(client,price)
					removeHungerLevel(client, burgerHeal[typ])
					infoBox(client, "Du gibst der Burgershotmitarbeitern dein Geld und isst dein Essen", 0, 125, 0, 5000)
				else
					infoBox(client, "Du bist noch nicht hungrig", 125, 0, 0, 5000 )	
				end
			else
				infoBox(client, "Du hast nicht genug Geld für dieses Essen", 125, 0, 0, 5000)
			end
		end
	else
		infoBox(client,"Error",125,0,0,5000)
	end
end)


createTeleportMarker(1199.3310546875,-918.2705078125,42.121757507324,0,0,366.66015625,-73.40234375,1001.5078125,10,0)
createTeleportMarker(362.8291015625,-75.126953125,1000.5078125,10,0,1200.462890625,-923.404296875,43.014408111572,0,0)