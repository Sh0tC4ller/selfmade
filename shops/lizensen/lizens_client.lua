
local screenW, screenH = guiGetScreenSize()
local currentID = 0
local currentLic = ""

local marker = createMarker(362.300078125,173.634765625,1007.3828125,"cylinder",1.5)
setElementInterior(marker,3)

addEventHandler("onClientMarkerHit",marker,function (player)
	if (player == localPlayer ) then
		showCursor(true)
		addEventHandler("onClientRender",getRootElement(),renderLizensen)
		addEventHandler("onClientClick",getRootElement(),onClickLizensen)
		currentLic = "Motorradschein"
	end
end)

function onClickLizensen(btn,state)
	if btn == "left" then
		if state == "down" then
			if isCursorOnElement(((screenW - 252) / 2) + 130, ((screenH - 35) / 2) + 150, 252, 35) then --Abbrechen
				showCursor(false)
				removeEventHandler("onClientRender",getRootElement(),renderLizensen)
				removeEventHandler("onClientClick",getRootElement(),onClickLizensen)
			end
			local count = 0
			for name,price in pairs(Config.licensePrice) do 
				count = count + 18
				if isCursorOnElement(((screenW - 255) / 2) - 125, ((screenH - 22) / 2) - 105 + count, 200,20) then
					currentID = count
					currentLic = name
				end
			end
			if isCursorOnElement(((screenW - 252) / 2) - 130, ((screenH - 35) / 2) + 150, 252, 35) then --Kaufen
				if (currentLic == "") then
					infoBox(player,"Bitte w�hle ein Schein aus!",125,0,0,5000)
				else
					triggerServerEvent("lizensBuy",getLocalPlayer(),currentLic)
				end
			end
		end
	else
		currentID = 0
		currentLic = ""
	end
end

function renderLizensen()
	dxDrawRectangle((screenW - 530) / 2, (screenH - 379) / 2, 530, 379, tocolor(0, 0, 0, 200), false)
	dxDrawRectangle((screenW - 530) / 2, ((screenH - 21) / 2) - 200, 530, 21, tocolor(0, 125, 0, 255), true)
	dxDrawRectangle((screenW - 530) / 2, ((screenH - 21) / 2) + 200, 530, 21, tocolor(0, 125, 0, 255), true)
	dxDrawText("Willkommen Sir,\nsie sehen aus als braeuchten sie Scheine.Hier haben sie eine Liste unserer Scheine inklusive der Preise.", (screenW - 515) / 2, ((screenH - 57) / 2) - 160, ((screenW - 515) / 2) + 515, ( ((screenH - 57) / 2) - 160) + 57, tocolor(255, 255, 255, 255), 1.00, "default", "left", "top", false, true, true, false, false)
	dxDrawRectangle((screenW - 513) / 2, (screenH - 247) / 2, 513, 247, tocolor(0, 0, 0, 230), true)
	dxDrawLine((screenW - 511) / 2,  ((screenH - 0) / 2) - 100, ((screenW - 511) / 2) + 511, ( ((screenH - 0) / 2) - 100) + 0, tocolor(255, 255, 255, 255), 1, true)
	dxDrawLine(((screenW - 0) / 2) - 20,  (screenH - 245) / 2, (((screenW - 0) / 2) - 20) + 0, ( (screenH - 245) / 2) + 245, tocolor(255, 255, 255, 255), 1, true)
	dxDrawLine(((screenW - 0) / 2) + 120,  (screenH - 245) / 2, (((screenW - 0) / 2) + 120) + 0, ( (screenH - 245) / 2) + 245, tocolor(255, 255, 255, 255), 1, true)
	dxDrawText("Lizensenkaufen", (screenW - 530) / 2, ((screenH - 23) / 2) - 200, ((screenW - 530) / 2) + 530, ( ((screenH - 23) / 2) - 200) + 23, tocolor(255, 255, 255, 255), 1.00, "default-bold", "center", "center", false, false, true, false, false)
	if isCursorOnElement(((screenW - 252) / 2) - 130, ((screenH - 35) / 2) + 150, 252, 35) then
		dxDrawRectangle(((screenW - 252) / 2) - 130, ((screenH - 35) / 2) + 150, 252, 35, tocolor(0, 0, 0, 255), true)
	else
		dxDrawRectangle(((screenW - 252) / 2) - 130, ((screenH - 35) / 2) + 150, 252, 35, tocolor(0, 125, 0, 255), true)
	end
	if isCursorOnElement(((screenW - 252) / 2) + 130, ((screenH - 35) / 2) + 150, 252, 35) then
		dxDrawRectangle(((screenW - 252) / 2) + 130, ((screenH - 35) / 2) + 150, 252, 35, tocolor(0, 0, 0, 255), true)
	else
		dxDrawRectangle(((screenW - 252) / 2) + 130, ((screenH - 35) / 2) + 150, 252, 35, tocolor(0, 125, 0, 255), true)
	end
	dxDrawText("Kaufen", ((screenW - 248) / 2) - 130, ((screenH - 35) / 2) + 150, (((screenW - 248) / 2) - 130) + 248, ( ((screenH - 35) / 2) + 150) + 35, tocolor(255, 255, 255, 255), 1.00, "default-bold", "center", "center", false, false, true, false, false)
	dxDrawText("Abbrechen", ((screenW - 248) / 2) + 130, ((screenH - 35) / 2) + 150, (((screenW - 248) / 2) + 130) + 248, ( ((screenH - 35) / 2) + 150) + 35, tocolor(255, 255, 255, 255), 1.00, "default-bold", "center", "center", false, false, true, false, false)
	dxDrawText("Schein", ((screenW - 255) / 2) - 125, ((screenH - 22) / 2) - 112, (((screenW - 255) / 2) - 125) + 255, ( ((screenH - 22) / 2) - 112) + 22, tocolor(255, 255, 255, 255), 1.00, "default", "center", "top", false, false, true, false, false)
	dxDrawText("Preis", ((screenW - 151) / 2) + 60, ((screenH - 22) / 2) - 112, (((screenW - 151) / 2) + 60) + 151, ( ((screenH - 22) / 2) - 112) + 22, tocolor(255, 255, 255, 255), 1.00, "default", "center", "top", false, false, true, false, false)
	dxDrawText("Gekauft?", ((screenW - 151) / 2) + 190, ((screenH - 22) / 2) - 112, (((screenW - 151) / 2) + 190) + 151, ( ((screenH - 22) / 2) - 112) + 22, tocolor(255, 255, 255, 255), 1.00, "default", "center", "top", false, false, true, false, false)
	local count = 0
	for name,price in pairs(Config.licensePrice) do 
		count = count + 18
		if isCursorOnElement(((screenW - 255) / 2) - 125, ((screenH - 22) / 2) - 105 + count, 200,20) then
			dxDrawText(name, ((screenW - 255) / 2) - 125, ((screenH - 22) / 2) - 105 + count, (((screenW - 255) / 2) - 125) + 255, ( ((screenH - 22) / 2) - 112) + 22, tocolor(255, 255, 255, 255), 1.00, "default-bold", "center", "top", false, false, true, false, false)
			dxDrawText(price, ((screenW - 151) / 2) + 60, ((screenH - 22) / 2) - 105+ count, (((screenW - 151) / 2) + 60) + 151, ( ((screenH - 22) / 2) - 112) + 22, tocolor(255, 255, 255, 255), 1.00, "default-bold", "center", "top", false, false, true, false, false)
			dxDrawText(getLicState(getElementData(localPlayer,name)), ((screenW - 151) / 2) + 190, ((screenH - 22) / 2) - 105+ count, (((screenW - 151) / 2) + 190) + 151, ( ((screenH - 22) / 2) - 112) + 22, tocolor(255, 255, 255, 255), 1.00, "default-bold", "center", "top", false, false, true, false, false)
		else
			if (currentID == count) then
				dxDrawText(name, ((screenW - 255) / 2) - 125, ((screenH - 22) / 2) - 105+ count, (((screenW - 255) / 2) - 125) + 255, ( ((screenH - 22) / 2) - 112) + 22, tocolor(255, 255, 255, 255), 1.00, "default-bold", "center", "top", false, false, true, false, false)
				dxDrawText(price, ((screenW - 151) / 2) + 60, ((screenH - 22) / 2) - 105+ count, (((screenW - 151) / 2) + 60) + 151, ( ((screenH - 22) / 2) - 112) + 22, tocolor(255, 255, 255, 255), 1.00, "default-bold", "center", "top", false, false, true, false, false)
				dxDrawText(getLicState(getElementData(localPlayer,name)), ((screenW - 151) / 2) + 190, ((screenH - 22) / 2) - 105+ count, (((screenW - 151) / 2) + 190) + 151, ( ((screenH - 22) / 2) - 112) + 22, tocolor(255, 255, 255, 255), 1.00, "default-bold", "center", "top", false, false, true, false, false)
			else
				dxDrawText(name, ((screenW - 255) / 2) - 125, ((screenH - 22) / 2) - 105+ count, (((screenW - 255) / 2) - 125) + 255, ( ((screenH - 22) / 2) - 112) + 22, tocolor(255, 255, 255, 255), 1.00, "default", "center", "top", false, false, true, false, false)
				dxDrawText(price, ((screenW - 151) / 2) + 60, ((screenH - 22) / 2) - 105+ count, (((screenW - 151) / 2) + 60) + 151, ( ((screenH - 22) / 2) - 112) + 22, tocolor(255, 255, 255, 255), 1.00, "default", "center", "top", false, false, true, false, false)
				dxDrawText(getLicState(getElementData(localPlayer,name)), ((screenW - 151) / 2) + 190, ((screenH - 22) / 2) - 105+ count, (((screenW - 151) / 2) + 190) + 151, ( ((screenH - 22) / 2) - 112) + 22, tocolor(255, 255, 255, 255), 1.00, "default", "center", "top", false, false, true, false, false)
			end
		end
	end
end