Ban = {}

-- Normale Funktionen überschreiben --
Ban.banPlayer = banPlayer
Ban.addBan = addBan

function Ban.init()
	addEventHandler("onPlayerConnect", getRootElement(), Ban.playerConnect)

	-- Commands --
	addCommandHandler("tban", Ban.tBan)
	addCommandHandler("rban", Ban.rBan)
	addCommandHandler("unban", Ban.unban)
	addCommandHandler("rkick", Ban.rKick)

	outputDebugString("Ban System initialisiert!")
end

function Ban.destructor()
	removeEventHandler("onPlayerConnect", getRootElement(), Ban.playerConnect)

	removeCommandHandler("tban", Ban.tBan)
	removeCommandHandler("rban", Ban.rBan)
	removeCommandHandler("unban", Ban.unban)
	removeCommandHandler("rkick", Ban.rKick)
end

function Ban.banPlayer(pPlayer, pIP, pUsername, pSerial, admin, reason, time ) 
	if pPlayer ~= nil then
		Ban.rBan("Console", _, pPlayer, reason)
	elseif pPlayer ~= nil and time ~= nil then	
		Ban.tBan("Console", _, pPlayer, "h=0;m=0;s="..time, reason)
	end
end

function Ban.addBan(ip, username, serial, admin, reason, seconds)
	return false
end

function Ban.playerConnect(pName, pIP, pUsername, pSerial)
	local sql = dbQuery(Mysql.handler, "SELECT * FROM `bans` WHERE `username` = ? AND `serial` = ?;", pName, pSerial, pIP)
	local result, num = dbPoll(sql, -1)

	if num > 0 then
		for _, spalte in ipairs(result) do
			if(spalte["type"] == "perm") then
				cancelEvent(true, "Du bist auf dem Server permanent von dem Admin "..spalte["admin"].." gebannt!")
			elseif(spalte["type"] == "temp") then
				if(spalte["bantime_expire"] - getRealTime().timestamp) <= 0 then
					dbExec(Mysql.handler, "DELETE FROM `bans` WHERE `username` = ? AND `serial` = ?;", pName, pSerial, pIP)
				else
					local restTime = ""

					if (spalte["bantime_expire"] - getRealTime().timestamp) < 60 then
						restTime = ("%s Sekunden"):format(((spalte["bantime_expire"] - getRealTime().timestamp) - (math.floor((spalte["bantime_expire"] - getRealTime().timestamp)/60)*60)))
					elseif (spalte["bantime_expire"] - getRealTime().timestamp) > 3600 then
						restTime = ("%s Stunden, %s Minuten und %s Sekunden"):format(math.floor(((spalte["bantime_expire"] - getRealTime().timestamp)/60)/60), (math.floor((spalte["bantime_expire"] - getRealTime().timestamp)/60) - (math.floor(((spalte["bantime_expire"] - getRealTime().timestamp)/60)/60))*60), ((spalte["bantime_expire"] - getRealTime().timestamp) - (math.floor((spalte["bantime_expire"] - getRealTime().timestamp)/60)*60)))
					elseif (spalte["bantime_expire"] - getRealTime().timestamp) >= 120 then
						restTime = ("%s Minuten und %s Sekunden"):format((math.floor((spalte["bantime_expire"] - getRealTime().timestamp)/60)), ((spalte["bantime_expire"] - getRealTime().timestamp) - (math.floor((spalte["bantime_expire"] - getRealTime().timestamp)/60)*60)))
					elseif (spalte["bantime_expire"] - getRealTime().timestamp) >= 60 then
						restTime = ("%s Minute und %s Sekunden"):format((math.floor((spalte["bantime_expire"] - getRealTime().timestamp)/60)), ((spalte["bantime_expire"] - getRealTime().timestamp) - (math.floor((spalte["bantime_expire"] - getRealTime().timestamp)/60)*60)))
					end

					cancelEvent(true, "Du bist auf dem Server temporär von dem Admin "..spalte["admin"].." gebannt worden. Du bist noch "..restTime.." gebannt!")

				end
			else
				cancelEvent(true, "In der Datenbank wurde etwas falsches eingetragen. Bitte melde dies einem Admin!")
			end
		end
	end
	dbFree(sql)
end

function Ban.rBan(admin, _, player, ...)
	if AdminSys.hasPermission(admin, "banPerm") or admin == "Console"then
		local reason = table.concat( {...}, " " )
		if player ~= nil and  reason ~=  "" then
			if getPlayerFromName(player) then
				dbExec(Mysql.handler, "INSERT INTO `bans`(`username`, `serial`, `admin`, `reason`, `type`, `bantime`, `bantime_expire`) VALUES (?,?,?,?,?,?,?);", player, getPlayerSerial(getPlayerFromName(player)), getPlayerName(admin), reason, "perm", getRealTime().timestamp, "/")
				triggerEvent("AdminshowKick", admin, getPlayerFromName(player), "Banned", reason, admin, "perm")
				outputChatBox("Du hast den Spieler "..player.." permanent gebannt", admin, 0, 125, 0)
				kickPlayer(getPlayerFromName(player), getPlayerName(admin), "Du wurdest von "..getPlayerName(admin).." permanent vom Server gebannt!")
			else
				local sql = dbQuery(Mysql.handler, "SELECT `Serial` FROM `user` WHERE `Name` = ?;", player)
            	local result, num = dbPoll(sql, -1)

            	if (num > 0) then
            		for i, spalte in ipairs(result) do
            			dbExec(Mysql.handler, "INSERT INTO `bans` (`username`, `serial`, `admin`, `reason`, `type`, `bantime`, `bantime_expire`) VALUES (?,?,?,?,?,?,?);", player, spalte["Serial"], getPlayerName(admin), reason, "perm", getRealTime().timestamp, "/")
            			outputChatBox("Du hast den Spieler "..player.." permanent gebannt", admin, 0, 125, 0)
            			triggerEvent("AdminshowKick", admin, player, "Banned", reason, admin, "perm")
            		end
            	else
            		outputChatBox("Spieler nicht gefunden!", admin, 125, 0, 0)
            	end
			end
		end
	end
end

function Ban.tBan(admin, _, player, time, ...)
	if AdminSys.hasPermission(admin, "banTemp") or admin == "Console" then
		local reason = table.concat( {...}, " " )

		if (player ~= nil and time ~= nil and reason ~= " ") then
			if (time:find("h") and time:find("m") and time:find("s")) then
				local bantime = 0
				
				for _, value in ipairs(split(time, ";")) do
					if (split(value, "=")[1] == "h") then
						bantime = bantime + (split(value, "=")[2]*60)*60
					elseif (split(value, "=")[1] == "m") then
						if (tonumber(split(value, "=")[2]) <= 60) then
							bantime = bantime + split(value, "=")[2]*60
						end
					elseif (split(value, "=")[1] == "s") then
						if (tonumber(split(value, "=")[2]) <= 60) then
							bantime = bantime + split(value, "=")[2]
						end
					end
	            end
	            local restZeit = getRealTime().timestamp + bantime
	            local restAusgabe = "" 
	            if (restZeit - getRealTime().timestamp) < 60 then
						restAusgabe = ("%s Sekunden"):format(((restZeit - getRealTime().timestamp) - (math.floor((restZeit - getRealTime().timestamp)/60)*60)))
					elseif (restZeit - getRealTime().timestamp) > 3600 then
						restAusgabe = ("%s Stunden, %s Minuten und %s Sekunden"):format(math.floor(((restZeit - getRealTime().timestamp)/60)/60), (math.floor((restZeit - getRealTime().timestamp)/60) - (math.floor(((restZeit - getRealTime().timestamp)/60)/60))*60), ((restZeit - getRealTime().timestamp) - (math.floor((restZeit - getRealTime().timestamp)/60)*60)))
					elseif (restZeit - getRealTime().timestamp) >= 120 then
						restAusgabe = ("%s Minuten und %s Sekunden"):format((math.floor((restZeit - getRealTime().timestamp)/60)), ((restZeit - getRealTime().timestamp) - (math.floor((restZeit - getRealTime().timestamp)/60)*60)))
					elseif (restZeit - getRealTime().timestamp) >= 60 then
						restAusgabe = ("%s Minute und %s Sekunden"):format((math.floor((restZeit - getRealTime().timestamp)/60)), ((restZeit - getRealTime().timestamp) - (math.floor((restZeit - getRealTime().timestamp)/60)*60)))
					end

	            if (getPlayerFromName(player)) then
	            	dbExec(Mysql.handler, "INSERT INTO `bans`(`username`, `serial`, `admin`, `reason`, `type`, `bantime`, `bantime_expire`) VALUES (?,?,?,?,?,?,?);", player, getPlayerSerial(getPlayerFromName(player)) , getPlayerName(admin), reason, "temp", getRealTime().timestamp, (getRealTime().timestamp + bantime))
					outputChatBox("Du hast den Spieler "..player.." für "..restAusgabe.." gebannt", admin, 0, 125, 0)
					triggerEvent("AdminshowKick", admin, getPlayerFromName(player), "Banned", reason, admin, "temp")
					kickPlayer(getPlayerFromName(player), getPlayerName(admin), "Du bist nun temporär für "..restAusgabe.." vom Server gebannt! Grund: "..reason)
	            else
	            	local sql = dbQuery(Mysql.handler, "SELECT `Serial` FROM `user` WHERE `Name` = ?;", player)
	            	local result, num = dbPoll(sql, -1)

	            	if (num > 0) then
	            		for i, spalte in ipairs(result) do
	            			outputChatBox("Du hast den Spieler "..player.." für "..restAusgabe.." temporär gebannt", admin, 0, 125, 0)
	            			dbExec(Mysql.handler, "INSERT INTO `bans`(`username`, `serial`, `admin`, `reason`, `type`, `bantime`, `bantime_expire`) VALUES (?,?,?,?,?,?,?);", player, spalte["Serial"], getPlayerName(admin), reason, "temp", getRealTime().timestamp, (getRealTime().timestamp + bantime))
	            			triggerEvent("AdminshowKick", admin, player, "Banned", reason, admin, "perm")
	            		end
	            	else
	            		outputChatBox("Spieler nicht gefunden!", admin, 125, 0, 0)
	            	end
	            end
	        end
	    end
	end
end

function Ban.unban(admin, _, player )
	if AdminSys.hasPermission(admin, "banUnban") then
		if player ~= nil then
			local sql = dbQuery(Mysql.handler, "SELECT * FROM `bans` WHERE `username` = ?;", player)
			local result, num = dbPoll(sql, -1)
			
			if num > 0 then
				for _, spalte in ipairs(result) do
					dbExec(Mysql.handler, "DELETE FROM `bans` WHERE `username` = ?;", player)
					outputChatBox("Du hast den Spieler "..player.." entbannt!", admin, 0, 125, 0)
				end
			else
				outputChatBox("Spieler ist nicht gebannt!", admin, 125, 0, 0)
			end
		end
	end
end

function Ban.rKick(admin, _, player, ...)
	if AdminSys.hasPermission(admin, "banKick") then
		local reason = table.concat( {...}, " " )
		if player ~= nil and reason ~= " " then
			if getPlayerFromName(player) then
				triggerEvent("AdminshowKick", admin, getPlayerFromName(player), "Kicked", reason, admin)

				outputChatBox("Du hast den Spieler wegen \""..reason.."\" von Server gekickt!", admin, 0, 125, 0)
				kickPlayer(getPlayerFromName(player), getPlayerName(admin), reason)
			else
				outputChatBox("Der Spieler ist nicht online!", admin, 125, 0, 0)
			end
		end 
	end
end
-- Ban System starten --
Ban.init()