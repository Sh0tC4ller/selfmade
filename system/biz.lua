Biz = {}
Biz.__index = Biz
Biz.count = 0

function Biz:add( bizname, x, y, z )
	local self = setmetatable({}, Biz)
	self.__index = self
	local count = Biz.count + 1
	Biz.count = count
	self.id = count
	self.besitzer = "none"
	self.name = bizname
	self.description = "nil"
	self.x = x
	self.y = y
	self.z = z
	self.kasse = 0
	self:saveUnique()
	self:loadDataFromDatabase()
	local pickup = createPickup(self.x, self.y, self.z, 3, 1247, 1000)
	setElementData(pickup, "id", self.id)
	addEventHandler("onPickupHit", pickup, function (hit)
		if getElementType(hit) == "player" then
			if self.besitzer == "none" then
				triggerClientEvent(hit, "showBizBuy", hit, self.id, self.name, self.description, pickup ) -- argument Biz ID...
				infoBox(hit, "Du kannst das Unternehmen kaufen... Kommt noch...", 0, 125, 0, 5000)
			else
				infoBox(hit, "Das Unternehmen:\n"..self.name.."\ngehört:\n\n"..self.besitzer, 125, 125, 0, 5000)
			end
		end
	end)

	addEvent("onBuyBiz", true)
	addEventHandler("onBuyBiz", getRootElement(), function (kaufer, id)
		if id == self.id then
			local query = dbQuery(sqlcon, "SELECT * FROM biz WHERE `Besitzer` = '"..getPlayerName(kaufer).."'")
			local result, num = dbPoll(query, -1)
			if num == 0 then
				self:setBesitzer(getPlayerName(kaufer))
				infoBox(kaufer, "Du hast das Unternehmen gekauft!", 0, 125, 0, 5000)
			else
				infoBox(kaufer, "Du besitzt bereits ein Unternehmen!", 125, 0, 0, 3000)	
			end
		end
	end)
	return self
end

function Biz:saveUnique()
	local queryGet = dbQuery(Mysql.handler, "SELECT * FROM `biz` WHERE `ID` = '?'", self.id)
	local result, num = dbPoll(queryGet, -1)
	if num == 0 then
		local querySet = dbQuery(Mysql.handler, "INSERT INTO `biz` (Name, Besitzer, Kasse, X, Y, Z, Description) VALUES (?, ?, ?, ?, ?, ?, ?)", self.name, self.besitzer, self.kasse, self.x, self.y, self.z, self.description)
		dbPoll(querySet, -1)
	end
end

function Biz:loadDataFromDatabase()
	local query = dbQuery(Mysql.handler, "SELECT * FROM `biz` WHERE `ID` = '?'", self.id)
	local result, num = dbPoll(query, -1)
	if num == 1 then
		self.besitzer = result[1]["Besitzer"]
		self.kasse = result[1]["Kasse"] 
		self.description = result[1]["Description"]
	end
end

function Biz:saveDataForDatabase()
	local query = dbQuery(Mysql.handler, "UPDATE `biz` SET `Besitzer`= '"..self.besitzer.."' , `Kasse`= '"..self.kasse.."', `Description` = '"..self.description.."' WHERE `ID` = '"..self.id.."'")
	dbPoll(query, -1)
end

function Biz:setBesitzer( name )
	self.besitzer = name
	self:saveDataForDatabase()
	self:loadDataFromDatabase()
end

function Biz:setDescription ( desc )
	self.description = desc
	self:saveDataForDatabase()
	self:loadDataFromDatabase()
end

function Biz:setKasse( amount )
	self.kasse = amount
	self:saveDataForDatabase()
	self:loadDataFromDatabase()
end

function Biz:getKasse()
	return self.kasse
end

function Biz:getBesitzer()
	return self.besitzer
end