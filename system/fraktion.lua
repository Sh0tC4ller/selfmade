
function isPlayerInTable ( player,the_table)
	local check = false
	for pl,_ in pairs(the_table) do 
		if pl == player then
			check = true
		end
	end
	return check
end

Fraktion = {}
Fraktion.__index = Fraktion
Fraktion.count = 0

function Fraktion:New(factioname )
	local self = setmetatable({},Fraktion)
	self.__index = self
	local count = Fraktion.count + 1
	Fraktion.count = count
	self.id = count
	self.vehicles = {}
	self.tore = {}
	self.ranks = {}
	self.waffen = {}
	self.name = factioname
	self.skins = {}
	addEventHandler("onPlayerChat",getRootElement(),function ( message, messageType)
		if messageType == 2 then
			cancelEvent()
			if tonumber(getElementData(source,"Fraktion")) == self.id then
				for _,player in ipairs(getElementsByType("player")) do 
					if getElementData(player,"Fraktion") then
						if tonumber(getElementData(player,"Fraktion")) == self.id then
							outputChatBox("[ "..self.name.."| "..self.ranks[tonumber(getElementData(source, "Fraktionrang"))].." ] "..getPlayerName(source).." : "..message..".",player,Config.fraktionColor[self.id][1], Config.fraktionColor[self.id][2], Config.fraktionColor[self.id][3],true)
						end
					end
				end
			end
		end
	end)

	if self.id > 3 then
		self:getWeaponData()
		setTimer(function() self:saveWeaponData() end, 1800000, -1)
	end

	return self
end

function Fraktion:getWeaponData()
	local sql = dbQuery(Mysql.handler, "SELECT * FROM `fraktionsWaffen` WHERE FraktionsID = '?'", tonumber(self.id))
	local result, num, err = dbPoll(sql, -1)
	if num == 1 then
		self.waffen = fromJSON(result[1]["Waffen"])
	else
		outputDebugString("Konnte keine WaffenDaten für die Fraktion ".. self.name.." finden!")
	end
end

function Fraktion:addWeaponPart(wepTeil, menge)
	if type(wepTeil) == "string" and type(menge) == "number" then
		self.waffen[wepTeil] = self.waffen[wepTeil] + tonumber(menge)
	else
		assert(type(wepTeil) == "string", "Fehler bei 'addWeaponPart' (Erwarte String als Argument1, habe "..type(wepTeil))
		assert(type(menge) == "number", "Fehler bei 'addWeaponPart' (Erwarte Number als Argument 2, habe "..type(menge))
	end
end

function Fraktion:removeWeaponPart(wepTeil, menge)
	if type(wepTeil) == "string" and type(menge) == "number" then
		self.waffen[wepTeil] = self.waffen[wepTeil] - menge
	else
		assert(type(wepTeil) == "string", "Fehler bei 'addWeaponPart' (Erwarte String als Argument1, habe "..type(wepTeil))
		assert(type(menge) == "number", "Fehler bei 'addWeaponPart' (Erwarte Number als Argument 2, habe "..type(menge))
	end
end

function Fraktion:getWeaponPart(wepTeil)
	if type(wepTeil) == "string" then
		return self.waffen[wepTeil]
	end
end

function Fraktion:saveWeaponData()
	local sql = dbQuery(Mysql.handler, "UPDATE `fraktionsWaffen` SET Waffen = '"..toJSON(self.waffen).."' WHERE FraktionsID = '"..self.id.."'")
	local result = dbPoll(sql, -1)
	if not result then
		outputDebugString("Fehler beim Speichern der Fraktions Waffen!", 0, 255, 0, 0)
	end
end


function Fraktion:addRank( rankname )
	table.insert(self.ranks,rankname)
end

function Fraktion:addCar ( vehid, x,y,z ,rz )
	local car = createVehicle(vehid,x,y,z,0,0,rz, self.name)
	setVehicleColor(car,Config.fraktionColor[self.id][1], Config.fraktionColor[self.id][2], Config.fraktionColor[self.id][3], Config.fraktionColor[self.id][1], Config.fraktionColor[self.id][2], Config.fraktionColor[self.id][3])
	setElementData(car, "Besitzer", self.name)
	addEventHandler("onVehicleEnter",car,function (player ,seat)
		if seat == 0 then
			if tonumber(getElementData(player,"Fraktion")) ~= self.id then
				infoBox(player, "Du musst in der Fraktion "..self.name.." sein", 125, 0, 0, 5000)
				setControlState ( player, "enter_exit", false )
				setTimer ( removePedFromVehicle, 750, 1, player )
				setTimer ( setControlState, 150, 1, player, "enter_exit", false )
				setTimer ( setControlState, 200, 1, player, "enter_exit", true )
				setTimer ( setControlState, 700, 1, player, "enter_exit", false )
				setTimer ( setVehicleDoorState, 800, 1, car, 2, 0)
			end
		end
		if getElementData(player, "Fraktion") == 1 or getElementData(player, "Fraktion") == 2 or getElementData(player, "Fraktion") == 3 then
			if seat == 0 or seat == 1 then
				bindKey(player, "C", "down", showPolicePC)
			end
		end
	end)
	addEventHandler("onVehicleStartExit", car, function(player, seat)
		if getElementData(player, "Fraktion") == 1 or getElementData(player, "Fraktion") == 2 or getElementData(player, "Fraktion") == 3 then
			if seat == 0 or seat == 1 then
				unbindKey(player, "C", "down", showPolicePC)
			end
		end
	end)
	table.insert(self.vehicles,car)
end

function Fraktion:addTor ( object,x,y,z, rot)
	local tor = createObject(object,x,y,z,0,0,rot)
	local colshape = createColSphere(x,y,z,15)
	setElementData(tor,"state",false)
	setElementData(tor,"Z",z)
	addEventHandler("reallife:onLogin",getRootElement(),function ()
		bindKey(source,"h","down",function ( source )
			if isElementWithinColShape(source,colshape) then
				if tonumber(getElementData(source,"Fraktion")) == self.id then
					if getElementData(tor,"state") == false then
						setElementData(tor,"state",true)
						moveObject(tor,2500,x,y,z-15)
					else
						setElementData(tor,"state",false)
						moveObject(tor,2500,x,y,z)
					end
				end
			end
		end, source)
	end)
	table.insert(self.tore, tor)
end

function showPolicePC(player)
	triggerClientEvent(player, "showPolicePC", player)
end

function Fraktion:setDutyPoint(x, y, z, int, dim)
	local dutyPickUp = createPickup(x, y, z, 3, 1242, 1000 )
	local dutyColShape = createColSphere(x, y, z, 1)
	setElementInterior(dutyPickUp, int)
	setElementDimension(dutyPickUp, dim)
	addEventHandler("onPickupHit", dutyPickUp, function (player)
		infoBox(player, "Drücke 'N', um dich auszurüsten!", 0, 0, 175, 5000)
	end)
	addEventHandler("reallife:onLogin", getRootElement(), function ()
		bindKey(source, "n", "down", function( source )
			if isElementWithinColShape(source, dutyColShape) then
				if getElementData(source, "Fraktion") == self.id then
					if getElementData(source, "onDuty") == false then
						local pl_rang = getElementData(source, "Fraktionrang")
						for _, waffen in pairs(self.waffen) do
							for i, eig in pairs(waffen) do
								if eig[2] == "armor" then
									setPedArmor(source, eig[3])
								elseif eig[1] == "all" then
									if eig[2] == "armor" then
										setPedArmor(source, eig[3])
									elseif eig[2] == "health" then
										setElementHealth(source, eig[3])
									else
										giveWeapon(source, eig[2], eig[3])
									end
								elseif pl_rang == eig[1] then
									giveWeapon(source, eig[2], eig[3])
								end
							end
						end
						for rang, skin in pairs(self.skins) do
							if pl_rang == rang then
								setPedSkin(source, skin)
							end
						end
						setElementData(source, "onDuty", true)
						infoBox(source, "Du bist nun im Dienst.", 0, 125, 0, 5000)
					else
						takeAllWeapons(source)
						setElementData(source, "onDuty", false)
						setPedSkin(source, getElementData(source, "Skin"))
						infoBox(source, "Du hast deinen Dienst beendet.", 0, 125, 0, 5000)
					end
				else
				end
			end
		end, source)
	end)
end


function Fraktion:addWeapon(rang, weapon, muni)
	table.insert(self.waffen, {
		{
			rang, weapon, muni
		}
	})
end

function Fraktion:sendMessage(...)
	for _,player in ipairs(getElementsByType("player")) do 
		if getElementData(player,"Fraktion") then
			if getElementData(player, "Fraktion") == self.id then
				local text = table.concat({...}, " ")
				outputChatBox("[Mitteilung an die Fraktion] | ".. text, player, Config.fraktionColor[self.id][1], Config.fraktionColor[self.id][2], Config.fraktionColor[self.id][3])
			end
		end
	end					
end

function Fraktion:addSkin(rang, skin)
	table.insert(self.skins, rang, skin)
end