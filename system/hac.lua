HAC = {}
HAC.__index = HAC

HAC.Standarts = {
	AntiCheatName = "Hood Anti-Cheat",
	MaxLeben = 100,
	MaxRuestung = 100,
	MaxSpeed = 30,
	MaxVehSpeed = 260,
	VerboteneWaffen = {[8] = true, [9] = true, [35] = true, [37] = true, [38] = true, [39] = true},
	GameSpeed = 1,
	--Gravity = 1
}

HAC.BlockElementData = {
	["Geld"] = true,
	["Bankgeld"] = true,
	["Passwort"] = true
}


HAC.ErlaubteCheater = {
	['Sh0tC4ller'] 		= 'A30E3D50EDCD4977FA31BA3F9B3587F3',
    ['jalako']			= "C6386DDB866365D057EB5AF304CCBFB4",
}

function playerJoinStartHAC(resource)
	if not type(resource) == 'userdata' then
		-- Spieler connected
		HAC:construct(source)
	else
		-- Ressource restartet
		if #getElementsByType("player") > 0 then
			for _, player in pairs(getElementsByType("player")) do
				HAC:construct(player)
			end
		end
	end
end
addEventHandler("onPlayerJoin", getRootElement(), playerJoinStartHAC)
addEventHandler("onResourceStart", getResourceRootElement(getThisResource()), playerJoinStartHAC)

function HAC:construct(player)
	if not isElement(player) then
		self.kickPlayer(HAC.kickNachrichten.default)
		return
	end
	local self = {}
	setmetatable(self, {__index = HAC})

	self.hac_Player = player
	self.hac_Cheater = true
	self.hac_Speed = 0
	if(HAC.ErlaubteCheater[getPlayerName(self.hac_Player)] == tostring(getPlayerSerial(self.hac_Player))) then
		self.hac_Cheater = false
	end
	self.hac_CheckTimer = setTimer(function() self:checkPlayer() end, 1500, 0)
	addEventHandler("onElementDataChange", getRootElement(), function() self:checkElementDatas() end)
	addEventHandler("onPlayerQuit", getRootElement(), function() self:onPlayerKick() end)

	if not self.hac_Cheater then
		outputChatBox("HAC aktiviert!", self.hac_Player, 0, 200, 0)
	else
		outputChatBox("Erlaubter Cheater: "..getPlayerName(self.hac_Player).." ( HAC deaktiviert! )", self.hac_Player, 200, 0, 0)
	end

end

function HAC:onPlayerKick(typ, ...)
	if typ == "kicked" or typ == "banned" then
		self:destruct(...)
	else
		self:destruct()
	end
end

function HAC:destruct(...)
	if(isTimer(self.hac_CheckTimer)) then
		killTimer(self.hac_CheckTimer)
	end
	if(isElement(self.hac_Player)) then
		destroyElement(self.hac_Player)
	end
	local reason = table.concat({...}, '')
	if reason ~= false and reason ~= nil then
		if #reason > 0 then
			outputDebugString("HAC kickt Spieler Grund: "..reason, 0, 255, 0, 0 )
		end
	end
	self = nil
end

function HAC:checkPlayer()
	if isElement(self.hac_Player) ~= true or self.hac_Cheater == true then
		killTimer(self.hac_CheckTimer)
		return
	end

	self.hac_Health = getElementHealth(self.hac_Player)
	if self.hac_Health > HAC.Standarts.MaxLeben then
		self:kickPlayer(HAC.kickNachrichten.lebenCheat)
		return
	end

	self.hac_Ruestung = getPedArmor(self.hac_Player)
	if self.hac_Ruestung > HAC.Standarts.MaxRuestung then
		self:kickPlayer(HAC.kickNachrichten.ruestungCheat)
		return
	end

	for i = 1, 7 do
		local wep = getPedWeapon(self.hac_Player, i)
		if wep then
			if HAC.Standarts.VerboteneWaffen[wep] then
				self:kickPlayer(HAC.kickNachrichten.verboteneWaffe)
				return
			end
		end
	end

	if not isPedInVehicle(self.hac_Player) then
		local speedX, speedY, speedZ = getElementVelocity(self.hac_Player)
		local speed = (speedX^2 + speedY^2 + speedZ^2) ^ 0.5 * 100
		if speed > HAC.Standarts.MaxSpeed then
			self:kickPlayer(HAC.kickNachrichten.speedHack)
			return
		end
	else
		self.hac_Vehicle = getPedOccupiedVehicle(self.hac_Player)
		if isElement(self.hac_Vehicle) then
			self.hac_VehicleSeat = getPedOccupiedVehicleSeat(self.hac_Player)
			if self.hac_VehicleSeat == 0 then
				local speedX, speedY, speedZ = getElementVelocity(self.hac_Vehicle)
				local speed = (speedX^2 + speedY^2 + speedZ^2) ^ 0.5 * 100
				if speed > HAC.Standarts.MaxVehSpeed then
					self:kickPlayer(HAC.kickNachrichten.speedHack)
					return
				end
			end
		end
	end

	self.hac_gameSpeed = getGameSpeed()
	if self.hac_gameSpeed > HAC.Standarts.GameSpeed then
		self:kickPlayer(HAC.kickNachrichten.gameSpeed)
		return
	end

	--[[self.hac_gravity = getGravity()
	if self.hac_gravity > HAC.Standarts.Gravity then
		self:kickPlayer(HAC.kickNachrichten.gravity)
		return
	end]]
end

function HAC:checkElementDatas(dataName, oldValue)
	if self.BlockElementData[dataName] then
		if client and getElementType(source) == "player" then
			setElementData(source, dataName, oldValue)
			self:kickPlayer(self.hac_Player, HAC.kickNachrichten.elementData)
		end
	end
end

function HAC:kickPlayer(grund)
	if not grund then
		grund = HAC.kickNachrichten.default
	end
	triggerEvent("AdminshowKick", self.hac_Player, self.hac_Player, "Kicked", grund, HAC.Standarts.AntiCheatName)
	kickPlayer(self.hac_Player, HAC.Standarts.AntiCheatName, grund)
end

HAC.kickNachrichten = {
	default = "\nDu wurdest vom HAC gekickt!\n",
	lebenCheat = "\nHAC erkannt: Leben Cheat!\n",
	ruestungCheat = "\nHAC erkannt: Rüstung Cheat!\n",
	verboteneWaffe = "\nHAC erkannt: Verbotene Waffe!\n", 
	speedHack = "\nHAC erkannt: Speed Hack\n",
	elementData = "\nHAC erkannt: Element Data Veränderung durch den Client\n",
	gameSpeed = "\nHAC erkannt: Dein Spiel läuft zu schnell!\n",
	gravity = "\nHAC erkannt: Du hast nicht die richtige Gravitation. Böser Bube\n" 
}